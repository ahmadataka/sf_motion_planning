#include "ros/ros.h"
#include "geometry_msgs/Pose.h"
#include "std_msgs/Float64MultiArray.h"
#include "geometry_msgs/PoseArray.h"
#include <iostream>
#include <armadillo>
#include <math.h>
// #include <random>

using namespace std;
using namespace arma;

// Armadillo documentation is available at:
// http://arma.sourceforge.net/docs.html

// It is IMPORTANT to be noted that l variable here stands for change of length 

// Global Variable
double d = 0.0134; // diameter of manipulator
double freq = 400.0;
double dt = 1.0/freq;
// double dt = 0.01;
double Lo = 0.1200;
double y[] = {-0.003, -0.002, -0.001, 0.003, 0.001, 0.002, -0.002, 0.003, -0.001};
double k_val[3], p_val[3], s_val[3];
float mu, sigma;
mat tip_pose, v, state, state_con;
mat A, B, C, P, Q, R;
mat Jv, Jkpsl;
unsigned char flag_pose;

double l_to_k(double l1, double l2, double l3)
{
    double k_out = 2*(pow(pow(l1,2)+pow(l2,2)+pow(l3,2)-l1*l2-l1*l3-l2*l3,0.5))/(d*(3*Lo+l1+l2+l3));
    
    return k_out;
}

double l_to_phi(double l_1, double l_2, double l_3)
{
    double phi_out = atan2((sqrt(3)*(l_2+l_3-2*l_1)),(3*(l_2-l_3)));
    
    return phi_out;
}

double l_to_s(double l1, double l2, double l3)
{
    double s_out = (3*Lo+l1+l2+l3)/3;
    
    return s_out;
}

mat pass_Jkpsl(double l[3][3])
{
	double jac_kpsl[9][9];
	
	jac_kpsl[0][0] = 2*(l[0][0] - l[0][1]/2 - l[0][2]/2)/((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824)*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))) - 0.0268*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))/pow((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824),2);
	jac_kpsl[0][1] = 2*(-l[0][0]/2 + l[0][1] - l[0][2]/2)/((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824)*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))) - 0.0268*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))/pow((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824),2);
	jac_kpsl[0][2] = 2*(-l[0][0]/2 - l[0][1]/2 + l[0][2])/((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824)*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))) - 0.0268*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))/pow((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824),2);
	jac_kpsl[0][3] = 0.0;
	jac_kpsl[0][4] = 0.0;
	jac_kpsl[0][5] = 0.0;
	jac_kpsl[0][6] = 0.0;
	jac_kpsl[0][7] = 0.0;
	jac_kpsl[0][8] = 0.0;
	jac_kpsl[1][0] = -2*sqrt(3)/((1 + 3*pow((-2*l[0][0] + l[0][1] + l[0][2]),2)/pow((3*l[0][1] - 3*l[0][2]),2))*(3*l[0][1] - 3*l[0][2]));
	jac_kpsl[1][1] = (sqrt(3)/(3*l[0][1] - 3*l[0][2]) - 3*sqrt(3)*(-2*l[0][0] + l[0][1] + l[0][2])/pow((3*l[0][1] - 3*l[0][2]),2))/(1 + 3*pow((-2*l[0][0] + l[0][1] + l[0][2]),2)/pow((3*l[0][1] - 3*l[0][2]),2));
	jac_kpsl[1][2] = (sqrt(3)/(3*l[0][1] - 3*l[0][2]) + 3*sqrt(3)*(-2*l[0][0] + l[0][1] + l[0][2])/pow((3*l[0][1] - 3*l[0][2]),2))/(1 + 3*pow((-2*l[0][0] + l[0][1] + l[0][2]),2)/pow((3*l[0][1] - 3*l[0][2]),2));
	jac_kpsl[1][3] = 0.0;
	jac_kpsl[1][4] = 0.0;
	jac_kpsl[1][5] = 0.0;
	jac_kpsl[1][6] = 0.0;
	jac_kpsl[1][7] = 0.0;
	jac_kpsl[1][8] = 0.0;
	jac_kpsl[2][0] = 1.0/3.0;
	jac_kpsl[2][1] = 1.0/3.0;
	jac_kpsl[2][2] = 1.0/3.0;
	jac_kpsl[2][3] = 0.0;
	jac_kpsl[2][4] = 0.0;
	jac_kpsl[2][5] = 0.0;
	jac_kpsl[2][6] = 0.0;
	jac_kpsl[2][7] = 0.0;
	jac_kpsl[2][8] = 0.0;
	jac_kpsl[3][0] = 0.0;
	jac_kpsl[3][1] = 0.0;
	jac_kpsl[3][2] = 0.0;
	jac_kpsl[3][3] = 2*(l[1][0] - l[1][1]/2 - l[1][2]/2)/((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824)*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))) - 0.0268*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))/pow((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824),2);
	jac_kpsl[3][4] = 2*(-l[1][0]/2 + l[1][1] - l[1][2]/2)/((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824)*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))) - 0.0268*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))/pow((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824),2);
	jac_kpsl[3][5] = 2*(-l[1][0]/2 - l[1][1]/2 + l[1][2])/((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824)*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))) - 0.0268*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))/pow((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824),2);
	jac_kpsl[3][6] = 0.0;
	jac_kpsl[3][7] = 0.0;
	jac_kpsl[3][8] = 0.0;
	jac_kpsl[4][0] = 0.0;
	jac_kpsl[4][1] = 0.0;
	jac_kpsl[4][2] = 0.0;
	jac_kpsl[4][3] = -2*sqrt(3)/((1 + 3*pow((-2*l[1][0] + l[1][1] + l[1][2]),2)/pow((3*l[1][1] - 3*l[1][2]),2))*(3*l[1][1] - 3*l[1][2]));
	jac_kpsl[4][4] = (sqrt(3)/(3*l[1][1] - 3*l[1][2]) - 3*sqrt(3)*(-2*l[1][0] + l[1][1] + l[1][2])/pow((3*l[1][1] - 3*l[1][2]),2))/(1 + 3*pow((-2*l[1][0] + l[1][1] + l[1][2]),2)/pow((3*l[1][1] - 3*l[1][2]),2));
	jac_kpsl[4][5] = (sqrt(3)/(3*l[1][1] - 3*l[1][2]) + 3*sqrt(3)*(-2*l[1][0] + l[1][1] + l[1][2])/pow((3*l[1][1] - 3*l[1][2]),2))/(1 + 3*pow((-2*l[1][0] + l[1][1] + l[1][2]),2)/pow((3*l[1][1] - 3*l[1][2]),2));
	jac_kpsl[4][6] = 0.0;
	jac_kpsl[4][7] = 0.0;
	jac_kpsl[4][8] = 0.0;
	jac_kpsl[5][0] = 0.0;
	jac_kpsl[5][1] = 0.0;
	jac_kpsl[5][2] = 0.0;
	jac_kpsl[5][3] = 1.0/3.0;
	jac_kpsl[5][4] = 1.0/3.0;
	jac_kpsl[5][5] = 1.0/3.0;
	jac_kpsl[5][6] = 0.0;
	jac_kpsl[5][7] = 0.0;
	jac_kpsl[5][8] = 0.0;
	jac_kpsl[6][0] = 0.0;
	jac_kpsl[6][1] = 0.0;
	jac_kpsl[6][2] = 0.0;
	jac_kpsl[6][4] = 0.0;
	jac_kpsl[6][3] = 0.0;
	jac_kpsl[6][5] = 0.0;
	jac_kpsl[6][6] = 2*(l[2][0] - l[2][1]/2 - l[2][2]/2)/((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824)*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))) - 0.0268*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))/pow((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824),2);
	jac_kpsl[6][7] = 2*(-l[2][0]/2 + l[2][1] - l[2][2]/2)/((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824)*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))) - 0.0268*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))/pow((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824),2);
	jac_kpsl[6][8] = 2*(-l[2][0]/2 - l[2][1]/2 + l[2][2])/((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824)*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))) - 0.0268*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))/pow((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824),2);
	jac_kpsl[7][0] = 0.0;
	jac_kpsl[7][1] = 0.0;
	jac_kpsl[7][2] = 0.0;
	jac_kpsl[7][3] = 0.0;
	jac_kpsl[7][4] = 0.0;
	jac_kpsl[7][5] = 0.0;
	jac_kpsl[7][6] = -2*sqrt(3)/((1 + 3*pow((-2*l[2][0] + l[2][1] + l[2][2]),2)/pow((3*l[2][1] - 3*l[2][2]),2))*(3*l[2][1] - 3*l[2][2]));
	jac_kpsl[7][7] = (sqrt(3)/(3*l[2][1] - 3*l[2][2]) - 3*sqrt(3)*(-2*l[2][0] + l[2][1] + l[2][2])/pow((3*l[2][1] - 3*l[2][2]),2))/(1 + 3*pow((-2*l[2][0] + l[2][1] + l[2][2]),2)/pow((3*l[2][1] - 3*l[2][2]),2));
	jac_kpsl[7][8] = (sqrt(3)/(3*l[2][1] - 3*l[2][2]) + 3*sqrt(3)*(-2*l[2][0] + l[2][1] + l[2][2])/pow((3*l[2][1] - 3*l[2][2]),2))/(1 + 3*pow((-2*l[2][0] + l[2][1] + l[2][2]),2)/pow((3*l[2][1] - 3*l[2][2]),2));
	jac_kpsl[8][0] = 0.0;
	jac_kpsl[8][1] = 0.0;
	jac_kpsl[8][2] = 0.0;
	jac_kpsl[8][3] = 0.0;
	jac_kpsl[8][4] = 0.0;
	jac_kpsl[8][5] = 0.0;
	jac_kpsl[8][6] = 1.0/3.0;
	jac_kpsl[8][7] = 1.0/3.0;
	jac_kpsl[8][8] = 1.0/3.0;
	
	mat Jg_ex;
    Jg_ex << jac_kpsl[0][0] << jac_kpsl[0][1] << jac_kpsl[0][2] << jac_kpsl[0][3] << jac_kpsl[0][4] << jac_kpsl[0][5] << jac_kpsl[0][6] << jac_kpsl[0][7] << jac_kpsl[0][8] << endr
	  << jac_kpsl[1][0] << jac_kpsl[1][1] << jac_kpsl[1][2] << jac_kpsl[1][3] << jac_kpsl[1][4] << jac_kpsl[1][5] << jac_kpsl[1][6] << jac_kpsl[1][7] << jac_kpsl[1][8] << endr
	  << jac_kpsl[2][0] << jac_kpsl[2][1] << jac_kpsl[2][2] << jac_kpsl[2][3] << jac_kpsl[2][4] << jac_kpsl[2][5] << jac_kpsl[2][6] << jac_kpsl[2][7] << jac_kpsl[2][8] << endr
	  << jac_kpsl[3][0] << jac_kpsl[3][1] << jac_kpsl[3][2] << jac_kpsl[3][3] << jac_kpsl[3][4] << jac_kpsl[3][5] << jac_kpsl[3][6] << jac_kpsl[3][7] << jac_kpsl[3][8] << endr
	  << jac_kpsl[4][0] << jac_kpsl[4][1] << jac_kpsl[4][2] << jac_kpsl[4][3] << jac_kpsl[4][4] << jac_kpsl[4][5] << jac_kpsl[4][6] << jac_kpsl[4][7] << jac_kpsl[4][8] << endr
	  << jac_kpsl[5][0] << jac_kpsl[5][1] << jac_kpsl[5][2] << jac_kpsl[5][3] << jac_kpsl[5][4] << jac_kpsl[5][5] << jac_kpsl[5][6] << jac_kpsl[5][7] << jac_kpsl[5][8] << endr
	  << jac_kpsl[6][0] << jac_kpsl[6][1] << jac_kpsl[6][2] << jac_kpsl[6][3] << jac_kpsl[6][4] << jac_kpsl[6][5] << jac_kpsl[6][6] << jac_kpsl[6][7] << jac_kpsl[6][8] << endr
	  << jac_kpsl[7][0] << jac_kpsl[7][1] << jac_kpsl[7][2] << jac_kpsl[7][3] << jac_kpsl[7][4] << jac_kpsl[7][5] << jac_kpsl[7][6] << jac_kpsl[7][7] << jac_kpsl[7][8] << endr
	  << jac_kpsl[8][0] << jac_kpsl[8][1] << jac_kpsl[8][2] << jac_kpsl[8][3] << jac_kpsl[8][4] << jac_kpsl[8][5] << jac_kpsl[8][6] << jac_kpsl[8][7] << jac_kpsl[8][8] << endr;
    
    return Jg_ex;
}

mat pass_Jv(double base_ang[3], double k[3], double p[3], double s[3], unsigned char segment)
{
    mat Jv_ex;
    double jac_v[3][9];
    if(segment == 3)
    {
		jac_v[0][0] = (-(-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + s[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + s[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + s[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] + s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0])/k[0] + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0])/k[0] + s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])/k[0] - (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])/pow(k[0],2) - (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/pow(k[0],2) - (-cos(k[0]*s[0]) + 1)*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])/pow(k[0],2);
		jac_v[0][1] = ((-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])*cos(p[0]) - sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(k[1]*s[1]) + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])*cos(p[0]) - sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(k[1]*s[1]) + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])*cos(p[0]) - sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(k[1]*s[1])/k[1] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[0]*s[0]) + 1)*cos(p[0])/k[0] - (-cos(k[0]*s[0]) + 1)*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])/k[0];
		jac_v[0][2] = (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]) + (-(-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + k[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + k[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + k[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1];
		jac_v[0][3] = (-cos(k[2]*s[2]) + 1)*(-s[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - s[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(p[2])/k[2] + (s[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + s[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + s[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1])/k[1] + s[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1])/k[1] + s[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1])/k[1] - ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/pow(k[1],2) - (-cos(k[1]*s[1]) + 1)*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/pow(k[1],2) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/pow(k[1],2);
		jac_v[0][4] = (-((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(k[1]*s[1])*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1])*cos(k[1]*s[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] - (-cos(k[1]*s[1]) + 1)*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])/k[1];
		jac_v[0][5] =  ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + (-cos(k[2]*s[2]) + 1)*(-k[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - k[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(p[2])/k[2] + (k[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + k[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2];
		jac_v[0][6] = s[2]*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2])/k[2] + s[2]*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(k[2]*s[2])/k[2] + s[2]*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])/k[2] - (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/pow(k[2],2) - (-cos(k[2]*s[2]) + 1)*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*cos(p[2])/pow(k[2],2) - (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/pow(k[2],2);
		jac_v[0][7] = (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(p[2])/k[2];
		jac_v[0][8] = (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]);
		jac_v[1][0] = ((-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]) + (s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + s[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]) - (s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + s[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + s[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] + s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0])/k[0] + s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0])/k[0] + s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0])/k[0] - (sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/pow(k[0],2) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])/pow(k[0],2) - (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])/pow(k[0],2);
		jac_v[1][1] = ((-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])*cos(p[0]) - sin(base_ang[0])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])*cos(p[0]) - sin(base_ang[0])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])*cos(p[0]) - sin(base_ang[0])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]))*sin(k[1]*s[1])/k[1] + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[0]*s[0]) + 1)*cos(p[0])/k[0] - (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*sin(p[0])*cos(base_ang[1])/k[0];
		jac_v[1][2] = (sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]) + ((-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]) + (k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + k[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]) - (k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + k[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + k[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1];
		jac_v[1][3] = (-cos(k[2]*s[2]) + 1)*(-s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1])/k[1] + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1])/k[1] + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1])/k[1] - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/pow(k[1],2) - (-cos(k[1]*s[1]) + 1)*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/pow(k[1],2) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/pow(k[1],2);
		jac_v[1][4] = (-((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(k[1]*s[1])*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] - (-cos(k[1]*s[1]) + 1)*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])/k[1];
		jac_v[1][5] = ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-cos(k[2]*s[2]) + 1)*(-k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2];
		jac_v[1][6] = s[2]*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2])/k[2] + s[2]*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2])/k[2] + s[2]*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])/k[2] - (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/pow(k[2],2) - (-cos(k[2]*s[2]) + 1)*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/pow(k[2],2) - (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/pow(k[2],2);
		jac_v[1][7] = (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(p[2])/k[2];
		jac_v[1][8] = (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2]) + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]);
		jac_v[2][0] = ((s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-s[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + s[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - (-s[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + s[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])/k[1] + (-s[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + s[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])/k[1] - s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0])/k[0] + s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1])/k[0] + s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0])/k[0] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[1])*cos(p[0])/pow(k[0],2) - (-cos(k[0]*s[0]) + 1)*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])/pow(k[0],2) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2])/pow(k[0],2);
		jac_v[2][1] = ((sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - (sin(base_ang[1])*sin(p[0])*sin(k[0]*s[0]) + sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (sin(base_ang[1])*sin(p[0])*sin(k[0]*s[0]) + sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (sin(base_ang[1])*sin(p[0])*sin(k[0]*s[0]) + sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/k[1] + (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[1])*sin(p[0])/k[0] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[2])*cos(base_ang[1])*cos(p[0])/k[0];
		jac_v[2][2] = -sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]) + ((k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-k[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + k[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - (-k[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + k[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])/k[1] + (-k[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + k[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])/k[1];
		jac_v[2][3] = (-cos(k[2]*s[2]) + 1)*(-s[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) - s[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) - s[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (s[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - s[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + s[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + s[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1])/k[1] + s[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1])/k[1] + s[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1])/k[1] - (sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/pow(k[1],2) - (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])/pow(k[1],2) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])/pow(k[1],2);
		jac_v[2][4] = (-(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1])*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + (sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] - (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1])/k[1];
		jac_v[2][5] = (sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]) + (-cos(k[2]*s[2]) + 1)*(-k[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) - k[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) - k[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (k[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - k[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + k[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2];
		jac_v[2][6] = s[2]*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2])/k[2] + s[2]*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2])/k[2] + s[2]*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])/k[2] - ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/pow(k[2],2) - (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/pow(k[2],2) - ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/pow(k[2],2);
		jac_v[2][7] = ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(p[2])/k[2];
		jac_v[2][8] = ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2]) + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2]) + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]);
    }
    else if(segment == 2)
    {
		jac_v[0][0] = -s[0]*(-cos(k[1]*s[1]) + 1)*sin(k[0]*s[0])*cos(p[0])*cos(p[1])/k[1] + s[0]*sin(k[1]*s[1])*cos(p[0])*cos(k[0]*s[0])/k[1] + s[0]*sin(k[0]*s[0])*cos(p[0])/k[0] - (-cos(k[0]*s[0]) + 1)*cos(p[0])/pow(k[0], 2);
		jac_v[0][1] = -(-cos(k[1]*s[1]) + 1)*sin(p[0])*cos(p[1])*cos(k[0]*s[0])/k[1] - (-cos(k[1]*s[1]) + 1)*sin(p[1])*cos(p[0])/k[1] - sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1])/k[1] - (-cos(k[0]*s[0]) + 1)*sin(p[0])/k[0];
		jac_v[0][2] = -k[0]*(-cos(k[1]*s[1]) + 1)*sin(k[0]*s[0])*cos(p[0])*cos(p[1])/k[1] + k[0]*sin(k[1]*s[1])*cos(p[0])*cos(k[0]*s[0])/k[1] + sin(k[0]*s[0])*cos(p[0]);
		jac_v[0][3] = -s[1]*sin(p[0])*sin(p[1])*sin(k[1]*s[1])/k[1] + s[1]*sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1])/k[1] + s[1]*sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0])/k[1] + (-cos(k[1]*s[1]) + 1)*sin(p[0])*sin(p[1])/pow(k[1], 2) - (-cos(k[1]*s[1]) + 1)*cos(p[0])*cos(p[1])*cos(k[0]*s[0])/pow(k[1], 2) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0])/pow(k[1], 2);
		jac_v[0][4] = -(-cos(k[1]*s[1]) + 1)*sin(p[0])*cos(p[1])/k[1] - (-cos(k[1]*s[1]) + 1)*sin(p[1])*cos(p[0])*cos(k[0]*s[0])/k[1];
		jac_v[0][5] =  -sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]);
		jac_v[0][6] = 0.0;
		jac_v[0][7] = 0.0;
		jac_v[0][8] = 0.0;
		jac_v[1][0] = -s[0]*(-cos(k[1]*s[1]) + 1)*sin(p[0])*sin(k[0]*s[0])*cos(p[1])/k[1] + s[0]*sin(p[0])*sin(k[1]*s[1])*cos(k[0]*s[0])/k[1] + s[0]*sin(p[0])*sin(k[0]*s[0])/k[0] - (-cos(k[0]*s[0]) + 1)*sin(p[0])/pow(k[0], 2);
		jac_v[1][1] = -(-cos(k[1]*s[1]) + 1)*sin(p[0])*sin(p[1])/k[1] + (-cos(k[1]*s[1]) + 1)*cos(p[0])*cos(p[1])*cos(k[0]*s[0])/k[1] + sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0])/k[1] + (-cos(k[0]*s[0]) + 1)*cos(p[0])/k[0];
		jac_v[1][2] = -k[0]*(-cos(k[1]*s[1]) + 1)*sin(p[0])*sin(k[0]*s[0])*cos(p[1])/k[1] + k[0]*sin(p[0])*sin(k[1]*s[1])*cos(k[0]*s[0])/k[1] + sin(p[0])*sin(k[0]*s[0]);
		jac_v[1][3] = s[1]*sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1])/k[1] + s[1]*sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0])/k[1] + s[1]*sin(p[1])*sin(k[1]*s[1])*cos(p[0])/k[1] - (-cos(k[1]*s[1]) + 1)*sin(p[0])*cos(p[1])*cos(k[0]*s[0])/pow(k[1], 2) - (-cos(k[1]*s[1]) + 1)*sin(p[1])*cos(p[0])/pow(k[1], 2) - sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1])/pow(k[1], 2);
		jac_v[1][4] = -(-cos(k[1]*s[1]) + 1)*sin(p[0])*sin(p[1])*cos(k[0]*s[0])/k[1] + (-cos(k[1]*s[1]) + 1)*cos(p[0])*cos(p[1])/k[1];
		jac_v[1][5] = sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]);
		jac_v[1][6] = 0.0;
		jac_v[1][7] = 0.0;
		jac_v[1][8] = 0.0;
		jac_v[2][0] = -s[0]*(-cos(k[1]*s[1]) + 1)*cos(p[1])*cos(k[0]*s[0])/k[1] - s[0]*sin(k[0]*s[0])*sin(k[1]*s[1])/k[1] + s[0]*cos(k[0]*s[0])/k[0] - sin(k[0]*s[0])/pow(k[0], 2);
		jac_v[2][1] = 0.0;
		jac_v[2][2] = -k[0]*(-cos(k[1]*s[1]) + 1)*cos(p[1])*cos(k[0]*s[0])/k[1] - k[0]*sin(k[0]*s[0])*sin(k[1]*s[1])/k[1] + cos(k[0]*s[0]);
		jac_v[2][3] = -s[1]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1])/k[1] + s[1]*cos(k[0]*s[0])*cos(k[1]*s[1])/k[1] + (-cos(k[1]*s[1]) + 1)*sin(k[0]*s[0])*cos(p[1])/pow(k[1], 2) - sin(k[1]*s[1])*cos(k[0]*s[0])/pow(k[1], 2);
		jac_v[2][4] = (-cos(k[1]*s[1]) + 1)*sin(p[1])*sin(k[0]*s[0])/k[1];
		jac_v[2][5] = -sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]);
		jac_v[2][6] = 0.0;
		jac_v[2][7] = 0.0;
		jac_v[2][8] = 0.0;
	}
	else if(segment == 1)
	{
		jac_v[0][0] = s[0]*sin(k[0]*s[0])*cos(p[0])/k[0] - (-cos(k[0]*s[0]) + 1)*cos(p[0])/pow(k[0], 2);
		jac_v[0][1] = -(-cos(k[0]*s[0]) + 1)*sin(p[0])/k[0];
		jac_v[0][2] = sin(k[0]*s[0])*cos(p[0]);
		jac_v[0][3] = 0.0;
		jac_v[0][4] = 0.0;
		jac_v[0][5] = 0.0;
		jac_v[0][6] = 0.0;
		jac_v[0][7] = 0.0;
		jac_v[0][8] = 0.0;
		jac_v[1][0] = s[0]*sin(p[0])*sin(k[0]*s[0])/k[0] - (-cos(k[0]*s[0]) + 1)*sin(p[0])/pow(k[0], 2);
		jac_v[1][1] = (-cos(k[0]*s[0]) + 1)*cos(p[0])/k[0];
		jac_v[1][2] = sin(p[0])*sin(k[0]*s[0]);
		jac_v[1][3] = 0.0;
		jac_v[1][4] = 0.0;
		jac_v[1][5] = 0.0;
		jac_v[1][6] = 0.0;
		jac_v[1][7] = 0.0;
		jac_v[1][8] = 0.0;
		jac_v[2][0] = s[0]*cos(k[0]*s[0])/k[0] - sin(k[0]*s[0])/pow(k[0], 2);
		jac_v[2][1] = 0.0;
		jac_v[2][2] = cos(k[0]*s[0]);
		jac_v[2][3] = 0.0;
		jac_v[2][4] = 0.0;
		jac_v[2][5] = 0.0;
		jac_v[2][6] = 0.0;
		jac_v[2][7] = 0.0;
		jac_v[2][8] = 0.0;
	}
    
    Jv_ex << jac_v[0][0] << jac_v[0][1] << jac_v[0][2] << jac_v[0][3] << jac_v[0][4] << jac_v[0][5] << jac_v[0][6] << jac_v[0][7] << jac_v[0][8] << endr
	  << jac_v[1][0] << jac_v[1][1] << jac_v[1][2] << jac_v[1][3] << jac_v[1][4] << jac_v[1][5] << jac_v[1][6] << jac_v[1][7] << jac_v[1][8] << endr
	  << jac_v[2][0] << jac_v[2][1] << jac_v[2][2] << jac_v[2][3] << jac_v[2][4] << jac_v[2][5] << jac_v[2][6] << jac_v[2][7] << jac_v[2][8] << endr;
    return Jv_ex;
}

void c_to_q()
{ 
  for(unsigned char i=0; i<3; i++)
  {
    k_val[i] = l_to_k(state(3*i,0),state(3*i+1,0),state(3*i+2,0));
    p_val[i] = l_to_phi(state(3*i,0),state(3*i+1,0),state(3*i+2,0));
    s_val[i] = l_to_s(state(3*i,0),state(3*i+1,0),state(3*i+2,0));
//     k_val[i] = l_to_k(state_con(3*i,0),state_con(3*i+1,0),state_con(3*i+2,0));
//     p_val[i] = l_to_phi(state_con(3*i,0),state_con(3*i+1,0),state_con(3*i+2,0));
//     s_val[i] = l_to_s(state_con(3*i,0),state_con(3*i+1,0),state_con(3*i+2,0));
  }
  
//   k_noi = l_to_k(l_noise[0],l_noise[1],l_noise[2]);
//   p_noi = l_to_phi(l_noise[0],l_noise[1],l_noise[2]);
//   s_noi = l_to_s(l_noise[0],l_noise[1],l_noise[2]);
}

mat homogeneous_transform(double kappa_sym, double phi_sym, double s_sym)
{
  mat T;
  T << cos(phi_sym)*cos(kappa_sym*s_sym) << -sin(phi_sym) << cos(phi_sym)*sin(kappa_sym*s_sym) << cos(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym << endr
    << sin(phi_sym)*cos(kappa_sym*s_sym) << cos(phi_sym) << sin(phi_sym)*sin(kappa_sym*s_sym) << sin(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym << endr
    << -sin(kappa_sym*s_sym) << 0 << cos(kappa_sym*s_sym) << sin(kappa_sym*s_sym)/kappa_sym << endr
    << 0 << 0 << 0 << 1 << endr;
  return T;
}

mat forward_kin(unsigned char segment_num, double k[3], double p[3], double s[3])
{
  mat T = eye<mat>(4,4);
  for(unsigned char i=0; i<segment_num; i++)
  {
    T = T*homogeneous_transform(k[i], p[i], s[i]);
  }
  return T;
} 

mat homo_to_pose(mat T)
{
  mat position;
  position << T(0,3) << endr
	   << T(1,3) << endr
	   << T(2,3) << endr;
  return position;
}

void integral_length()
{

  mat u, P_dum, state_dummy, matrix_dum, Kalman, jacobi_dummy;
  double k_dum[3], p_dum[3], s_dum[3];
  double len_dummy[3][3];
  u = v;
  
  // Extended Kalman Filter
  P_dum = A*P*A.t() + Q;
  state_dummy = A*state + B*u;
//   state_dummy = A*state_con + B*u;
  
  for(unsigned char i=0; i<3; i++)
  {
    k_dum[i] = l_to_k(state_dummy(3*i,0),state_dummy(3*i+1,0),state_dummy(3*i+2,0));
    p_dum[i] = l_to_phi(state_dummy(3*i,0),state_dummy(3*i+1,0),state_dummy(3*i+2,0));
    s_dum[i] = l_to_s(state_dummy(3*i,0),state_dummy(3*i+1,0),state_dummy(3*i+2,0));
  }
  unsigned char ind = 0;
  for(unsigned char i=0; i<3; i++)
  {
    for(unsigned char j=0; j<3; j++)
    {
      len_dummy[i][j] = state_dummy(ind,0);
      ind++;
    }
  }
  
  double angle_dummy[3] = {0.0, 0.0, 0.0};
  //k_dum[0] = 1.0; k_dum[1] = 0.5; k_dum[2] = -1.0; 
  //p_dum[0] = 0.2; p_dum[1] = 0.1; p_dum[2] = 0.3; 
  //s_dum[0] = 0.12; s_dum[1] = 0.12; s_dum[2] = 0.12; 
  for(unsigned char i=0; i<3; i++)
  {
	jacobi_dummy = join_cols(jacobi_dummy, pass_Jv(angle_dummy, k_dum, p_dum, s_dum, i+1));
  }
  
  Jv = jacobi_dummy;
//   Jv.print();
//   cout << " " << endl;
  Jkpsl = pass_Jkpsl(len_dummy);
  C = Jv*Jkpsl;
  
  matrix_dum = (C*P_dum*C.t()+R);
  if(det(matrix_dum) != 0) Kalman = P_dum*C.t()*inv(matrix_dum);
  else cout << "DET=0" << endl;
  
  mat tip_pose_est;
  tip_pose_est = join_cols(homo_to_pose(forward_kin(1, k_dum, p_dum, s_dum)), homo_to_pose(forward_kin(2, k_dum, p_dum, s_dum)));
  tip_pose_est = join_cols(tip_pose_est, homo_to_pose(forward_kin(3, k_dum, p_dum, s_dum)));
  
  state = state_dummy + Kalman*(tip_pose - tip_pose_est);
  
  // Constraint
  mat delta, state_con_pre;
  mat tau, tau_save, tau_final, vector_dum;
  mat D, d;
  
  D = (-1)*eye<mat>(9,9);
  d = Lo*ones<mat>(9,1);
  
  tau << 0.0 << endr
      << 0.0 << endr
      << 0.0 << endr
      << 0.0 << endr
      << 0.0 << endr
      << 0.0 << endr
      << 0.0 << endr
      << 0.0 << endr
      << 0.0 << endr;
  
  delta = state - state_con;
  unsigned char flag_cons = 0;
  for(unsigned char i=0; i<5; i++)
  {
    tau = tau + delta/5;
    state_con_pre = state_con + tau;
    vector_dum = D*state_con_pre - d;
    if((vector_dum(0,0) > 0) || (vector_dum(1,0)>0) || (vector_dum(2,0) > 0) || (vector_dum(3,0)>0) || (vector_dum(4,0) > 0) || (vector_dum(5,0)>0) || (vector_dum(6,0) > 0) || (vector_dum(7,0)>0) || (vector_dum(8,0) > 0))
    {
      flag_cons = 1;
      tau_save = tau - delta/5;
      break;
    }
  }
  if(flag_cons == 0) tau_final = tau;
  else tau_final = tau_save;
  state_con = state_con + tau_final;
//   tau_final.print();
  P = (eye<mat>(9,9)-Kalman*C)*P_dum;
  c_to_q();
}

void get_pose(const geometry_msgs::PoseArray::ConstPtr& msg)
{
  geometry_msgs::PoseArray pos;
  pos = *msg;
  tip_pose << pos.poses[0].position.x << endr
	   << pos.poses[0].position.y << endr
	   << pos.poses[0].position.z << endr
	   << pos.poses[1].position.x << endr
	   << pos.poses[1].position.y << endr
	   << pos.poses[1].position.z << endr
	   << pos.poses[2].position.x << endr
	   << pos.poses[2].position.y << endr
	   << pos.poses[2].position.z << endr;
  flag_pose = 1;
}

void get_speed(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray sp;
  sp = *msg;
  v << sp.data[0] << endr
    << sp.data[1] << endr
    << sp.data[2] << endr
    << sp.data[3] << endr
    << sp.data[4] << endr
    << sp.data[5] << endr
    << sp.data[6] << endr
    << sp.data[7] << endr
    << sp.data[8] << endr;
}

void initialize()
{
  A = eye<mat>(9,9);
  B = dt*eye<mat>(9,9);
  tip_pose << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr;
  v << 0.0 << endr
    << 0.0 << endr
    << 0.0 << endr
    << 0.0 << endr
    << 0.0 << endr
    << 0.0 << endr
    << 0.0 << endr
    << 0.0 << endr
    << 0.0 << endr;
  
  // Kalman initialization
  mu = 0; sigma = 0.005;
  unsigned char ind = 0;
  mat mat_dummy;
  for(unsigned char i=0; i<3; i++)
  {
    for(unsigned char j=0; j<3; j++)
    {
      mat_dummy << y[ind] << endr;
      state = join_cols(state, mat_dummy);
      state_con = join_cols(state_con, mat_dummy);
      ind++;
    }
  }
//   state.print();
  c_to_q();
  P = eye<mat>(9,9);
  Q = 0.0000000001*eye<mat>(9,9);
  R = pow(sigma,2)*eye<mat>(9,9);
}

int
main(int argc, char** argv)
  { 
    std_msgs::Float64MultiArray state_send, kps, state_con_send;
    ros::init(argc,argv, "observer_3");
    ros::NodeHandle n;
    
    initialize();
    
    ros::Rate loop_rate(freq);
    
    ros::Publisher pub = n.advertise<std_msgs::Float64MultiArray>("configuration_space_est",10);
    ros::Publisher length_pub = n.advertise<std_msgs::Float64MultiArray>("length_est",10);
    ros::Publisher length_con_pub = n.advertise<std_msgs::Float64MultiArray>("length_est_con",10);
    
    ros::Subscriber field_sub = n.subscribe("length_rate", 10, get_speed);
    ros::Subscriber rec_vel_obs = n.subscribe("tip_pose", 10, get_pose);

    while(ros::ok())
    {
      kps.data.clear();
      state_send.data.clear();
      state_con_send.data.clear();
      if(flag_pose == 1) integral_length();


      for(unsigned char i=0; i<9; i++)
      {
	state_send.data.push_back(state(i,0));
	state_con_send.data.push_back(state_con(i,0));
      }	
      
      for(unsigned char i=0; i<3; i++)
      {
	kps.data.push_back(k_val[i]);
	kps.data.push_back(p_val[i]);
	kps.data.push_back(s_val[i]);
      }
      if(flag_pose == 1)
      {
	pub.publish(kps);
	length_pub.publish(state_send);
	length_con_pub.publish(state_con_send);
      }
//       if(flag_pose == 0)
//       {
// 	state.print();
// 	cout << " " << endl;
//       }
      ros::spinOnce();
//       loop_rate.sleep();
    }
    return 0;
  }
