#include "ros/ros.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseArray.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Float64.h"
#include "continuum_manipulator/Vector3Array.h"
#include "continuum_manipulator/Matrices.h"
#include "continuum_manipulator/JacobianField.h"
#include <iostream>
#include <armadillo>
#include <math.h>

using namespace std;
using namespace arma;

// Armadillo documentation is available at:
// http://arma.sourceforge.net/docs.html

// Global Variable
double freq = 40.0;
double dt = 1.0/freq;
double k[3], p[3], s[3];
double position_base[3], angle_base[3];
// double K2 = 0.0000003, rho_0 = 0.025;
float K2 = 0.0000002, rho_0 = 0.07, rho_02 = 0.15, K3 = 0.0001, K4 = 0.001, K5 = 0.00005;
int field_type, obs_num, base, freq_int;
int num_per_seg = 5;
double max_vel = 0.07, maks = 0.0, mini = 100.0;
double  stiff_vec;
mat obs[10];
mat field_sum, field_sum_sens, go_to_goal, sensor_jacobian;
int observer;
mat tangent, tangent_init;
double redundancy_weight;
double obs_range = 0.03;
continuum_manipulator::Vector3Array twist;
mat obs_cur, agent_cur, lc_cross, B, la_cross, obs_sensor;
int flag_obs, flag_print;
double twist_mag, field_max, twist_max, dist_min;
double fun;
int length_active;

mat pass_Jvb(double base_ang[3], double k[3], double p[3], double s[3])
{
	mat Jvb_ex;
	double jac_b[3][6];
	jac_b[0][0] = 1.0;
    jac_b[0][1] = 0.0;
    jac_b[0][2] = 0.0;
    jac_b[0][3] = (((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) + sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*(((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) + sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + (((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) + sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) + sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (-cos(k[1]*s[1]) + 1)*((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/k[1] + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/k[1] + (-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/k[0] + (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])/k[0] - (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])/k[0];
    jac_b[0][4] = ((sin(base_ang[1])*sin(p[0])*cos(base_ang[0]) + sin(base_ang[2])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(base_ang[0])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*sin(p[0])*cos(base_ang[0]) + sin(base_ang[2])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]) + cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(base_ang[0])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((sin(base_ang[1])*sin(p[0])*cos(base_ang[0]) + sin(base_ang[2])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]) + cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(base_ang[0])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (sin(base_ang[1])*sin(p[0])*cos(base_ang[0]) + sin(base_ang[2])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[1])*cos(base_ang[0])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])/k[1] + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]) + cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] - (-cos(k[0]*s[0]) + 1)*sin(base_ang[1])*cos(base_ang[0])*cos(p[0])/k[0] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[2])*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])/k[0] + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])/k[0];
    jac_b[0][5] = ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[1])*cos(p[0])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[1])*sin(k[1]*s[1])*cos(p[0]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])*cos(p[0])/k[1] + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/k[0] + (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])/k[0];
    jac_b[1][0] = 0.0;
    jac_b[1][1] = 1.0;
    jac_b[1][2] = 0.0;
    jac_b[1][3] = (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*cos(p[2])/k[2] + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (-cos(k[1]*s[1]) + 1)*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/k[1] + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/k[1] + (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])/k[0] + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/k[0] + (-cos(k[0]*s[0]) + 1)*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])/k[0];
    jac_b[1][4] = ((sin(base_ang[0])*sin(base_ang[1])*sin(p[0]) + sin(base_ang[0])*sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[0])*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((sin(base_ang[0])*sin(base_ang[1])*sin(p[0]) + sin(base_ang[0])*sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[0])*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[0])*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((sin(base_ang[0])*sin(base_ang[1])*sin(p[0]) + sin(base_ang[0])*sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[0])*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[0])*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (sin(base_ang[0])*sin(base_ang[1])*sin(p[0]) + sin(base_ang[0])*sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[0])*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])/k[1] + (-sin(base_ang[0])*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] - (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*sin(base_ang[1])*cos(p[0])/k[0] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])/k[0] + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2])/k[0];
    jac_b[1][5] = (-(-(-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]))*sin(p[1]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((-(-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]))*sin(k[1]*s[1]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((-(-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]))*cos(k[1]*s[1]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2])/k[2] + (-(-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]))*sin(k[1]*s[1])/k[1] + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])*cos(p[0])/k[1] + (-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])/k[0] + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/k[0];
    jac_b[2][0] = 0.0;
    jac_b[2][1] = 0.0;
    jac_b[2][2] = 1.0;
    jac_b[2][3] = 0.0;
    jac_b[2][4] = ((-sin(base_ang[1])*sin(base_ang[2])*cos(p[0]) + sin(p[0])*cos(base_ang[1]))*cos(p[1]) - (-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[2]) - cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((-sin(base_ang[1])*sin(base_ang[2])*cos(p[0]) + sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0]) - sin(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[2]) - cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((-sin(base_ang[1])*sin(base_ang[2])*cos(p[0]) + sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0]) - sin(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[2]) - cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (-sin(base_ang[1])*sin(base_ang[2])*cos(p[0]) + sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[2]) - cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/k[1] + (-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0]) - sin(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/k[1] - (-cos(k[0]*s[0]) + 1)*sin(base_ang[1])*sin(base_ang[2])*sin(p[0])/k[0] - (-cos(k[0]*s[0]) + 1)*cos(base_ang[1])*cos(p[0])/k[0] - sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[2])/k[0];
    jac_b[2][5] = (-(sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(p[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(p[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(p[0])*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(p[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[2])*cos(base_ang[1])*cos(k[0]*s[0]) + sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1]) + sin(p[1])*cos(base_ang[1])*cos(base_ang[2])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(p[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-sin(base_ang[2])*cos(base_ang[1])*cos(k[0]*s[0]) + sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(k[1]*s[1]) + sin(p[1])*sin(k[1]*s[1])*cos(base_ang[1])*cos(base_ang[2])*cos(p[0]))*sin(k[2]*s[2])/k[2] + (sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(p[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (-sin(base_ang[2])*cos(base_ang[1])*cos(k[0]*s[0]) + sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])/k[1] + (-cos(k[1]*s[1]) + 1)*sin(p[1])*cos(base_ang[1])*cos(base_ang[2])*cos(p[0])/k[1] + (-cos(k[0]*s[0]) + 1)*sin(p[0])*cos(base_ang[1])*cos(base_ang[2])/k[0] - sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])/k[0];

    Jvb_ex << jac_b[0][0] << jac_b[0][1] << jac_b[0][2] << jac_b[0][3] << jac_b[0][4] << jac_b[0][5] << endr
	  << jac_b[1][0] << jac_b[1][1] << jac_b[1][2] << jac_b[1][3] << jac_b[1][4] << jac_b[1][5] << endr
	  << jac_b[2][0] << jac_b[2][1] << jac_b[2][2] << jac_b[2][3] << jac_b[2][4] << jac_b[2][5] << endr;

    return Jvb_ex;
}

mat pass_Jv(double base_ang[3], double k[3], double p[3], double s[3])
{
    mat Jv_ex;
    double jac_v[3][9];
    jac_v[0][0] = (-(-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + s[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + s[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + s[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] + s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0])/k[0] + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0])/k[0] + s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])/k[0] - (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])/pow(k[0],2) - (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/pow(k[0],2) - (-cos(k[0]*s[0]) + 1)*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])/pow(k[0],2);
    jac_v[0][1] = ((-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])*cos(p[0]) - sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(k[1]*s[1]) + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])*cos(p[0]) - sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(k[1]*s[1]) + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])*cos(p[0]) - sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(k[1]*s[1])/k[1] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[0]*s[0]) + 1)*cos(p[0])/k[0] - (-cos(k[0]*s[0]) + 1)*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])/k[0];
    jac_v[0][2] = (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]) + (-(-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + k[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + k[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + k[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1];
    jac_v[0][3] = (-cos(k[2]*s[2]) + 1)*(-s[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - s[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(p[2])/k[2] + (s[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + s[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + s[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1])/k[1] + s[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1])/k[1] + s[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1])/k[1] - ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/pow(k[1],2) - (-cos(k[1]*s[1]) + 1)*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/pow(k[1],2) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/pow(k[1],2);
    jac_v[0][4] = (-((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(k[1]*s[1])*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1])*cos(k[1]*s[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] - (-cos(k[1]*s[1]) + 1)*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])/k[1];
    jac_v[0][5] =  ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + (-cos(k[2]*s[2]) + 1)*(-k[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - k[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(p[2])/k[2] + (k[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + k[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2];
    jac_v[0][6] = s[2]*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2])/k[2] + s[2]*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(k[2]*s[2])/k[2] + s[2]*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])/k[2] - (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/pow(k[2],2) - (-cos(k[2]*s[2]) + 1)*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*cos(p[2])/pow(k[2],2) - (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/pow(k[2],2);
    jac_v[0][7] = (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(p[2])/k[2];
    jac_v[0][8] = (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]);
    jac_v[1][0] = ((-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]) + (s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + s[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]) - (s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + s[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + s[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] + s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0])/k[0] + s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0])/k[0] + s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0])/k[0] - (sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/pow(k[0],2) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])/pow(k[0],2) - (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])/pow(k[0],2);
    jac_v[1][1] = ((-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])*cos(p[0]) - sin(base_ang[0])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])*cos(p[0]) - sin(base_ang[0])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])*cos(p[0]) - sin(base_ang[0])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]))*sin(k[1]*s[1])/k[1] + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[0]*s[0]) + 1)*cos(p[0])/k[0] - (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*sin(p[0])*cos(base_ang[1])/k[0];
    jac_v[1][2] = (sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]) + ((-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]) + (k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + k[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]) - (k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + k[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + k[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1];
    jac_v[1][3] = (-cos(k[2]*s[2]) + 1)*(-s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1])/k[1] + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1])/k[1] + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1])/k[1] - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/pow(k[1],2) - (-cos(k[1]*s[1]) + 1)*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/pow(k[1],2) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/pow(k[1],2);
    jac_v[1][4] = (-((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(k[1]*s[1])*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] - (-cos(k[1]*s[1]) + 1)*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])/k[1];
    jac_v[1][5] = ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-cos(k[2]*s[2]) + 1)*(-k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2];
    jac_v[1][6] = s[2]*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2])/k[2] + s[2]*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2])/k[2] + s[2]*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])/k[2] - (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/pow(k[2],2) - (-cos(k[2]*s[2]) + 1)*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/pow(k[2],2) - (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/pow(k[2],2);
    jac_v[1][7] = (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(p[2])/k[2];
    jac_v[1][8] = (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2]) + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]);
    jac_v[2][0] = ((s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-s[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + s[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - (-s[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + s[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])/k[1] + (-s[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + s[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])/k[1] - s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0])/k[0] + s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1])/k[0] + s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0])/k[0] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[1])*cos(p[0])/pow(k[0],2) - (-cos(k[0]*s[0]) + 1)*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])/pow(k[0],2) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2])/pow(k[0],2);
    jac_v[2][1] = ((sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - (sin(base_ang[1])*sin(p[0])*sin(k[0]*s[0]) + sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (sin(base_ang[1])*sin(p[0])*sin(k[0]*s[0]) + sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (sin(base_ang[1])*sin(p[0])*sin(k[0]*s[0]) + sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/k[1] + (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[1])*sin(p[0])/k[0] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[2])*cos(base_ang[1])*cos(p[0])/k[0];
    jac_v[2][2] = -sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]) + ((k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-k[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + k[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - (-k[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + k[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])/k[1] + (-k[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + k[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])/k[1];
    jac_v[2][3] = (-cos(k[2]*s[2]) + 1)*(-s[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) - s[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) - s[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (s[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - s[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + s[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + s[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1])/k[1] + s[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1])/k[1] + s[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1])/k[1] - (sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/pow(k[1],2) - (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])/pow(k[1],2) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])/pow(k[1],2);
    jac_v[2][4] = (-(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1])*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + (sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] - (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1])/k[1];
    jac_v[2][5] = (sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]) + (-cos(k[2]*s[2]) + 1)*(-k[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) - k[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) - k[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (k[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - k[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + k[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2];
    jac_v[2][6] = s[2]*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2])/k[2] + s[2]*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2])/k[2] + s[2]*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])/k[2] - ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/pow(k[2],2) - (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/pow(k[2],2) - ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/pow(k[2],2);
    jac_v[2][7] = ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(p[2])/k[2];
    jac_v[2][8] = ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2]) + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2]) + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]);


    Jv_ex << jac_v[0][0] << jac_v[0][1] << jac_v[0][2] << jac_v[0][3] << jac_v[0][4] << jac_v[0][5] << jac_v[0][6] << jac_v[0][7] << jac_v[0][8] << endr
	  << jac_v[1][0] << jac_v[1][1] << jac_v[1][2] << jac_v[1][3] << jac_v[1][4] << jac_v[1][5] << jac_v[1][6] << jac_v[1][7] << jac_v[1][8] << endr
	  << jac_v[2][0] << jac_v[2][1] << jac_v[2][2] << jac_v[2][3] << jac_v[2][4] << jac_v[2][5] << jac_v[2][6] << jac_v[2][7] << jac_v[2][8] << endr;
    return Jv_ex;
}

mat pass_Jkpsl(double l[3][3])
{
	double jac_kpsl[9][9];

	jac_kpsl[0][0] = 2*(l[0][0] - l[0][1]/2 - l[0][2]/2)/((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824)*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))) - 0.0268*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))/pow((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824),2);
	jac_kpsl[0][1] = 2*(-l[0][0]/2 + l[0][1] - l[0][2]/2)/((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824)*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))) - 0.0268*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))/pow((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824),2);
	jac_kpsl[0][2] = 2*(-l[0][0]/2 - l[0][1]/2 + l[0][2])/((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824)*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))) - 0.0268*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))/pow((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824),2);
	jac_kpsl[0][3] = 0.0;
	jac_kpsl[0][4] = 0.0;
	jac_kpsl[0][5] = 0.0;
	jac_kpsl[0][6] = 0.0;
	jac_kpsl[0][7] = 0.0;
	jac_kpsl[0][8] = 0.0;
	jac_kpsl[1][0] = -2*sqrt(3)/((1 + 3*pow((-2*l[0][0] + l[0][1] + l[0][2]),2)/pow((3*l[0][1] - 3*l[0][2]),2))*(3*l[0][1] - 3*l[0][2]));
	jac_kpsl[1][1] = (sqrt(3)/(3*l[0][1] - 3*l[0][2]) - 3*sqrt(3)*(-2*l[0][0] + l[0][1] + l[0][2])/pow((3*l[0][1] - 3*l[0][2]),2))/(1 + 3*pow((-2*l[0][0] + l[0][1] + l[0][2]),2)/pow((3*l[0][1] - 3*l[0][2]),2));
	jac_kpsl[1][2] = (sqrt(3)/(3*l[0][1] - 3*l[0][2]) + 3*sqrt(3)*(-2*l[0][0] + l[0][1] + l[0][2])/pow((3*l[0][1] - 3*l[0][2]),2))/(1 + 3*pow((-2*l[0][0] + l[0][1] + l[0][2]),2)/pow((3*l[0][1] - 3*l[0][2]),2));
	jac_kpsl[1][3] = 0.0;
	jac_kpsl[1][4] = 0.0;
	jac_kpsl[1][5] = 0.0;
	jac_kpsl[1][6] = 0.0;
	jac_kpsl[1][7] = 0.0;
	jac_kpsl[1][8] = 0.0;
	jac_kpsl[2][0] = 1.0/3.0;
	jac_kpsl[2][1] = 1.0/3.0;
	jac_kpsl[2][2] = 1.0/3.0;
	jac_kpsl[2][3] = 0.0;
	jac_kpsl[2][4] = 0.0;
	jac_kpsl[2][5] = 0.0;
	jac_kpsl[2][6] = 0.0;
	jac_kpsl[2][7] = 0.0;
	jac_kpsl[2][8] = 0.0;
	jac_kpsl[3][0] = 0.0;
	jac_kpsl[3][1] = 0.0;
	jac_kpsl[3][2] = 0.0;
	jac_kpsl[3][3] = 2*(l[1][0] - l[1][1]/2 - l[1][2]/2)/((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824)*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))) - 0.0268*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))/pow((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824),2);
	jac_kpsl[3][4] = 2*(-l[1][0]/2 + l[1][1] - l[1][2]/2)/((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824)*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))) - 0.0268*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))/pow((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824),2);
	jac_kpsl[3][5] = 2*(-l[1][0]/2 - l[1][1]/2 + l[1][2])/((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824)*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))) - 0.0268*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))/pow((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824),2);
	jac_kpsl[3][6] = 0.0;
	jac_kpsl[3][7] = 0.0;
	jac_kpsl[3][8] = 0.0;
	jac_kpsl[4][0] = 0.0;
	jac_kpsl[4][1] = 0.0;
	jac_kpsl[4][2] = 0.0;
	jac_kpsl[4][3] = -2*sqrt(3)/((1 + 3*pow((-2*l[1][0] + l[1][1] + l[1][2]),2)/pow((3*l[1][1] - 3*l[1][2]),2))*(3*l[1][1] - 3*l[1][2]));
	jac_kpsl[4][4] = (sqrt(3)/(3*l[1][1] - 3*l[1][2]) - 3*sqrt(3)*(-2*l[1][0] + l[1][1] + l[1][2])/pow((3*l[1][1] - 3*l[1][2]),2))/(1 + 3*pow((-2*l[1][0] + l[1][1] + l[1][2]),2)/pow((3*l[1][1] - 3*l[1][2]),2));
	jac_kpsl[4][5] = (sqrt(3)/(3*l[1][1] - 3*l[1][2]) + 3*sqrt(3)*(-2*l[1][0] + l[1][1] + l[1][2])/pow((3*l[1][1] - 3*l[1][2]),2))/(1 + 3*pow((-2*l[1][0] + l[1][1] + l[1][2]),2)/pow((3*l[1][1] - 3*l[1][2]),2));
	jac_kpsl[4][6] = 0.0;
	jac_kpsl[4][7] = 0.0;
	jac_kpsl[4][8] = 0.0;
	jac_kpsl[5][0] = 0.0;
	jac_kpsl[5][1] = 0.0;
	jac_kpsl[5][2] = 0.0;
	jac_kpsl[5][3] = 1.0/3.0;
	jac_kpsl[5][4] = 1.0/3.0;
	jac_kpsl[5][5] = 1.0/3.0;
	jac_kpsl[5][6] = 0.0;
	jac_kpsl[5][7] = 0.0;
	jac_kpsl[5][8] = 0.0;
	jac_kpsl[6][0] = 0.0;
	jac_kpsl[6][1] = 0.0;
	jac_kpsl[6][2] = 0.0;
	jac_kpsl[6][4] = 0.0;
	jac_kpsl[6][3] = 0.0;
	jac_kpsl[6][5] = 0.0;
	jac_kpsl[6][6] = 2*(l[2][0] - l[2][1]/2 - l[2][2]/2)/((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824)*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))) - 0.0268*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))/pow((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824),2);
	jac_kpsl[6][7] = 2*(-l[2][0]/2 + l[2][1] - l[2][2]/2)/((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824)*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))) - 0.0268*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))/pow((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824),2);
	jac_kpsl[6][8] = 2*(-l[2][0]/2 - l[2][1]/2 + l[2][2])/((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824)*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))) - 0.0268*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))/pow((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824),2);
	jac_kpsl[7][0] = 0.0;
	jac_kpsl[7][1] = 0.0;
	jac_kpsl[7][2] = 0.0;
	jac_kpsl[7][3] = 0.0;
	jac_kpsl[7][4] = 0.0;
	jac_kpsl[7][5] = 0.0;
	jac_kpsl[7][6] = -2*sqrt(3)/((1 + 3*pow((-2*l[2][0] + l[2][1] + l[2][2]),2)/pow((3*l[2][1] - 3*l[2][2]),2))*(3*l[2][1] - 3*l[2][2]));
	jac_kpsl[7][7] = (sqrt(3)/(3*l[2][1] - 3*l[2][2]) - 3*sqrt(3)*(-2*l[2][0] + l[2][1] + l[2][2])/pow((3*l[2][1] - 3*l[2][2]),2))/(1 + 3*pow((-2*l[2][0] + l[2][1] + l[2][2]),2)/pow((3*l[2][1] - 3*l[2][2]),2));
	jac_kpsl[7][8] = (sqrt(3)/(3*l[2][1] - 3*l[2][2]) + 3*sqrt(3)*(-2*l[2][0] + l[2][1] + l[2][2])/pow((3*l[2][1] - 3*l[2][2]),2))/(1 + 3*pow((-2*l[2][0] + l[2][1] + l[2][2]),2)/pow((3*l[2][1] - 3*l[2][2]),2));
	jac_kpsl[8][0] = 0.0;
	jac_kpsl[8][1] = 0.0;
	jac_kpsl[8][2] = 0.0;
	jac_kpsl[8][3] = 0.0;
	jac_kpsl[8][4] = 0.0;
	jac_kpsl[8][5] = 0.0;
	jac_kpsl[8][6] = 1.0/3.0;
	jac_kpsl[8][7] = 1.0/3.0;
	jac_kpsl[8][8] = 1.0/3.0;

	mat Jg_ex;
    Jg_ex << jac_kpsl[0][0] << jac_kpsl[0][1] << jac_kpsl[0][2] << jac_kpsl[0][3] << jac_kpsl[0][4] << jac_kpsl[0][5] << jac_kpsl[0][6] << jac_kpsl[0][7] << jac_kpsl[0][8] << endr
	  << jac_kpsl[1][0] << jac_kpsl[1][1] << jac_kpsl[1][2] << jac_kpsl[1][3] << jac_kpsl[1][4] << jac_kpsl[1][5] << jac_kpsl[1][6] << jac_kpsl[1][7] << jac_kpsl[1][8] << endr
	  << jac_kpsl[2][0] << jac_kpsl[2][1] << jac_kpsl[2][2] << jac_kpsl[2][3] << jac_kpsl[2][4] << jac_kpsl[2][5] << jac_kpsl[2][6] << jac_kpsl[2][7] << jac_kpsl[2][8] << endr
	  << jac_kpsl[3][0] << jac_kpsl[3][1] << jac_kpsl[3][2] << jac_kpsl[3][3] << jac_kpsl[3][4] << jac_kpsl[3][5] << jac_kpsl[3][6] << jac_kpsl[3][7] << jac_kpsl[3][8] << endr
	  << jac_kpsl[4][0] << jac_kpsl[4][1] << jac_kpsl[4][2] << jac_kpsl[4][3] << jac_kpsl[4][4] << jac_kpsl[4][5] << jac_kpsl[4][6] << jac_kpsl[4][7] << jac_kpsl[4][8] << endr
	  << jac_kpsl[5][0] << jac_kpsl[5][1] << jac_kpsl[5][2] << jac_kpsl[5][3] << jac_kpsl[5][4] << jac_kpsl[5][5] << jac_kpsl[5][6] << jac_kpsl[5][7] << jac_kpsl[5][8] << endr
	  << jac_kpsl[6][0] << jac_kpsl[6][1] << jac_kpsl[6][2] << jac_kpsl[6][3] << jac_kpsl[6][4] << jac_kpsl[6][5] << jac_kpsl[6][6] << jac_kpsl[6][7] << jac_kpsl[6][8] << endr
	  << jac_kpsl[7][0] << jac_kpsl[7][1] << jac_kpsl[7][2] << jac_kpsl[7][3] << jac_kpsl[7][4] << jac_kpsl[7][5] << jac_kpsl[7][6] << jac_kpsl[7][7] << jac_kpsl[7][8] << endr
	  << jac_kpsl[8][0] << jac_kpsl[8][1] << jac_kpsl[8][2] << jac_kpsl[8][3] << jac_kpsl[8][4] << jac_kpsl[8][5] << jac_kpsl[8][6] << jac_kpsl[8][7] << jac_kpsl[8][8] << endr;

    return Jg_ex;
}


mat pass_Js(mat A, mat B)
{
  return join_rows(A, B);
}

double find_magnitude(mat vect)
{
  double mag;
	mag = sqrt(pow(vect(0,0),2)+pow(vect(1,0),2)+pow(vect(2,0),2));
  return mag;
}


mat homogeneous_transform(double kappa_sym, double phi_sym, double s_sym)
{
  mat T;
  T << cos(phi_sym)*cos(kappa_sym*s_sym) << -sin(phi_sym) << cos(phi_sym)*sin(kappa_sym*s_sym) << cos(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym << endr
    << sin(phi_sym)*cos(kappa_sym*s_sym) << cos(phi_sym) << sin(phi_sym)*sin(kappa_sym*s_sym) << sin(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym << endr
    << -sin(kappa_sym*s_sym) << 0 << cos(kappa_sym*s_sym) << sin(kappa_sym*s_sym)/kappa_sym << endr
    << 0 << 0 << 0 << 1 << endr;
  return T;
}

mat homogeneous_transform_base(double x, double y, double z, double alpha, double beta, double gamma)
{
  mat Tb;
  Tb << cos(alpha)*cos(beta) << cos(alpha)*sin(beta)*sin(gamma)-sin(alpha)*cos(gamma) << cos(alpha)*sin(beta)*cos(gamma)+sin(alpha)*sin(gamma) << x << endr
     << sin(alpha)*cos(beta) << sin(alpha)*sin(beta)*sin(gamma)+cos(alpha)*cos(gamma) << sin(alpha)*sin(beta)*cos(gamma)-cos(alpha)*sin(gamma) << y << endr
     << -sin(beta) << cos(beta)*sin(gamma) << cos(beta)*cos(gamma) << z << endr
     << 0 << 0 << 0 << 1 << endr;
    return Tb;
}

geometry_msgs::Pose homo_to_pose(mat T)
{
    geometry_msgs::Pose pose;
    pose.position.x = T(0,3);
    pose.position.y = T(1,3);
    pose.position.z = T(2,3);
    pose.orientation.w = 0.5*sqrt(1+T(0,0)+T(1,1)+T(2,2));
    pose.orientation.x = (T(2,1)-T(1,2))/(4*pose.orientation.w);
    pose.orientation.y = (T(0,2)-T(2,0))/(4*pose.orientation.w);
    pose.orientation.z = (T(1,0)-T(0,1))/(4*pose.orientation.w);

    return pose;
}

mat homo_to_pose_sym(mat T)
{
  mat position;
  position << T(0,3) << endr
	   << T(1,3) << endr
	   << T(2,3) << endr;
  return position;
}

mat homo_to_R(mat T)
{
  mat dcm;
  dcm = T(span(0,2),span(0,2));

  return dcm;
}

mat zeros_mat()
{
  mat zero_mat;
  zero_mat << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr;

  return zero_mat;
}

mat zeros_mat_field()
{
  mat zero_mat;
  zero_mat << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr;

  return zero_mat;
}

mat zeros_mat_base()
{
  mat zero_mat;
  zero_mat << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr;

  return zero_mat;
}

mat obstacle_field(mat pose_of_sensor, unsigned char obs_index, unsigned char ind_sensor)
{
  double d = 0.0134; // radius of manipulator
  double speed;
  mat velocity, stiff_mat;
  mat vector_obs = pose_of_sensor - obs[obs_index];
  mat dummy = vector_obs.t()*vector_obs;
  double rho = sqrt((double)(dummy(0,0)));
  double rho_modify = rho - d - obs_range;
  if(rho_modify < mini) mini = rho_modify;
  stiff_mat << stiff_vec << 0 << 0 << endr
	    << 0 << stiff_vec << 0 << endr
	    << 0 << 0 << stiff_vec << endr;
  mat ident = eye<mat>(3,3);

  if(field_type == 0)
  {
    if(rho_modify<rho_0) velocity = K2*stiff_mat*(vector_obs/rho)*(1/rho_modify - 1/rho_0)/pow(rho_modify,2);
    else velocity = zeros_mat_field();
  }
  else if(field_type == 1)
  {
    if(rho_modify<rho_0)
    {
//       if(rho_modify<=(d+obs_range))
//       {
// // 	cout << "rho:" << rho_modify << endl;
// 	rho_modify = d+obs_range;
//       }
      if(rho_modify<=0.015)
      {
// 	cout << "rho:" << rho_modify << endl;
	rho_modify = 0.015;
      }
      speed = K2*stiff_vec*(1/rho_modify - 1/rho_0)/pow(rho_modify,2);
      if(speed > maks)
      {
	maks = speed;
// 	cout << "maks: " << maks << endl;
      }
      velocity = speed*ident*(vector_obs/rho);
    }
    else velocity = zeros_mat_field();
  }
  else if(field_type == 2)
  {
    if((vector_obs(0,0)!=0) and (vector_obs(1,0)!=0))
    {

      // Initial Current direction
      // obs_cur << 0.0 << endr
	    //   << -1.0 << endr
	    //   << 0.0 << endr;
			obs_cur = -tangent;
      if(rho_modify<rho_0)
      {
				flag_obs++;
	// Current generation on the robot
	// Current = tangent
	agent_cur = tangent;

	// New approach - untested
	// obs_cur = -1*(agent_cur - (dot(agent_cur, vector_obs)/find_magnitude(vector_obs))*(vector_obs/find_magnitude(vector_obs)));

	// obs_cur = obs_cur/find_magnitude(obs_cur);

	obs_sensor = vector_obs/find_magnitude(vector_obs);
	// obs_sensor.print("obs_sensor_dir:");
	// cout << "sensor_in" << int(ind_sensor) << endl;
// 	if(twist_mag!=0 && ind_sensor==8)
	// Magnetic Field generation
	lc_cross << 0.0 << -obs_cur(2,0) << obs_cur(1,0) << endr
		 << obs_cur(2,0) << 0.0 << -obs_cur(0,0) << endr
		 << -obs_cur(1,0) << obs_cur(0,0) << 0.0 << endr;
// 	B = lc_cross*agent_cur*twist_mag/pow(rho_modify,2);
	B = lc_cross*obs_sensor*(1/rho_modify - 1/rho_0)/pow(rho_modify,2);
	// B = lc_cross*obs_sensor*(1/rho_modify - 1/rho_0);

	// Force acting on a robot
	la_cross << 0.0 << -agent_cur(2,0) << agent_cur(1,0) << endr
		 << agent_cur(2,0) << 0.0 << -agent_cur(0,0) << endr
		 << -agent_cur(1,0) << agent_cur(0,0) << 0.0 << endr;

	// velocity = K3*la_cross*B;
	velocity = K2*la_cross*B;

	// B.print("B: ");
	// agent_cur.print("agent_cur: ");
	// velocity.print("field: ");
      }
      else velocity = zeros_mat_field();
    }
  }

	else if(field_type == 3)
  {
    if((vector_obs(0,0)!=0) and (vector_obs(1,0)!=0))
    {

      // Initial Current direction
      obs_cur << 0.0 << endr
	      << 1.0 << endr
	      << 0.0 << endr;
      if(rho_modify<rho_0)
      {
	// Current generation on the robot
	// Current = twist
	agent_cur << twist.vector[obs_num*ind_sensor+obs_index].x << endr
		  << twist.vector[obs_num*ind_sensor+obs_index].y << endr
		  << twist.vector[obs_num*ind_sensor+obs_index].z << endr;
	twist_mag = find_magnitude(agent_cur);
// 	if(twist_mag!=0 && ind_sensor==8)
	if(twist_mag>0.001)
	{
	agent_cur = agent_cur/(twist_mag);
// 	agent_cur.print("agent_cur: ");
	// Magnetic Field generation
	lc_cross << 0.0 << -obs_cur(2,0) << obs_cur(1,0) << endr
		 << obs_cur(2,0) << 0.0 << -obs_cur(0,0) << endr
		 << -obs_cur(1,0) << obs_cur(0,0) << 0.0 << endr;
// 	B = lc_cross*agent_cur*twist_mag/pow(rho_modify,2);
	B = lc_cross*agent_cur*twist_mag*(1/rho_modify - 1/rho_0);

	// Force acting on a robot
	la_cross << 0.0 << -agent_cur(2,0) << agent_cur(1,0) << endr
		 << agent_cur(2,0) << 0.0 << -agent_cur(0,0) << endr
		 << -agent_cur(1,0) << agent_cur(0,0) << 0.0 << endr;

	velocity = K3*la_cross*B;

	B.print("B: ");
	velocity.print("field: ");
// 	cout << "obstacle: " << int(obs_index) << endl;
// 	cout << "sensor: " << int(ind_sensor) << endl;
// 	velocity.print("F=");

	}
	else
	{
	  velocity = zeros_mat_field();
	}
      }
      else velocity = zeros_mat_field();
    }

  }
	else if(field_type == 4)
  {
    if((vector_obs(0,0)!=0) and (vector_obs(1,0)!=0))
    {

      // Initial Current direction
      // obs_cur << 0.0 << endr
	    //   << 1.0 << endr
	    //   << 0.0 << endr;
      if(rho_modify<rho_0)
      {
				flag_obs++;
				// Current generation on the robot
				// Current = twist
				agent_cur << twist.vector[obs_num*ind_sensor+obs_index].x << endr
					  << twist.vector[obs_num*ind_sensor+obs_index].y << endr
					  << twist.vector[obs_num*ind_sensor+obs_index].z << endr;

				// cout << "index_twist: " << int(obs_num*ind_sensor+obs_index) << endl;

				twist_mag = find_magnitude(agent_cur);
				// cout << "twist_mag: " << twist_mag << endl;
			// 	if(twist_mag!=0 && ind_sensor==8)
				if(twist_mag!=0.0)
				{
					agent_cur = agent_cur/(twist_mag);
          obs_cur = agent_cur - (dot(agent_cur, vector_obs)/find_magnitude(vector_obs))*(vector_obs/find_magnitude(vector_obs));
					// obs_cur = -1*(agent_cur - (dot(agent_cur, vector_obs)/find_magnitude(vector_obs))*(vector_obs/find_magnitude(vector_obs)));

          obs_cur = obs_cur/find_magnitude(obs_cur);// 	agent_cur.print("agent_cur: ");
					// Magnetic Field generation// cout << "sensor_in" << int(ind_sensor) << endl;
					lc_cross << 0.0 << -obs_cur(2,0) << obs_cur(1,0) << endr
						 << obs_cur(2,0) << 0.0 << -obs_cur(0,0) << endr
						 << -obs_cur(1,0) << obs_cur(0,0) << 0.0 << endr;
				// 	B = lc_cross*agent_cur*twist_mag/pow(rho_modify,2);
					// fun = (1/rho_modify - 1/rho_02)/pow(rho_modify,2);
					fun = (1/rho_modify);
					if(fun>field_max)
					{
						field_max = fun;
						dist_min = rho_modify;
						twist_max = twist_mag;
						cout << "dist: " << dist_min << endl;
						cout << "twist: " << twist_max << endl;
						cout << "field: " << 0.01*field_max*twist_max << endl;
					}
					B = lc_cross*agent_cur*twist_mag*fun;
					// B = lc_cross*agent_cur*(1/rho_modify - 1/rho_02)/pow(rho_modify,2);
					// B = lc_cross*agent_cur*twist_mag*(1/rho_modify);
					// Force acting on a robot
					la_cross << 0.0 << -agent_cur(2,0) << agent_cur(1,0) << endr
						 << agent_cur(2,0) << 0.0 << -agent_cur(0,0) << endr
						 << -agent_cur(1,0) << agent_cur(0,0) << 0.0 << endr;

					velocity = 0.01*la_cross*B*dt;
					
					// velocity = K2*la_cross*B;

					// B.print("B: ");
					// velocity.print("field: ");
					// obs_cur.print("obs_cur: ");
					// agent_cur.print("agent_cur: ");
					// cout << "sensor_in: " << int(ind_sensor) << endl;
					// vector_obs.print("vector_obs");
				// 	cout << "obstacle: " << int(obs_index) << endl;
				// 	cout << "sensor: " << int(ind_sensor) << endl;
				// 	velocity.print("F=");

				}
				else
				{
				  velocity = zeros_mat_field();
				}
      }
      else
			{
				velocity = zeros_mat_field();
			}
    }

  }
	if(rho_modify>=2*rho_0) redundancy_weight = 0;
  else if(rho_modify>rho_0 && rho_modify<2*rho) redundancy_weight = pow(cos((M_PI/2)*((rho_modify-rho_0)/(rho_0))),2);
  else redundancy_weight = 1;

  return velocity;
}

float get_dist(mat pose_of_sensor, unsigned char obs_index)
{
  float d = 0.0134; // radius of manipulator
  mat distance;
  mat vector_obs = pose_of_sensor - obs[obs_index];
  mat dummy = vector_obs.t()*vector_obs;
  float rho = sqrt((float)(dummy(0,0)));
  float rho_modify = rho - d - obs_range;

  return rho_modify;
}

void get_obs(const geometry_msgs::PoseArray::ConstPtr& msg)
{
  geometry_msgs::PoseArray env;
  env = *msg;

  for(unsigned char i=0; i<obs_num; i++)
  {
    obs[i] << env.poses[i].position.x << endr
	   << env.poses[i].position.y << endr
	   << env.poses[i].position.z << endr;
  }
}

void get_kps(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray kps;
  kps = *msg;
  if(base == 1)
  {
    for(unsigned char i=0; i<3; i++)
    {
      position_base[i] = kps.data[i];
      angle_base[i] = kps.data[3+i];
      k[i] = kps.data[6+3*i];
      p[i] = kps.data[7+3*i];
      s[i] = kps.data[8+3*i];
    }
  }
  else
  {
    for(unsigned char i=0; i<3; i++)
    {
      k[i] = kps.data[3*i];
      p[i] = kps.data[1+3*i];
      s[i] = kps.data[2+3*i];
    }
  }
}

void vel_callback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray vel;
  vel = *msg;
  go_to_goal << vel.data[0] << endr
	     << vel.data[1] << endr
	     << vel.data[2] << endr;
}

void get_stiff(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray stiff;
  stiff = *msg;
  stiff_vec = stiff.data[0];
}

void twist_callback(const continuum_manipulator::Vector3Array::ConstPtr& msg)
{
  twist = *msg;
}

void initialize()
{
  field_sum << 0.0 << endr
	    << 0.0 << endr
	    << 0.0 << endr;
  for(unsigned char i=0; i<obs_num; i++)
  {
    obs[i] << 1.0 << endr
	   << 1.0 << endr
	   << 1.0 << endr;
  }
  go_to_goal << 0.0 << endr
	     << 0.0 << endr
	     << 0.0 << endr;
  k[0] = 0.706325262037712; k[1] = 0.706325262037712; k[2] = 0.706325262037712;
  p[0] = 2.09439510239320; p[1] = -1.04719755119660; p[2] = 2.09439510239320;
  s[0] = 0.122; s[1] = 0.122; s[2] = 0.122;
  for(unsigned char i=0; i<3; i++)
  {
    position_base[i] = 0.0;
    angle_base[i] = 0.0;
  }
  stiff_vec = 1;
  tangent_init << 0.0 << endr
	       << 0.0 << endr
	       << 1.0 << endr;
}

void calc_sensor_jacobian(unsigned char segment_num, unsigned char sensor_num)
{
  double k_pass[3], p_pass[3], s_pass[3];
    if(segment_num == 1)
    {
      k_pass[0] = k[0]; k_pass[1] = 0.0; k_pass[2] = 0.0;
      p_pass[0] = p[0]; p_pass[1] = 0.0; p_pass[2] = 0.0;
      s_pass[0] = (sensor_num+1)*s[segment_num-1]/(num_per_seg); s_pass[1] = 0.0; s_pass[2] = 0.0;
    }
    else if(segment_num == 2)
    {
      k_pass[0] = k[0]; k_pass[1] = k[1]; k_pass[2] = 0.0;
      p_pass[0] = p[0]; p_pass[1] = p[1]; p_pass[2] = 0.0;
      s_pass[0] = s[0]; s_pass[1] = (sensor_num+1)*s[segment_num-1]/(num_per_seg); s_pass[2] = 0.0;
    }
    else if(segment_num == 3)
    {
      k_pass[0] = k[0]; k_pass[1] = k[1]; k_pass[2] = k[2];
      p_pass[0] = p[0]; p_pass[1] = p[1]; p_pass[2] = p[2];
      s_pass[0] = s[0]; s_pass[1] = s[1]; s_pass[2] = (sensor_num+1)*s[segment_num-1]/(num_per_seg);
    }
    if(base == 1) sensor_jacobian = pass_Js(pass_Jvb(angle_base, k, p_pass, s_pass), pass_Jv(angle_base, k, p_pass, s_pass));
    else
    {
      double angle_dummy[3] = {0.0, 0.0, 0.0};
      sensor_jacobian = pass_Jv(angle_dummy, k, p_pass, s_pass);
			for(unsigned char in_seg=segment_num; in_seg<3; in_seg++)
		  {
		    sensor_jacobian(0, 3*(in_seg)+2) = 0;
		    sensor_jacobian(1, 3*(in_seg)+2) = 0;
				sensor_jacobian(2, 3*(in_seg)+2) = 0;
		  }
    }

}

int
main(int argc, char** argv)
  {
    std_msgs::Float64MultiArray send_obs, send_obs_sens, send_dist;
    geometry_msgs::PoseArray sensor_pose;
    mat sensor_matrix, velocity_field, sensor_;
    vector<float> dist_to_obs;
		continuum_manipulator::JacobianField jacobi_sent;
		std_msgs::Float64 redundancy_sent;
		mat jacobian_closest;
    ros::init(argc,argv, "sensor_pose_node");
    ros::NodeHandle n;
    n.param("freq", freq_int, freq_int);
    n.param("fieldtype", field_type, field_type);
    n.param("obs_num", obs_num, obs_num);
    n.param("base", base, base);
    n.param("kalman_filter", observer, observer);
		n.param("length_on", length_active, length_active);
    initialize();
    mat list_of_field[3*num_per_seg*obs_num];
		mat field_sens_close;
		double redundancy_weight_close;
    if(freq_int!=0)
    {
      freq = (double)(freq_int);
    }
    ros::Rate loop_rate(freq);

    ros::Publisher sensor_pub = n.advertise<geometry_msgs::PoseArray>("sensor_pose",10);
//     ros::Publisher vel_obs_pub = n.advertise<std_msgs::Float64MultiArray>("motion_plan/obs_field/cspace",10);
    ros::Publisher vel_obs_pub = n.advertise<std_msgs::Float64MultiArray>("obs_field_configuration_space",10);
    ros::Publisher vel_sensor_pub = n.advertise<std_msgs::Float64MultiArray>("obs_field_task_space",10);
//     ros::Publisher vel_sensor_pub = n.advertise<std_msgs::Float64MultiArray>("motion_plan/obs_field/tspace",10);
    ros::Publisher dist_obs_pub = n.advertise<std_msgs::Float64MultiArray>("min_distance",10);
		ros::Publisher jacobi_pub = n.advertise<continuum_manipulator::JacobianField>("sensor_jacobi",10);
		ros::Publisher redundancy_pub = n.advertise<std_msgs::Float64>("redundancy_weight",10);

		ros::Subscriber obs_sub = n.subscribe("obs_pose", 10, get_obs);
//     ros::Subscriber config_space_sub = n.subscribe("observer/cspace/estimate", 10, get_kps);
      ros::Subscriber config_space_sub = n.subscribe("configuration_space", 10, get_kps);
//     ros::Subscriber rec_vel = n.subscribe("motion_plan/goal_field", 10, vel_callback);
    ros::Subscriber rec_vel = n.subscribe("/field", 10, vel_callback);
		ros::Subscriber rec_tw = n.subscribe("/twist", 10, twist_callback);
//     ros::Subscriber stiff_sub = n.subscribe("stiffness/obs", 10, get_stiff);

//     obs[0].print();
		field_max = 0.0;
		dist_min = 100.0;
		fun = 0.0;
    while(ros::ok())
    {
      sensor_ = eye<mat>(4,4);
      sensor_pose.poses.clear();
      send_obs_sens.data.clear();
      dist_to_obs.clear();
      send_dist.data.clear();
      if(base == 1)
      {
				sensor_ = sensor_*homogeneous_transform_base(position_base[0], position_base[1], position_base[2], angle_base[0], angle_base[1], angle_base[2]);
				field_sum = zeros_mat_base();
      }
      else
      {
				field_sum = zeros_mat();
      }

      float maxi = 10.0;
      unsigned char sensor_max, obs_max, index_max, index=0;
			unsigned char sensor_ind = 0;
			field_sens_close.set_size(3,1);
      field_sens_close.zeros();
      for(unsigned char i=0; i<3; i++)
      {
				for(unsigned char j=0; j<num_per_seg; j++)
				{
				  sensor_matrix = sensor_*homogeneous_transform(k[i], p[i], (j+1)*s[i]/(num_per_seg));
				  sensor_pose.poses.push_back(homo_to_pose(sensor_matrix));

				  tangent = homo_to_R(sensor_matrix)*tangent_init;

				  calc_sensor_jacobian(i+1,j);
				  field_sum_sens = zeros_mat_field();
				  for(unsigned char h = 0; h<obs_num; h++)
				  {
				    velocity_field = obstacle_field(homo_to_pose_sym(sensor_matrix),h, sensor_ind);
				    dist_to_obs.push_back(get_dist(homo_to_pose_sym(sensor_matrix),h));
				    if((field_type==1))
				    {
				      field_sum += pinv(sensor_jacobian)*velocity_field;
				      field_sum_sens += velocity_field;
				    }
				    else
				    {
			// 	      cout << field_type << endl;
				      if(get_dist(homo_to_pose_sym(sensor_matrix),h) < maxi)
				      {
								maxi = get_dist(homo_to_pose_sym(sensor_matrix),h);
								sensor_max = j;
								obs_max = h;
								index_max = index;
								field_sum_sens = velocity_field;
								field_sens_close = velocity_field;
								field_sum = pinv(sensor_jacobian)*velocity_field;
								jacobian_closest = sensor_jacobian;
								redundancy_weight_close = redundancy_weight;
				      }
				      index++;
				    }
	  			}

					sensor_ind++;
				  for(unsigned char h = 0; h<3; h++)
				  {
				    send_obs_sens.data.push_back(field_sum_sens(h,0));
				  }
				}
				sensor_ = sensor_matrix;
      }
			if(flag_obs>0) flag_print = 1;
			else
			{
				flag_print = 0;
			}
			flag_obs = 0;
			// cout << "obs_close:" << int(index_max) << endl;
			// field_sens_close.print("field:");
			// cout << flag_obs << endl;
			// if(flag_print == 1)
			// {
			// 	// B.print("B: ");
			// 	// field_sens_close.print("field: ");
			// 	// for(unsigned char i=0;i<3;i++)
			// 	// {
			// 	//
			// 	//
			// 	// }
			// 	// obs_cur.print("obs_cur: ");
			// 	// agent_cur.print("agent_cur: ");
			// 	cout << "sensor_in: " << int(index_max) << endl;
			// }
			// if(maxi<dist_min)
			// {
			// 	dist_min = maxi;
			// 	cout << "dist: " << dist_min << endl;
			// 	cout << "twist: " << twist_mag << endl;
			// 	cout << "field: " << field_max << endl;
			// }
			// vector_obs.print("vector_obs");

			jacobi_sent.jacobian.row = jacobian_closest.n_rows;
      jacobi_sent.jacobian.column = jacobian_closest.n_cols;
      jacobi_sent.jacobian.matrix_data.clear();
      for(unsigned char i=0; i<jacobi_sent.jacobian.row; i++)
      {
				for(unsigned char j=0; j<jacobi_sent.jacobian.column; j++)
				{
				  jacobi_sent.jacobian.matrix_data.push_back(jacobian_closest(i,j));
				}
      }
      jacobi_sent.vector_field.x = field_sens_close(0,0);
      jacobi_sent.vector_field.y = field_sens_close(1,0);
			jacobi_sent.vector_field.z = field_sens_close(2,0);


      float max = 0.0;
      for(unsigned char i=0; i<(num_per_seg*obs_num); i++)
      {
				if(dist_to_obs[i] > max) max = dist_to_obs[i];
      }
      send_dist.data.push_back(max);

      send_obs.data.clear();
      unsigned char state_num;
      if(base==0) state_num = 9;
      else state_num = 15;
      for(unsigned char i=0; i<state_num; i++)
      {
				send_obs.data.push_back(field_sum(i,0));

      }

			redundancy_sent.data = redundancy_weight_close;
      sensor_pose.header.stamp = ros::Time::now();
      vel_obs_pub.publish(send_obs);
      sensor_pub.publish(sensor_pose);
      vel_sensor_pub.publish(send_obs_sens);
      dist_obs_pub.publish(send_dist);
			jacobi_pub.publish(jacobi_sent);
      redundancy_pub.publish(redundancy_sent);
      ros::spinOnce();
      loop_rate.sleep();
    }
    return 0;
  }
