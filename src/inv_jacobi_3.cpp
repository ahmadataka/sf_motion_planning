#include "ros/ros.h"
#include "geometry_msgs/Pose.h"
#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/Float64.h"
#include "geometry_msgs/Pose.h"
#include "continuum_manipulator/JacobianField.h"
#include <iostream>
#include <armadillo>
#include <math.h>
// #include <random>

using namespace std;
using namespace arma;

// Armadillo documentation is available at:
// http://arma.sourceforge.net/docs.html

// It is IMPORTANT to be noted that l variable here stands for change of length
// Global Variable
double d = 0.0134; // diameter of manipulator
double freq = 40.0;
double dt = 1.0/freq;
// double dt = 0.01;
double Lo = 0.1200;
// double y[] = {0.040266536742486536, 0.044424972370305314, 0.04349848893400116, 0.03942309472635675, 0.040451768913697374, 0.039035182095829186, 0.03038409332226508, 0.0065335041493895804, 0.07039087784251256};
// double y[] = {0.040266536742486536, 0.054424972370305314, 0.04349848893400116, 0.03942309472635675, 0.045451768913697374, 0.035035182095829186, 0.03038409332226508, 0.0065335041493895804, 0.07039087784251256};
// double y[] = {0.06131908774484627, 0.05258922075448585, 0.04783115212999886, 0.04500457028944119, 0.05089275569302214, 0.04739764336177495, 0.027868696045970354, 0.01753723985543397, 0.08513967374092057};
// double y[] = {0.0010961461242604085, 0.003669162858767959, 0.0033936725085199687, 0.0027723968909232144, 0.002462343137465879, 0.0017726613076709415, 0.0003171599566899583, -0.0007575515222707968, 0.006744804762127911};
double y[] = {0.002185073211087172, 0.013844107151657448, 0.02127896636075233, 0.011625708022642836, -0.0015575909147796168, 0.026584259563230038, 0.009536396107657672, 0.011578932751209454, 0.014420456620548895};
double len[3][3], l_noise[9], rate[9];
double k_val[3], p_val[3], s_val[3];
double k_noi, p_noi, s_noi;
double K_config = 0.005;
unsigned char flag_error;
int noise_flag;
double  stiff_vec;
double s_min = -0.3*Lo, s_max = 0.3*Lo, s_lim = 0.005;
static int guide, flag_move;
mat e_vector, path_vel, v_obs, fields, noise_mat, v_config, fields_euler, euler;
mat Jv, Jvb, Jkpsl, Ju, Jw;
int flag_try = 0;
std_msgs::Float64MultiArray config_field;
ros::Time begin, current;
int freq_int;
mat sensor_jacobi, sensor_jacobi_raw, obstacle_field;
double red_weight;
int flag_jacobi = 0;
static mat obstacle_term;
static mat goal_term;
mat field_out;
int length_active, orient_active, euler_flag, field_type;

double l_to_k(double l1, double l2, double l3)
{
    double k_out = 2*(pow(pow(l1,2)+pow(l2,2)+pow(l3,2)-l1*l2-l1*l3-l2*l3,0.5))/(d*(3*Lo+l1+l2+l3));

    return k_out;
}

double l_to_phi(double l_1, double l_2, double l_3)
{
    double phi_out = atan2((sqrt(3)*(l_2+l_3-2*l_1)),(3*(l_2-l_3)));

    return phi_out;
}

double l_to_s(double l1, double l2, double l3)
{
    double s_out = (3*Lo+l1+l2+l3)/3;

    return s_out;
}

mat pass_Jkpsl(double l[3][3])
{
	double jac_kpsl[9][9];

	jac_kpsl[0][0] = 2*(l[0][0] - l[0][1]/2 - l[0][2]/2)/((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824)*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))) - 0.0268*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))/pow((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824),2);
	jac_kpsl[0][1] = 2*(-l[0][0]/2 + l[0][1] - l[0][2]/2)/((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824)*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))) - 0.0268*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))/pow((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824),2);
	jac_kpsl[0][2] = 2*(-l[0][0]/2 - l[0][1]/2 + l[0][2])/((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824)*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))) - 0.0268*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))/pow((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824),2);
	jac_kpsl[0][3] = 0.0;
	jac_kpsl[0][4] = 0.0;
	jac_kpsl[0][5] = 0.0;
	jac_kpsl[0][6] = 0.0;
	jac_kpsl[0][7] = 0.0;
	jac_kpsl[0][8] = 0.0;
	jac_kpsl[1][0] = -2*sqrt(3)/((1 + 3*pow((-2*l[0][0] + l[0][1] + l[0][2]),2)/pow((3*l[0][1] - 3*l[0][2]),2))*(3*l[0][1] - 3*l[0][2]));
	jac_kpsl[1][1] = (sqrt(3)/(3*l[0][1] - 3*l[0][2]) - 3*sqrt(3)*(-2*l[0][0] + l[0][1] + l[0][2])/pow((3*l[0][1] - 3*l[0][2]),2))/(1 + 3*pow((-2*l[0][0] + l[0][1] + l[0][2]),2)/pow((3*l[0][1] - 3*l[0][2]),2));
	jac_kpsl[1][2] = (sqrt(3)/(3*l[0][1] - 3*l[0][2]) + 3*sqrt(3)*(-2*l[0][0] + l[0][1] + l[0][2])/pow((3*l[0][1] - 3*l[0][2]),2))/(1 + 3*pow((-2*l[0][0] + l[0][1] + l[0][2]),2)/pow((3*l[0][1] - 3*l[0][2]),2));
	jac_kpsl[1][3] = 0.0;
	jac_kpsl[1][4] = 0.0;
	jac_kpsl[1][5] = 0.0;
	jac_kpsl[1][6] = 0.0;
	jac_kpsl[1][7] = 0.0;
	jac_kpsl[1][8] = 0.0;
	jac_kpsl[2][0] = 1.0/3.0;
	jac_kpsl[2][1] = 1.0/3.0;
	jac_kpsl[2][2] = 1.0/3.0;
	jac_kpsl[2][3] = 0.0;
	jac_kpsl[2][4] = 0.0;
	jac_kpsl[2][5] = 0.0;
	jac_kpsl[2][6] = 0.0;
	jac_kpsl[2][7] = 0.0;
	jac_kpsl[2][8] = 0.0;
	jac_kpsl[3][0] = 0.0;
	jac_kpsl[3][1] = 0.0;
	jac_kpsl[3][2] = 0.0;
	jac_kpsl[3][3] = 2*(l[1][0] - l[1][1]/2 - l[1][2]/2)/((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824)*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))) - 0.0268*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))/pow((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824),2);
	jac_kpsl[3][4] = 2*(-l[1][0]/2 + l[1][1] - l[1][2]/2)/((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824)*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))) - 0.0268*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))/pow((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824),2);
	jac_kpsl[3][5] = 2*(-l[1][0]/2 - l[1][1]/2 + l[1][2])/((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824)*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))) - 0.0268*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))/pow((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824),2);
	jac_kpsl[3][6] = 0.0;
	jac_kpsl[3][7] = 0.0;
	jac_kpsl[3][8] = 0.0;
	jac_kpsl[4][0] = 0.0;
	jac_kpsl[4][1] = 0.0;
	jac_kpsl[4][2] = 0.0;
	jac_kpsl[4][3] = -2*sqrt(3)/((1 + 3*pow((-2*l[1][0] + l[1][1] + l[1][2]),2)/pow((3*l[1][1] - 3*l[1][2]),2))*(3*l[1][1] - 3*l[1][2]));
	jac_kpsl[4][4] = (sqrt(3)/(3*l[1][1] - 3*l[1][2]) - 3*sqrt(3)*(-2*l[1][0] + l[1][1] + l[1][2])/pow((3*l[1][1] - 3*l[1][2]),2))/(1 + 3*pow((-2*l[1][0] + l[1][1] + l[1][2]),2)/pow((3*l[1][1] - 3*l[1][2]),2));
	jac_kpsl[4][5] = (sqrt(3)/(3*l[1][1] - 3*l[1][2]) + 3*sqrt(3)*(-2*l[1][0] + l[1][1] + l[1][2])/pow((3*l[1][1] - 3*l[1][2]),2))/(1 + 3*pow((-2*l[1][0] + l[1][1] + l[1][2]),2)/pow((3*l[1][1] - 3*l[1][2]),2));
	jac_kpsl[4][6] = 0.0;
	jac_kpsl[4][7] = 0.0;
	jac_kpsl[4][8] = 0.0;
	jac_kpsl[5][0] = 0.0;
	jac_kpsl[5][1] = 0.0;
	jac_kpsl[5][2] = 0.0;
	jac_kpsl[5][3] = 1.0/3.0;
	jac_kpsl[5][4] = 1.0/3.0;
	jac_kpsl[5][5] = 1.0/3.0;
	jac_kpsl[5][6] = 0.0;
	jac_kpsl[5][7] = 0.0;
	jac_kpsl[5][8] = 0.0;
	jac_kpsl[6][0] = 0.0;
	jac_kpsl[6][1] = 0.0;
	jac_kpsl[6][2] = 0.0;
	jac_kpsl[6][4] = 0.0;
	jac_kpsl[6][3] = 0.0;
	jac_kpsl[6][5] = 0.0;
	jac_kpsl[6][6] = 2*(l[2][0] - l[2][1]/2 - l[2][2]/2)/((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824)*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))) - 0.0268*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))/pow((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824),2);
	jac_kpsl[6][7] = 2*(-l[2][0]/2 + l[2][1] - l[2][2]/2)/((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824)*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))) - 0.0268*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))/pow((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824),2);
	jac_kpsl[6][8] = 2*(-l[2][0]/2 - l[2][1]/2 + l[2][2])/((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824)*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))) - 0.0268*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))/pow((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824),2);
	jac_kpsl[7][0] = 0.0;
	jac_kpsl[7][1] = 0.0;
	jac_kpsl[7][2] = 0.0;
	jac_kpsl[7][3] = 0.0;
	jac_kpsl[7][4] = 0.0;
	jac_kpsl[7][5] = 0.0;
	jac_kpsl[7][6] = -2*sqrt(3)/((1 + 3*pow((-2*l[2][0] + l[2][1] + l[2][2]),2)/pow((3*l[2][1] - 3*l[2][2]),2))*(3*l[2][1] - 3*l[2][2]));
	jac_kpsl[7][7] = (sqrt(3)/(3*l[2][1] - 3*l[2][2]) - 3*sqrt(3)*(-2*l[2][0] + l[2][1] + l[2][2])/pow((3*l[2][1] - 3*l[2][2]),2))/(1 + 3*pow((-2*l[2][0] + l[2][1] + l[2][2]),2)/pow((3*l[2][1] - 3*l[2][2]),2));
	jac_kpsl[7][8] = (sqrt(3)/(3*l[2][1] - 3*l[2][2]) + 3*sqrt(3)*(-2*l[2][0] + l[2][1] + l[2][2])/pow((3*l[2][1] - 3*l[2][2]),2))/(1 + 3*pow((-2*l[2][0] + l[2][1] + l[2][2]),2)/pow((3*l[2][1] - 3*l[2][2]),2));
	jac_kpsl[8][0] = 0.0;
	jac_kpsl[8][1] = 0.0;
	jac_kpsl[8][2] = 0.0;
	jac_kpsl[8][3] = 0.0;
	jac_kpsl[8][4] = 0.0;
	jac_kpsl[8][5] = 0.0;
	jac_kpsl[8][6] = 1.0/3.0;
	jac_kpsl[8][7] = 1.0/3.0;
	jac_kpsl[8][8] = 1.0/3.0;

	mat Jg_ex;
    Jg_ex << jac_kpsl[0][0] << jac_kpsl[0][1] << jac_kpsl[0][2] << jac_kpsl[0][3] << jac_kpsl[0][4] << jac_kpsl[0][5] << jac_kpsl[0][6] << jac_kpsl[0][7] << jac_kpsl[0][8] << endr
	  << jac_kpsl[1][0] << jac_kpsl[1][1] << jac_kpsl[1][2] << jac_kpsl[1][3] << jac_kpsl[1][4] << jac_kpsl[1][5] << jac_kpsl[1][6] << jac_kpsl[1][7] << jac_kpsl[1][8] << endr
	  << jac_kpsl[2][0] << jac_kpsl[2][1] << jac_kpsl[2][2] << jac_kpsl[2][3] << jac_kpsl[2][4] << jac_kpsl[2][5] << jac_kpsl[2][6] << jac_kpsl[2][7] << jac_kpsl[2][8] << endr
	  << jac_kpsl[3][0] << jac_kpsl[3][1] << jac_kpsl[3][2] << jac_kpsl[3][3] << jac_kpsl[3][4] << jac_kpsl[3][5] << jac_kpsl[3][6] << jac_kpsl[3][7] << jac_kpsl[3][8] << endr
	  << jac_kpsl[4][0] << jac_kpsl[4][1] << jac_kpsl[4][2] << jac_kpsl[4][3] << jac_kpsl[4][4] << jac_kpsl[4][5] << jac_kpsl[4][6] << jac_kpsl[4][7] << jac_kpsl[4][8] << endr
	  << jac_kpsl[5][0] << jac_kpsl[5][1] << jac_kpsl[5][2] << jac_kpsl[5][3] << jac_kpsl[5][4] << jac_kpsl[5][5] << jac_kpsl[5][6] << jac_kpsl[5][7] << jac_kpsl[5][8] << endr
	  << jac_kpsl[6][0] << jac_kpsl[6][1] << jac_kpsl[6][2] << jac_kpsl[6][3] << jac_kpsl[6][4] << jac_kpsl[6][5] << jac_kpsl[6][6] << jac_kpsl[6][7] << jac_kpsl[6][8] << endr
	  << jac_kpsl[7][0] << jac_kpsl[7][1] << jac_kpsl[7][2] << jac_kpsl[7][3] << jac_kpsl[7][4] << jac_kpsl[7][5] << jac_kpsl[7][6] << jac_kpsl[7][7] << jac_kpsl[7][8] << endr
	  << jac_kpsl[8][0] << jac_kpsl[8][1] << jac_kpsl[8][2] << jac_kpsl[8][3] << jac_kpsl[8][4] << jac_kpsl[8][5] << jac_kpsl[8][6] << jac_kpsl[8][7] << jac_kpsl[8][8] << endr;

    return Jg_ex;
}

mat pass_Jv(double base_ang[3], double k[3], double p[3], double s[3])
{
    mat Jv_ex;
    double jac_v[3][9];
    jac_v[0][0] = (-(-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + s[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + s[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + s[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] + s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0])/k[0] + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0])/k[0] + s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])/k[0] - (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])/pow(k[0],2) - (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/pow(k[0],2) - (-cos(k[0]*s[0]) + 1)*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])/pow(k[0],2);
    jac_v[0][1] = ((-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])*cos(p[0]) - sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(k[1]*s[1]) + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])*cos(p[0]) - sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(k[1]*s[1]) + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])*cos(p[0]) - sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(k[1]*s[1])/k[1] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[0]*s[0]) + 1)*cos(p[0])/k[0] - (-cos(k[0]*s[0]) + 1)*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])/k[0];
    jac_v[0][2] = (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]) + (-(-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + k[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + k[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + k[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1];
    jac_v[0][3] = (-cos(k[2]*s[2]) + 1)*(-s[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - s[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(p[2])/k[2] + (s[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + s[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + s[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1])/k[1] + s[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1])/k[1] + s[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1])/k[1] - ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/pow(k[1],2) - (-cos(k[1]*s[1]) + 1)*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/pow(k[1],2) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/pow(k[1],2);
    jac_v[0][4] = (-((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(k[1]*s[1])*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1])*cos(k[1]*s[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] - (-cos(k[1]*s[1]) + 1)*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])/k[1];
    jac_v[0][5] =  ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + (-cos(k[2]*s[2]) + 1)*(-k[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - k[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(p[2])/k[2] + (k[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + k[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2];
    jac_v[0][6] = s[2]*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2])/k[2] + s[2]*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(k[2]*s[2])/k[2] + s[2]*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])/k[2] - (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/pow(k[2],2) - (-cos(k[2]*s[2]) + 1)*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*cos(p[2])/pow(k[2],2) - (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/pow(k[2],2);
    jac_v[0][7] = (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(p[2])/k[2];
    jac_v[0][8] = (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]);
    jac_v[1][0] = ((-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]) + (s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + s[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]) - (s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + s[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + s[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] + s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0])/k[0] + s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0])/k[0] + s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0])/k[0] - (sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/pow(k[0],2) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])/pow(k[0],2) - (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])/pow(k[0],2);
    jac_v[1][1] = ((-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])*cos(p[0]) - sin(base_ang[0])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])*cos(p[0]) - sin(base_ang[0])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])*cos(p[0]) - sin(base_ang[0])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]))*sin(k[1]*s[1])/k[1] + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[0]*s[0]) + 1)*cos(p[0])/k[0] - (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*sin(p[0])*cos(base_ang[1])/k[0];
    jac_v[1][2] = (sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]) + ((-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]) + (k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + k[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]) - (k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + k[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + k[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1];
    jac_v[1][3] = (-cos(k[2]*s[2]) + 1)*(-s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1])/k[1] + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1])/k[1] + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1])/k[1] - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/pow(k[1],2) - (-cos(k[1]*s[1]) + 1)*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/pow(k[1],2) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/pow(k[1],2);
    jac_v[1][4] = (-((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(k[1]*s[1])*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] - (-cos(k[1]*s[1]) + 1)*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])/k[1];
    jac_v[1][5] = ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-cos(k[2]*s[2]) + 1)*(-k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2];
    jac_v[1][6] = s[2]*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2])/k[2] + s[2]*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2])/k[2] + s[2]*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])/k[2] - (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/pow(k[2],2) - (-cos(k[2]*s[2]) + 1)*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/pow(k[2],2) - (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/pow(k[2],2);
    jac_v[1][7] = (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(p[2])/k[2];
    jac_v[1][8] = (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2]) + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]);
    jac_v[2][0] = ((s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-s[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + s[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - (-s[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + s[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])/k[1] + (-s[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + s[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])/k[1] - s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0])/k[0] + s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1])/k[0] + s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0])/k[0] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[1])*cos(p[0])/pow(k[0],2) - (-cos(k[0]*s[0]) + 1)*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])/pow(k[0],2) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2])/pow(k[0],2);
    jac_v[2][1] = ((sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - (sin(base_ang[1])*sin(p[0])*sin(k[0]*s[0]) + sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (sin(base_ang[1])*sin(p[0])*sin(k[0]*s[0]) + sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (sin(base_ang[1])*sin(p[0])*sin(k[0]*s[0]) + sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/k[1] + (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[1])*sin(p[0])/k[0] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[2])*cos(base_ang[1])*cos(p[0])/k[0];
    jac_v[2][2] = -sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]) + ((k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-k[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + k[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - (-k[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + k[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])/k[1] + (-k[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + k[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])/k[1];
    jac_v[2][3] = (-cos(k[2]*s[2]) + 1)*(-s[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) - s[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) - s[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (s[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - s[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + s[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + s[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1])/k[1] + s[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1])/k[1] + s[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1])/k[1] - (sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/pow(k[1],2) - (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])/pow(k[1],2) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])/pow(k[1],2);
    jac_v[2][4] = (-(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1])*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + (sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] - (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1])/k[1];
    jac_v[2][5] = (sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]) + (-cos(k[2]*s[2]) + 1)*(-k[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) - k[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) - k[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (k[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - k[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + k[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2];
    jac_v[2][6] = s[2]*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2])/k[2] + s[2]*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2])/k[2] + s[2]*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])/k[2] - ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/pow(k[2],2) - (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/pow(k[2],2) - ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/pow(k[2],2);
    jac_v[2][7] = ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(p[2])/k[2];
    jac_v[2][8] = ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2]) + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2]) + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]);


    Jv_ex << jac_v[0][0] << jac_v[0][1] << jac_v[0][2] << jac_v[0][3] << jac_v[0][4] << jac_v[0][5] << jac_v[0][6] << jac_v[0][7] << jac_v[0][8] << endr
	        << jac_v[1][0] << jac_v[1][1] << jac_v[1][2] << jac_v[1][3] << jac_v[1][4] << jac_v[1][5] << jac_v[1][6] << jac_v[1][7] << jac_v[1][8] << endr
	        << jac_v[2][0] << jac_v[2][1] << jac_v[2][2] << jac_v[2][3] << jac_v[2][4] << jac_v[2][5] << jac_v[2][6] << jac_v[2][7] << jac_v[2][8] << endr;
    return Jv_ex;
}

mat pass_Jw(double k[3], double p[3], double s[3])
{
    mat Jw_ex;
    double JacOmega[3][9];
    JacOmega[0][0] = ((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*cos(p[2]) - (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(p[2]))*(s[0]*sin(p[1])*cos(p[2])*cos(k[0]*s[0]) - (s[0]*sin(k[0]*s[0])*sin(k[1]*s[1]) - s[0]*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2])) + ((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) + (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*cos(k[2]*s[2]))*(s[0]*sin(p[1])*sin(p[2])*sin(k[2]*s[2])*cos(k[0]*s[0]) + (s[0]*sin(k[0]*s[0])*sin(k[1]*s[1]) - s[0]*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) + (-s[0]*sin(k[0]*s[0])*cos(k[1]*s[1]) - s[0]*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]))*cos(k[2]*s[2])) + ((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*cos(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2]))*(s[0]*sin(p[1])*sin(p[2])*cos(k[0]*s[0])*cos(k[2]*s[2]) + (s[0]*sin(k[0]*s[0])*sin(k[1]*s[1]) - s[0]*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - (-s[0]*sin(k[0]*s[0])*cos(k[1]*s[1]) - s[0]*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]));
    JacOmega[0][1] = 0.0;
    JacOmega[0][2] = ((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*cos(p[2]) - (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(p[2]))*(k[0]*sin(p[1])*cos(p[2])*cos(k[0]*s[0]) - (k[0]*sin(k[0]*s[0])*sin(k[1]*s[1]) - k[0]*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2])) + ((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) + (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*cos(k[2]*s[2]))*(k[0]*sin(p[1])*sin(p[2])*sin(k[2]*s[2])*cos(k[0]*s[0]) + (k[0]*sin(k[0]*s[0])*sin(k[1]*s[1]) - k[0]*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) + (-k[0]*sin(k[0]*s[0])*cos(k[1]*s[1]) - k[0]*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]))*cos(k[2]*s[2])) + ((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*cos(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2]))*(k[0]*sin(p[1])*sin(p[2])*cos(k[0]*s[0])*cos(k[2]*s[2]) + (k[0]*sin(k[0]*s[0])*sin(k[1]*s[1]) - k[0]*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - (-k[0]*sin(k[0]*s[0])*cos(k[1]*s[1]) - k[0]*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]));
    JacOmega[0][3] = -((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*cos(p[2]) - (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(p[2]))*(s[1]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) - s[1]*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2]) + ((s[1]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) - s[1]*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) + (-s[1]*sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - s[1]*sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(k[2]*s[2]))*((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) + (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*cos(k[2]*s[2])) + ((s[1]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) - s[1]*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - (-s[1]*sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - s[1]*sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]))*((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*cos(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2]));
    JacOmega[0][4] = ((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*cos(p[2]) - (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(p[2]))*(-sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[1])*cos(p[2])) + ((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) + (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*cos(k[2]*s[2]))*(sin(p[1])*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(k[2]*s[2]) + sin(p[1])*sin(k[0]*s[0])*sin(k[2]*s[2])*cos(p[2])*cos(k[1]*s[1]) + sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2])*cos(p[1])) + ((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*cos(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2]))*(-sin(p[1])*sin(k[0]*s[0])*sin(k[1]*s[1])*sin(k[2]*s[2]) + sin(p[1])*sin(k[0]*s[0])*cos(p[2])*cos(k[1]*s[1])*cos(k[2]*s[2]) + sin(p[2])*sin(k[0]*s[0])*cos(p[1])*cos(k[2]*s[2]));
    JacOmega[0][5] = -((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*cos(p[2]) - (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(p[2]))*(k[1]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) - k[1]*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2]) + ((k[1]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) - k[1]*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) + (-k[1]*sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - k[1]*sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(k[2]*s[2]))*((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) + (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*cos(k[2]*s[2])) + ((k[1]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) - k[1]*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - (-k[1]*sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - k[1]*sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]))*((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*cos(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2]));
    JacOmega[0][6] = (-s[2]*(-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + s[2]*(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) + s[2]*sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[2]*s[2]))*((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) + (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*cos(k[2]*s[2])) + (-s[2]*(-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) - s[2]*(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) - s[2]*sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2]))*((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*cos(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2]));
    JacOmega[0][7] = ((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*cos(p[2]) - (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(p[2]))*(-(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2]) - sin(p[1])*sin(p[2])*sin(k[0]*s[0])) + (-(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(p[2])*sin(k[2]*s[2]) + sin(p[1])*sin(k[0]*s[0])*sin(k[2]*s[2])*cos(p[2]))*((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) + (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*cos(k[2]*s[2])) + (-(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(p[2])*cos(k[2]*s[2]) + sin(p[1])*sin(k[0]*s[0])*cos(p[2])*cos(k[2]*s[2]))*((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*cos(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2]));
    JacOmega[0][8] = (-k[2]*(-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + k[2]*(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) + k[2]*sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[2]*s[2]))*((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) + (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*cos(k[2]*s[2])) + (-k[2]*(-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) - k[2]*(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) - k[2]*sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2]))*((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*cos(k[2]*s[2]) + (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - (sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2]));
    JacOmega[1][0] = (-(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(p[2]) + sin(p[1])*sin(k[0]*s[0])*cos(p[2]))*(s[0]*sin(p[1])*sin(k[0]*s[0])*cos(p[0])*cos(p[2]) - (-s[0]*sin(k[0]*s[0])*cos(p[0])*cos(p[1])*cos(k[1]*s[1]) - s[0]*sin(k[1]*s[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])) + (-(-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[2]*s[2]))*(s[0]*sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(p[0])*cos(k[2]*s[2]) - (-s[0]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0])*cos(p[1]) + s[0]*cos(p[0])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-s[0]*sin(k[0]*s[0])*cos(p[0])*cos(p[1])*cos(k[1]*s[1]) - s[0]*sin(k[1]*s[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2])) + ((-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2]))*(s[0]*sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2])*cos(p[0]) + (-s[0]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0])*cos(p[1]) + s[0]*cos(p[0])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-s[0]*sin(k[0]*s[0])*cos(p[0])*cos(p[1])*cos(k[1]*s[1]) - s[0]*sin(k[1]*s[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]));
    JacOmega[1][1] = ((sin(p[0])*sin(p[1])*cos(k[0]*s[0]) - cos(p[0])*cos(p[1]))*cos(p[2]) - (sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) - sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) - sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(p[2]))*(-(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(p[2]) + sin(p[1])*sin(k[0]*s[0])*cos(p[2])) + (-(-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[2]*s[2]))*((sin(p[0])*sin(p[1])*cos(k[0]*s[0]) - cos(p[0])*cos(p[1]))*sin(p[2])*cos(k[2]*s[2]) + (sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) - sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) - sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) - sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) - sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2])) + ((-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2]))*((sin(p[0])*sin(p[1])*cos(k[0]*s[0]) - cos(p[0])*cos(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) - sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) - sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) + (-sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) - sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) - sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*cos(k[2]*s[2]));
    JacOmega[1][2] = (-(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(p[2]) + sin(p[1])*sin(k[0]*s[0])*cos(p[2]))*(k[0]*sin(p[1])*sin(k[0]*s[0])*cos(p[0])*cos(p[2]) - (-k[0]*sin(k[0]*s[0])*cos(p[0])*cos(p[1])*cos(k[1]*s[1]) - k[0]*sin(k[1]*s[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])) + (-(-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[2]*s[2]))*(k[0]*sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(p[0])*cos(k[2]*s[2]) - (-k[0]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0])*cos(p[1]) + k[0]*cos(p[0])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-k[0]*sin(k[0]*s[0])*cos(p[0])*cos(p[1])*cos(k[1]*s[1]) - k[0]*sin(k[1]*s[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2])) + ((-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2]))*(k[0]*sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2])*cos(p[0]) + (-k[0]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0])*cos(p[1]) + k[0]*cos(p[0])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-k[0]*sin(k[0]*s[0])*cos(p[0])*cos(p[1])*cos(k[1]*s[1]) - k[0]*sin(k[1]*s[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]));
    JacOmega[1][3] = -(-(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(p[2]) + sin(p[1])*sin(k[0]*s[0])*cos(p[2]))*(s[1]*sin(p[0])*sin(p[1])*sin(k[1]*s[1]) - s[1]*sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) - s[1]*sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(p[2]) + ((s[1]*sin(p[0])*sin(p[1])*sin(k[1]*s[1]) - s[1]*sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) - s[1]*sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) + (-s[1]*sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - s[1]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + s[1]*cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]))*((-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2])) + ((s[1]*sin(p[0])*sin(p[1])*sin(k[1]*s[1]) - s[1]*sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) - s[1]*sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) - (-s[1]*sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - s[1]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + s[1]*cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]))*(-(-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[2]*s[2]));
    JacOmega[1][4] = ((sin(p[0])*sin(p[1]) - cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(p[2]) - (-sin(p[0])*cos(p[1])*cos(k[1]*s[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2]))*(-(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(p[2]) + sin(p[1])*sin(k[0]*s[0])*cos(p[2])) + (-(-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[2]*s[2]))*((sin(p[0])*sin(p[1]) - cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(k[1]*s[1])*cos(p[1]) - sin(p[1])*sin(k[1]*s[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[2]*s[2]) + (-sin(p[0])*cos(p[1])*cos(k[1]*s[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2])) + ((-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2]))*((sin(p[0])*sin(p[1]) - cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(k[1]*s[1])*cos(p[1]) - sin(p[1])*sin(k[1]*s[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[2]*s[2]) + (-sin(p[0])*cos(p[1])*cos(k[1]*s[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]));
    JacOmega[1][5] = -(-(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(p[2]) + sin(p[1])*sin(k[0]*s[0])*cos(p[2]))*(k[1]*sin(p[0])*sin(p[1])*sin(k[1]*s[1]) - k[1]*sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) - k[1]*sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(p[2]) + ((k[1]*sin(p[0])*sin(p[1])*sin(k[1]*s[1]) - k[1]*sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) - k[1]*sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) + (-k[1]*sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - k[1]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + k[1]*cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]))*((-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2])) + ((k[1]*sin(p[0])*sin(p[1])*sin(k[1]*s[1]) - k[1]*sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) - k[1]*sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) - (-k[1]*sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - k[1]*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + k[1]*cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]))*(-(-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[2]*s[2]));
    JacOmega[1][6] = (-(-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[2]*s[2]))*(-s[2]*(-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*sin(k[2]*s[2]) - s[2]*(-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(k[2]*s[2]) - s[2]*(-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])) + ((-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2]))*(s[2]*(-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*cos(k[2]*s[2]) - s[2]*(-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]) + s[2]*(-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]));
    JacOmega[1][7] = (-(-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2]) - (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2]))*(-(-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(p[2]) + sin(p[1])*sin(k[0]*s[0])*cos(p[2])) + ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) - (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2])*sin(k[2]*s[2]))*((-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2])) + ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2])*cos(k[2]*s[2]))*(-(-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[2]*s[2]));
    JacOmega[1][8] = (-(-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[2]*s[2]))*(-k[2]*(-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*sin(k[2]*s[2]) - k[2]*(-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(k[2]*s[2]) - k[2]*(-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])) + ((-sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2]) + sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2]))*(k[2]*(-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*cos(k[2]*s[2]) - k[2]*(-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]) + k[2]*(-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]));
    JacOmega[2][0] = ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[2]) - (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2]))*(s[0]*sin(p[0])*sin(p[1])*sin(k[0]*s[0])*cos(p[2]) - (-s[0]*sin(p[0])*sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - s[0]*sin(p[0])*sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(p[2])) + ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]))*(s[0]*sin(p[0])*sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2]) + (-s[0]*sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + s[0]*sin(p[0])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-s[0]*sin(p[0])*sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - s[0]*sin(p[0])*sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2])) + ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]))*(s[0]*sin(p[0])*sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[2]*s[2]) - (-s[0]*sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + s[0]*sin(p[0])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-s[0]*sin(p[0])*sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - s[0]*sin(p[0])*sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]));
    JacOmega[2][1] = pow(((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[2]) - (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2])),2) + pow(((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])),2) + pow(((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2])),2);
    JacOmega[2][2] = ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[2]) - (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2]))*(k[0]*sin(p[0])*sin(p[1])*sin(k[0]*s[0])*cos(p[2]) - (-k[0]*sin(p[0])*sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - k[0]*sin(p[0])*sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(p[2])) + ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]))*(k[0]*sin(p[0])*sin(p[1])*sin(p[2])*sin(k[0]*s[0])*sin(k[2]*s[2]) + (-k[0]*sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + k[0]*sin(p[0])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-k[0]*sin(p[0])*sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - k[0]*sin(p[0])*sin(k[1]*s[1])*cos(k[0]*s[0]))*sin(k[2]*s[2])*cos(p[2])) + ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]))*(k[0]*sin(p[0])*sin(p[1])*sin(p[2])*sin(k[0]*s[0])*cos(k[2]*s[2]) - (-k[0]*sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[1]) + k[0]*sin(p[0])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-k[0]*sin(p[0])*sin(k[0]*s[0])*cos(p[1])*cos(k[1]*s[1]) - k[0]*sin(p[0])*sin(k[1]*s[1])*cos(k[0]*s[0]))*cos(p[2])*cos(k[2]*s[2]));
    JacOmega[2][3] = -((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[2]) - (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2]))*(-s[1]*sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) - s[1]*sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) - s[1]*sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(p[2]) + (-(-s[1]*sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + s[1]*sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + s[1]*sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-s[1]*sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) - s[1]*sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) - s[1]*sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*cos(p[2])*cos(k[2]*s[2]))*((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2])) + ((-s[1]*sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + s[1]*sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + s[1]*sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-s[1]*sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) - s[1]*sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) - s[1]*sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2])*cos(p[2]))*((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]));
    JacOmega[2][4] = ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[2]) - (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2]))*((-sin(p[0])*cos(p[1])*cos(k[0]*s[0]) - sin(p[1])*cos(p[0]))*cos(p[2]) - (-sin(p[0])*sin(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + cos(p[0])*cos(p[1])*cos(k[1]*s[1]))*sin(p[2])) + ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]))*((-sin(p[0])*cos(p[1])*cos(k[0]*s[0]) - sin(p[1])*cos(p[0]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*sin(k[1]*s[1])*cos(k[0]*s[0]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1]))*cos(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + cos(p[0])*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])) + ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]))*((-sin(p[0])*cos(p[1])*cos(k[0]*s[0]) - sin(p[1])*cos(p[0]))*sin(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(p[1])*sin(k[1]*s[1])*cos(k[0]*s[0]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1]))*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + cos(p[0])*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]));
    JacOmega[2][5] = -((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[2]) - (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2]))*(-k[1]*sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) - k[1]*sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) - k[1]*sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(p[2]) + (-(-k[1]*sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + k[1]*sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + k[1]*sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(k[2]*s[2]) + (-k[1]*sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) - k[1]*sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) - k[1]*sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*cos(p[2])*cos(k[2]*s[2]))*((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2])) + ((-k[1]*sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + k[1]*sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + k[1]*sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (-k[1]*sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) - k[1]*sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) - k[1]*sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2])*cos(p[2]))*((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]));
    JacOmega[2][6] = ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]))*(s[2]*(-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*cos(k[2]*s[2]) + s[2]*(-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - s[2]*(sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2])) + ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]))*(-s[2]*(-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*sin(k[2]*s[2]) - s[2]*(-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) - s[2]*(sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*cos(k[2]*s[2]));
    JacOmega[2][7] = ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[2]) - (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(p[2]))*(-(-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2]) - (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])) + ((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(k[2]*s[2])*cos(p[2]) - (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(p[2])*sin(k[2]*s[2]))*((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])) + ((-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*cos(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(p[2])*cos(k[2]*s[2]))*((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]));
    JacOmega[2][8] = ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*cos(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]))*(k[2]*(-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*cos(k[2]*s[2]) + k[2]*(-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]) - k[2]*(sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2])) + ((-sin(p[0])*cos(p[1]) - sin(p[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[2])*cos(k[2]*s[2]) - (-sin(p[0])*sin(p[1])*sin(k[1]*s[1]) + sin(k[0]*s[0])*cos(p[0])*cos(k[1]*s[1]) + sin(k[1]*s[1])*cos(p[0])*cos(p[1])*cos(k[0]*s[0]))*sin(k[2]*s[2]) + (-sin(p[0])*sin(p[1])*cos(k[1]*s[1]) - sin(k[0]*s[0])*sin(k[1]*s[1])*cos(p[0]) + cos(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]))*cos(p[2])*cos(k[2]*s[2]))*(-k[2]*(-sin(p[0])*sin(p[1])*cos(k[0]*s[0]) + cos(p[0])*cos(p[1]))*sin(p[2])*sin(k[2]*s[2]) - k[2]*(-sin(p[0])*sin(k[0]*s[0])*sin(k[1]*s[1]) + sin(p[0])*cos(p[1])*cos(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]) - k[2]*(sin(p[0])*sin(k[0]*s[0])*cos(k[1]*s[1]) + sin(p[0])*sin(k[1]*s[1])*cos(p[1])*cos(k[0]*s[0]) + sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*cos(k[2]*s[2]));

    Jw_ex << JacOmega[0][0] << JacOmega[0][1] << JacOmega[0][2] << JacOmega[0][3] << JacOmega[0][4] << JacOmega[0][5] << JacOmega[0][6] << JacOmega[0][7] << JacOmega[0][8] << endr
	        << JacOmega[1][0] << JacOmega[1][1] << JacOmega[1][2] << JacOmega[1][3] << JacOmega[1][4] << JacOmega[1][5] << JacOmega[1][6] << JacOmega[1][7] << JacOmega[1][8] << endr
	        << JacOmega[2][0] << JacOmega[2][1] << JacOmega[2][2] << JacOmega[2][3] << JacOmega[2][4] << JacOmega[2][5] << JacOmega[2][6] << JacOmega[2][7] << JacOmega[2][8] << endr;

    return Jw_ex;
}

void c_to_q()
{
  for(unsigned char i=0; i<3; i++)
  {
    k_val[i] = l_to_k(len[i][0],len[i][1],len[i][2]);
    p_val[i] = l_to_phi(len[i][0],len[i][1],len[i][2]);
    s_val[i] = l_to_s(len[i][0],len[i][1],len[i][2]);
  }

//   k_noi = l_to_k(l_noise[0],l_noise[1],l_noise[2]);
//   p_noi = l_to_phi(l_noise[0],l_noise[1],l_noise[2]);
//   s_noi = l_to_s(l_noise[0],l_noise[1],l_noise[2]);
}

mat constraint_potential(double l[3][3])
{
    double vel[9];
    config_field.data.clear();
    double a_i = 0.5*(Lo + s_min + Lo + s_max);
    unsigned char ind = 0;
    for(unsigned char i=0; i<3; i++)
    {
      for(unsigned char j=0; j<3; j++)
      {
      double weight = 5.0*(1.0-exp(-0.5*norm(fields)));

      vel[ind] = -0.05*weight*1.0/3.0*(Lo+l[i][j]-a_i)/pow((a_i-s_max),2);

      config_field.data.push_back(vel[ind]);
      ind++;
      }
    }
    mat cf_mat;
    cf_mat << vel[0] << endr
	   << vel[1] << endr
	   << vel[2] << endr
	   << vel[3] << endr
	   << vel[4] << endr
	   << vel[5] << endr
	   << vel[6] << endr
	   << vel[7] << endr
	   << vel[8] << endr;
    return cf_mat;
}

// void redundancy_resolution()
// {
//   mat red_term, grad_s, grad_final, s_mat, smid_mat, length_field;
//   int mat_size;
//   double s_mid = 0.5*(s_min+s_max);
//   double s_half = (s_mid-s_min);
//   double length_weight, sigma;
// //   double gain = 0.3; //Gain for combined obstacle + constraint avoidance
//   double gain = 0.0003; // Gain for constraint avoidance only
//   mat_size = segment_num*2+base_ind;
//   s_mat << s_val[0] << endr
// 	<< s_val[1] << endr
// 	<< s_val[2] << endr;
//   smid_mat << s_mid << endr
// 	   << s_mid << endr
// 	   << s_mid << endr;
//
//   mat weight_length;
//   int cond;
//   weight_length.set_size(3,1);
//   weight_length.zeros();
//   double boundary;
//
//   for(unsigned char i=0; i<3; i++)
//   {
//     if(abs(s_mat(i,0)-smid_mat(i,0))<s_half/3)
//     {
//       weight_length(i,0) = 0;
//       cond = 0;
//     }
//     else if(abs(s_mat(i,0)-smid_mat(i,0))>=s_half/3 && abs(s_mat(i,0)-smid_mat(i,0))<2*s_half/3)
//     {
//       weight_length(i,0) = pow(cos((M_PI/2)*((abs(s_mat(i,0)-smid_mat(i,0))-(s_half/3))/(2*s_half/3-s_half/3))),2);
//       cond = 1;
//     }
//     else
//     {
//       weight_length(i,0) = 1;
//       cond = 2;
//     }
//   }
// //   cout << cond << endl;
//   double cons = 1;
//   grad_s = -cons*(s_mat - smid_mat)/pow((s_mid-s_min),2);
//   grad_final << 0.0 << endr
// 	     << grad_s(0,0) << endr
// 	     << 0.0 << endr
// 	     << grad_s(1,0) << endr
// 	     << 0.0 << endr
// 	     << grad_s(2,0) << endr;
//
// // length and kappa redundancy
// //   grad_final.print("grad:");
// //   red_term = (eye<mat>(mat_size,mat_size)-pinv(Jv)*Jv)*grad_final;
//   if(base==0)
//   {
//     length_jacobi << 0 << 1 << 0 << 0 << 0 << 0 << endr
// 		  << 0 << 0 << 0 << 1 << 0 << 0 << endr
// 		  << 0 << 0 << 0 << 0 << 0 << 1 << endr;
//   }
//   else
//   {
//     length_jacobi << 0 << 0 << 1 << 0 << 0 << 0 << 0 << endr
// 		  << 0 << 0 << 0 << 0 << 1 << 0 << 0 << endr
// 		  << 0 << 0 << 0 << 0 << 0 << 0 << 1 << endr;
//   }
//   length_field << grad_s(0,0) << endr
// 	       << grad_s(1,0) << endr
// 	       << grad_s(2,0) << endr;
// //   length_field << weight_length(0,0)*grad_s(0,0) << endr
// // 	       << weight_length(1,0)*grad_s(1,0) << endr
// // 	       << weight_length(2,0)*grad_s(2,0) << endr;
//
// //   cout << weight_length(2,0) << endl;
// // Length redundancy 3rd priority combined with obstacle avoidance
// //   mat J_tilda;
// //   J_tilda = sensor_jacobi*(eye<mat>(mat_size,mat_size)-pinv(Jv)*Jv);
// //   red_term = (eye<mat>(mat_size,mat_size)-pinv(Jv)*Jv)*(eye<mat>(mat_size,mat_size)-pinv(J_tilda)*J_tilda)*grad_final;
// //   final_term = gain*red_term;
//
// // Length redundancy only
// //   sigma = norm(s_mat-smid_mat);
//
//   double max = 0.0;
//   for(unsigned char i=0; i<3; i++)
//   {
//     if(weight_length(i,0)>max) max = weight_length(i,0);
//   }
//   length_weight = 0.5*max;
// //   cout << length_weight << endl;
//
// //   if(pow(sigma,2)<3*pow(s_half/3,2)) length_weight = 0;
// //   else if(pow(sigma,2)>=3*pow(s_half/3,2) && pow(sigma,2)<3*pow(2*s_half/3,2)) length_weight = pow(cos((M_PI/2)*((pow(sigma,2)-3*pow(s_half/3,2))/(3*pow(2*s_half/3,2)-3*pow(s_half/3,2)))),2);
// //   else length_weight = 1;
//
//   red_term = pinv(length_jacobi*(eye<mat>(mat_size,mat_size)-pinv(Jv)*Jv))*(gain*length_field - length_jacobi*pinv(Jv)*fields);
//   final_term = length_weight*red_term;
// //   cout << length_weight << endl;
// //   final_term = red_term;
// }

// void redundancy_constraint()
// {
//   mat red_term, grad_s, grad_final, s_mat, smid_mat, length_field;
//   int mat_size;
//   double s_mid = 0.5*(s_min+s_max);
//   double s_half = (s_mid-s_min);
//   double gain = 0.0003; // Gain for constraint avoidance only
//   mat_size = segment_num*2+base_ind;
//   s_mat << s_val[0] << endr
// 	<< s_val[1] << endr
// 	<< s_val[2] << endr;
//   smid_mat << s_mid << endr
// 	   << s_mid << endr
// 	   << s_mid << endr;
//
//   mat rho_min, rho_max, rho_0;
//   rho_min = s_mat - s_min*ones(3,1);
//   rho_max = -s_mat + s_max*ones(3,1);
//   rho_0 = s_mid/2;
//   for(unsigned char=0; i<3; i++)
//   {
//     if(rho_min(i,0)<rho_0) grad_s(i,0) = gain*(1/rho_min(i,0) - 1/rho_0)/pow(rho_min(i,0),2);
//     else if(rho_max(i,0)<rho_0) grad_s(i,0) = gain*(-1)*(1/rho_max(i,0) - 1/rho_0)/pow(rho_max(i,0),2);
//     else grad_s(i,0) = 0;
//   }
//
//   if(rho_modify>=2*rho_0) redundancy_weight = 0;
//   else if(rho_modify>rho_0 && rho_modify<2*rho) redundancy_weight = pow(cos((M_PI/2)*((rho_modify-rho_0)/(rho_0))),2);
//   else redundancy_weight = 1;
//
//   red_term = pinv(length_jacobi*(eye<mat>(mat_size,mat_size)-pinv(Jv)*Jv))*(gain*length_field - length_jacobi*pinv(Jv)*fields);
//   final_term = length_weight*red_term;
// }
void redundancy_obstacle()
{
  int mat_size;
  mat_size = 9;
//   Obstacle 2nd priority
  obstacle_term = red_weight*pinv(sensor_jacobi*(eye<mat>(mat_size,mat_size)-pinv(Ju)*Ju))*(obstacle_field - sensor_jacobi*pinv(Ju)*fields);

  // Obstacle 3rd priority
//   mat J_tilda, J_aug;
//   J_aug = join_cols(Jv, length_jacobi);
//   J_tilda = sensor_jacobi*(eye<mat>(mat_size,mat_size)-pinv(J_aug)*J_aug);
//   obstacle_term = red_weight*pinv(J_tilda)*(obstacle_field - sensor_jacobi*(goal_term+final_term));

}

void integral_length()
{

  // Inverse Jacobian
//   double diameter = 0.015;
//   double scale = 128.0*9.54929659643/(diameter/2.0);
//   double rpm_max = 9090.0;
//   double tendon_max = rpm_max/scale;
  mat rate_mat, stiff_mat;
  mat jumper;
  if(length_active == 1)
  {
    if(orient_active == 1)
    {
      if(euler_flag == 1)
      {
        // cout << "check" << endl;
        mat omegatoeuler;
        omegatoeuler << -sin(euler[1,0]) << 0 << 1 << endr
                     << cos(euler[1,0])*sin(euler[2,0]) << cos(euler[2,0]) << 0 << endr
                     << cos(euler[1,0])*cos(euler[2,0]) << -sin(euler[1,0]) << 0 << endr;
        Ju = join_cols(Jv, omegatoeuler*Jw)*Jkpsl;

      }

    }
    else
    {
      Ju = Jv*Jkpsl;
    }
    if(flag_jacobi==1) sensor_jacobi = sensor_jacobi_raw*Jkpsl;
  }
  else
  {
    Ju = Jv;
    if(flag_jacobi==1) sensor_jacobi = sensor_jacobi_raw;
  }

  mat rate_dummy;
  //   redundancy_resolution();

  //   rate_dummy = pinv(Jv)*(fields) + v_obs + final_term;
  //   if(flag_jacobi == 1) rate_dummy = pinv(Jv)*(fields) + pinv(sensor_jacobi)*(obstacle_field) + final_term;
  //   cout << "tes" << endl;
  if(flag_jacobi == 1)
  {
    if(orient_active==1)
    {
      if(euler_flag == 1)
      {
        // cout << "tes" << endl;
        mat fields_tot;
        fields_tot = join_cols(fields, fields_euler);
        // fields.print("fields:");
        // fields_euler.print("fields_euler:");
        // Ju.print("Ju:");
        goal_term = pinv(Ju)*fields_tot;

      }
      else
      {
        goal_term = zeros(9,1);
      }

    }
    else
    {
        goal_term = pinv(Ju)*(fields);
    }

      // redundancy_resolution();

    // Obstacle Redundancy, use length and kappa redundancy as well
    // sensor_jacobi.print("sens_jac");
      obstacle_term = pinv(sensor_jacobi)*obstacle_field;
    // redundancy_obstacle();


    rate_dummy = goal_term + obstacle_term;// + final_term;

      // rate_dummy = goal_term + final_term;
    // rate_dummy = goal_term;
    // rate_dummy = obstacle_term;
    field_out = sensor_jacobi*rate_dummy;
    // field_out = field_out - obstacle_field;
    // field_out.print("field_out:");
  }
  else
  {
    rate_dummy.set_size(9,1);
    rate_dummy.zeros();
  }
  //   final_term.print("red_term: ");
  if(guide == 1)
  {
    rate_mat = rate_dummy;
  }
  else
  {
    if(flag_move == 1)
    {
      rate_mat = rate_dummy;
  // //       rate_mat.print("rate_move:");
    }
    else
    {
  //       cout << "segment: " << segment_num << endl;
      rate_mat.set_size(9, 1);
      rate_mat.zeros();
    }
  }

  // rate_mat = pinv(Ju)*(fields) + v_obs;// + v_config;

  for(unsigned char i=0; i<9; i++)
  {
    rate[i] = rate_mat(i,0);
//     if(rate[i] > tendon_max)
//     {
//       rate[i] = tendon_max;
//       cout << rate[i] << endl;
//     }
//     else if(rate[i] < -tendon_max)
//     {
//       rate[i] = -tendon_max;
//       cout << rate[i] << endl;
//     }

  }

  //Integral Length
  if(length_active==1)
  {
    unsigned char ind = 0;
    for(unsigned char i=0; i<3; i++)
    {
      for(unsigned char j=0; j<3; j++)
      {
        len[i][j] = len[i][j] + rate[ind]*dt;
        ind++;

        // constraint
        // if(len[i][j]<s_min) len[i][j] = s_min;
        // if(len[i][j]>s_max) len[i][j] = s_max;
      }
  //     l_noise[i] = l_noise[i] + rate[i]*dt;// + noise_mat(i,0);
    }


  //   if((l[0]<0.0) && (l[1]<0.0) && (l[2]<0.0)) flag_error = 1;
    c_to_q();
  }
  else
  {
    for(unsigned char i=0; i<3; i++)
    {
      k_val[i] = k_val[i] + rate[3*i]*dt;
      p_val[i] = p_val[i] + rate[3*i+1]*dt;
      s_val[i] = s_val[i] + rate[3*i+2]*dt;
    }
  }

}

// void get_error(const geometry_msgs::Pose::ConstPtr& msg)
// {
//   geometry_msgs::Pose er;
//   er = *msg;
//   e_vector << er.position.x << endr
// 	   << er.position.y << endr
// 	   << er.position.z << endr;
// }
//
// void get_ref(const std_msgs::Float64MultiArray::ConstPtr& msg)
// {
//   std_msgs::Float64MultiArray pathv;
//   pathv = *msg;
//   path_vel << pathv.data[0] << endr
// 	   << pathv.data[1] << endr
// 	   << pathv.data[2] << endr;
// }

void get_field(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray pathv;
  pathv = *msg;
  fields << pathv.data[0] << endr
	   << pathv.data[1] << endr
	   << pathv.data[2] << endr;
//   fields.print();
}

void get_field_euler(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray pathv;
  pathv = *msg;
  fields_euler << pathv.data[2] << endr
	   << pathv.data[1] << endr
	   << pathv.data[0] << endr;
//   fields.print();
}

void get_euler(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray pathv;
  pathv = *msg;
  euler_flag = 1;
  euler << pathv.data[2] << endr
	   << pathv.data[1] << endr
	   << pathv.data[0] << endr;
//   fields.print();
}

void obs_callback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray vob;
  vob = *msg;
  v_obs << vob.data[0] << endr
	<< vob.data[1] << endr
	<< vob.data[2] << endr
	<< vob.data[3] << endr
	<< vob.data[4] << endr
	<< vob.data[5] << endr
	<< vob.data[6] << endr
	<< vob.data[7] << endr
	<< vob.data[8] << endr;
}

// void get_noise(const std_msgs::Float64MultiArray::ConstPtr& msg)
// {
//   std_msgs::Float64MultiArray noise;
//   noise = *msg;
//   noise_mat << noise.data[0] << endr
// 	    << noise.data[0] << endr
// 	    << noise.data[0] << endr;
// }
//
// void get_stiff(const std_msgs::Float64MultiArray::ConstPtr& msg)
// {
//   std_msgs::Float64MultiArray stiff;
//   stiff = *msg;
//   stiff_vec = stiff.data[0];
// }
//

void jacobi_callback(const continuum_manipulator::JacobianField::ConstPtr& msg)
{
  continuum_manipulator::JacobianField jacobi_get;
  jacobi_get = *msg;
  sensor_jacobi_raw.set_size(jacobi_get.jacobian.row, jacobi_get.jacobian.column);
  unsigned char array_in = 0;
  for(unsigned char row_in = 0; row_in<jacobi_get.jacobian.row; row_in++)
  {
    for(unsigned char col_in = 0; col_in<jacobi_get.jacobian.column; col_in++)
    {
      sensor_jacobi_raw(row_in,col_in) = jacobi_get.jacobian.matrix_data[array_in];
      array_in++;
    }
  }
  obstacle_field << jacobi_get.vector_field.x << endr
		            << jacobi_get.vector_field.y << endr
                << jacobi_get.vector_field.z << endr;
  flag_jacobi = 1;
}

void red_callback(const std_msgs::Float64::ConstPtr& msg)
{
  std_msgs::Float64 var;
  var = *msg;
  red_weight = var.data;
}

void initialize()
{
//   e_vector << 0.0 << endr
// 	   << 0.0 << endr
// 	   << 0.0 << endr;
  fields << 0.0 << endr
	 << 0.0 << endr
	 << 0.0 << endr;
  v_obs << 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr;
  v_config << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr;
  noise_mat << 0.0 << endr
	    << 0.0 << endr
	    << 0.0 << endr;
  unsigned char ind = 0;
  for(unsigned char i=0; i<3; i++)
  {
    for(unsigned char j=0; j<3; j++)
    {
      len[i][j] = y[ind];
      ind++;
    }
  }

  c_to_q();
  cout << k_val[0] << " " << k_val[1] << " " << k_val[2] << endl;
  cout << p_val[0] << " " << p_val[1] << " " << p_val[2] << endl;
  cout << s_val[0] << " " << s_val[1] << " " << s_val[2] << endl;
  flag_error = 0;
  euler_flag = 0;
}

int
main(int argc, char** argv)
  {
    std_msgs::Float64MultiArray vel_send, field_send, kps, length, kps_noi, length_noisy;
    ros::init(argc,argv, "inv_jacobi_3");
    ros::NodeHandle n;
    n.param("guide", guide, guide);
    initialize();
//     n.param("noise", noise_flag, noise_flag);
    n.param("freq", freq_int, freq_int);
    n.param("length_on", length_active, length_active);
    n.param("euler_on", orient_active, orient_active);
    n.param("fieldtype", field_type, field_type);
     if(freq_int!=0)
    {
      freq = (double)(freq_int);
    }
    ros::Rate loop_rate(freq);

//     ros::Publisher vel_pub = n.advertise<std_msgs::Float64MultiArray>("motor_speed",10);
//     ros::Publisher pub = n.advertise<std_msgs::Float64MultiArray>("observer/cspace/true",10);
//     ros::Publisher c_noisy = n.advertise<std_msgs::Float64MultiArray>("observer/cspace/noisy",10);
//     ros::Publisher length_pub = n.advertise<std_msgs::Float64MultiArray>("observer/length/true",10);
//     ros::Publisher length_n_pub = n.advertise<std_msgs::Float64MultiArray>("observer/length/noisy",10);
//     ros::Publisher base_pub = n.advertise<std_msgs::Float64MultiArray>("observer/base/true",10);
//     ros::Publisher base_n_pub = n.advertise<std_msgs::Float64MultiArray>("observer/base/noisy",10);
//     ros::Publisher field_pub = n.advertise<std_msgs::Float64MultiArray>("motion_plan/goal_field",10);
    ros::Publisher pub = n.advertise<std_msgs::Float64MultiArray>("configuration_space",10);
    ros::Publisher length_pub = n.advertise<std_msgs::Float64MultiArray>("length",10);
    ros::Publisher trial_pub = n.advertise<std_msgs::Float64MultiArray>("configuration_field",10);
    ros::Publisher dot_pub = n.advertise<std_msgs::Float64MultiArray>("length_rate",10);

//     ros::Subscriber error_sub = n.subscribe("error", 10, get_error);
    ros::Subscriber field_sub = n.subscribe("field", 10, get_field);
    ros::Subscriber euler_field_sub = n.subscribe("field_euler", 10, get_field_euler);
    ros::Subscriber euler_sub = n.subscribe("euler", 10, get_euler);
    ros::Subscriber rec_vel_obs = n.subscribe("obs_field_configuration_space", 10, obs_callback);
    ros::Subscriber jacobi_sub = n.subscribe("sensor_jacobi", 10, jacobi_callback);
    ros::Subscriber redundancy_sub = n.subscribe("redundancy_weight", 10, red_callback);

//     ros::Subscriber field_sub = n.subscribe("error", 10, get_field);
//     ros::Subscriber ref_sub = n.subscribe("ref_speed", 10, get_ref);
//     ros::Subscriber rec_vel_obs = n.subscribe("motion_plan/obs_field/cspace", 10, obs_callback);
//     ros::Subscriber procnoi_sub = n.subscribe("process_noise", 10, get_noise);
//     ros::Subscriber stiff_sub = n.subscribe("stiffness/goal", 10, get_stiff);

    begin = ros::Time::now();
    mat field_out;

    while(ros::ok() && (flag_error == 0))
    {
      n.getParam("/moving", flag_move);
//       vel_send.data.clear();
      kps.data.clear();
//       kps_noi.data.clear();
      length.data.clear();
//       length_noisy.data.clear();
      field_send.data.clear();
      double angle_dummy[3] = {0.0, 0.0, 0.0};
      Jv = pass_Jv(angle_dummy, k_val, p_val, s_val);
      Jw = pass_Jw(k_val, p_val, s_val);
      Jkpsl = pass_Jkpsl(len);
//       Jkpsl.print();
      // cout << len[0,0] << endl;
      v_config = constraint_potential(len);
// cout << "a" << endl;
// cout << orient_active << endl;
      integral_length();
// cout << "b" << endl;
      unsigned char ind = 0;
      for(unsigned char i=0; i<3; i++)
      {
	for(unsigned char j=0; j<3; j++)
	{
	  length.data.push_back(len[i][j]);
	  field_send.data.push_back(rate[ind]);
	  ind++;
	}
      }

      for(unsigned char i=0; i<3; i++)
      {
	kps.data.push_back(k_val[i]);
	kps.data.push_back(p_val[i]);
	kps.data.push_back(s_val[i]);
      }

      pub.publish(kps);
      length_pub.publish(length);
      trial_pub.publish(config_field);
      dot_pub.publish(field_send);
//       c_noisy.publish(kps_noi);
//       length_n_pub.publish(length_noisy);
      ros::spinOnce();
      loop_rate.sleep();
      // cout << "c" << endl;
    }
    return 0;
  }
