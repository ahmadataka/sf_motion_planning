#include "ros/ros.h"
#include "geometry_msgs/Pose.h"
#include "std_msgs/Float64MultiArray.h"
#include <iostream>
#include <armadillo>
#include <math.h>

using namespace std;
using namespace arma;

// Armadillo documentation is available at:
// http://arma.sourceforge.net/docs.html

// Global Variable
float d = 0.0134; // radius of manipulator
float L0 = 0.12; // normal length
float freq = 40.0;
float dt = 1.0/freq;
float l[] = {0.1232, 0.1203, 0.1123}, l_noise[] = {0.1232, 0.1203, 0.1123}, rate[3];
float k_val, p_val, s_val;
float mu, sigma;

mat tip_pose, v, state;
mat A, B, C, P, Q, R;
mat Jg, Jh;

float l_to_k(float l1, float l2, float l3)
{
    float k_out = 2*(pow(pow(l1,2)+pow(l2,2)+pow(l3,2)-l1*l2-l1*l3-l2*l3,0.5))/(d*(l1+l2+l3));
    
    return k_out;
}

float l_to_phi(float l_1, float l_2, float l_3)
{
    float phi_out = atan2((sqrt(3)*(l_2+l_3-2*l_1)),(3*(l_2-l_3)));
    
    return phi_out;
}

float l_to_s(float l1, float l2, float l3)
{
    float s_out = (l1+l2+l3)/3;
    
    return s_out;
}

mat pass_Jg(float l1, float l2, float l3)
{
    float l_sum = l1+l2+l3;
    float l_sqrt = sqrt(pow(l1,2)+pow(l2,2)+pow(l3,2)-l1*l2-l1*l3-l2*l3);
    mat Jg_ex;
    Jg_ex << 3*(l1*l2+l1*l3-pow(l2,2)-pow(l3,2))/(d*pow(l_sum,2)*l_sqrt) << -3*(pow(l1,2)-l1*l2-l2*l3+pow(l3,2))/(d*pow(l_sum,2)*l_sqrt) << -3*(pow(l1,2)-l1*l3-l2*l3+pow(l2,2))/(d*pow(l_sum,2)*l_sqrt) << endr
	  << sqrt(3)*(l3-l2)/(2*pow(l_sqrt,2)) << sqrt(3)*(l1-l3)/(2*pow(l_sqrt,2)) << sqrt(3)*(l2-l1)/(2*pow(l_sqrt,2)) << endr
	  << 1.0/3.0 << 1.0/3.0 << 1.0/3.0 << endr;
    
    return Jg_ex;
}

mat pass_Jh(float k, float p, float s)
{
    mat Jh_ex;
    Jh_ex << cos(p)*(k*s*sin(k*s)+cos(k*s)-1)/pow(k,2) << -sin(p)*(1-cos(k*s))/k << cos(p)*sin(k*s) << endr
	  << sin(p)*(k*s*sin(k*s)+cos(k*s)-1)/pow(k,2) << cos(p)*(1-cos(k*s))/k << sin(p)*sin(k*s) << endr
	  << (-sin(k*s)+k*s*cos(k*s))/pow(k,2) << 0 << cos(k*s) << endr;
    return Jh_ex;
}

void c_to_q()
{
  k_val = l_to_k(state(0,0),state(1,0),state(2,0));
  p_val = l_to_phi(state(0,0),state(1,0),state(2,0));
  s_val = l_to_s(state(0,0),state(1,0),state(2,0));
}

mat homogeneous_transform(float kappa_sym, float phi_sym, float s_sym)
{
  mat T;
  T << cos(phi_sym)*cos(kappa_sym*s_sym) << -sin(phi_sym) << cos(phi_sym)*sin(kappa_sym*s_sym) << cos(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym << endr
    << sin(phi_sym)*cos(kappa_sym*s_sym) << cos(phi_sym) << sin(phi_sym)*sin(kappa_sym*s_sym) << sin(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym << endr
    << -sin(kappa_sym*s_sym) << 0 << cos(kappa_sym*s_sym) << sin(kappa_sym*s_sym)/kappa_sym << endr
    << 0 << 0 << 0 << 1 << endr;
  return T;
}  
    
mat homo_to_pose(mat T)
{
  mat position;
  position << T(0,3) << endr
	   << T(1,3) << endr
	   << T(2,3) << endr;
  return position;
}

void integral_length()
{
  mat u, Ju, P_dum, state_dummy, matrix_dum, Kalman;
  float k_dum, p_dum, s_dum;
  
  u = v;
  
  // Extended Kalman Filter
  P_dum = A*P*A.t() + Q;
  state_dummy = A*state + B*u;
  k_dum = l_to_k(state_dummy(0,0),state_dummy(1,0),state_dummy(2,0));
  p_dum = l_to_phi(state_dummy(0,0),state_dummy(1,0),state_dummy(2,0));
  s_dum = l_to_s(state_dummy(0,0),state_dummy(1,0),state_dummy(2,0));
  
  Jg = pass_Jg(state_dummy(0,0),state_dummy(1,0),state_dummy(2,0));
  Jh = pass_Jh(k_dum, p_dum, s_dum);
  Ju = Jh*Jg;
  C = Ju;
  
  matrix_dum = (C*P_dum*C.t()+R);
  if(det(matrix_dum) != 0) Kalman = P_dum*C.t()*inv(matrix_dum);
  else cout << "DET=0" << endl;
  
  state = state_dummy + Kalman*(tip_pose - homo_to_pose(homogeneous_transform(k_dum, p_dum, s_dum)));
  P = (eye<mat>(3,3)-Kalman*C)*P_dum;
  c_to_q();
}
    
void get_pose(const geometry_msgs::Pose::ConstPtr& msg)
{
  geometry_msgs::Pose pos;
  pos = *msg;
  tip_pose << pos.position.x << endr
	   << pos.position.y << endr
	   << pos.position.z << endr;
}

void get_speed(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray sp;
  sp = *msg;
  v << sp.data[0] << endr
    << sp.data[1] << endr
    << sp.data[2] << endr;
}

void initialize()
{
  A = eye<mat>(3,3);
  B = dt*eye<mat>(3,3);
  tip_pose << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr;
  v << 0.0 << endr
    << 0.0 << endr
    << 0.0 << endr;
  
  // Kalman initialization
  mu = 0; sigma = 0.005;
  state << L0+0.01 << endr
	<< L0-0.01 << endr
	<< L0 << endr;
  c_to_q();
  P = eye<mat>(3,3);
  Q = 0.0000000001*eye<mat>(3,3);
  R = pow(sigma,2)*eye<mat>(3,3);
}

int
main(int argc, char** argv)
  { 
    std_msgs::Float64MultiArray state_send, kps;
    ros::init(argc,argv, "observer");
    ros::NodeHandle n;
    
    initialize();
    
    ros::Rate loop_rate(freq);
    
    ros::Publisher state_pub = n.advertise<std_msgs::Float64MultiArray>("observer/length/estimate",10);
    ros::Publisher cspace_pub = n.advertise<std_msgs::Float64MultiArray>("observer/cspace/estimate",10);
    
    ros::Subscriber speed_sub = n.subscribe("motor_speed", 10, get_speed);
    ros::Subscriber pose_sub = n.subscribe("observer/tip/noisy", 10, get_pose);
    
    while(ros::ok())
    {
      state_send.data.clear();
      kps.data.clear();
//       cout << "TES" << endl;
      integral_length();
      for(unsigned char i=0; i<3; i++)
      {
	state_send.data.push_back(state(i,0));
      }
      kps.data.push_back(k_val);
      kps.data.push_back(p_val);
      kps.data.push_back(s_val);
      
      state_pub.publish(state_send);
      cspace_pub.publish(kps);
      ros::spinOnce();
      loop_rate.sleep();
    }
    return 0;
  }