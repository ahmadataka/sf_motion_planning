#include "ros/ros.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseArray.h"
#include "std_msgs/Float64MultiArray.h"
#include <iostream>
#include <armadillo>
#include <math.h>
#include <vector>

using namespace std;
using namespace arma;

// Armadillo documentation is available at:
// http://arma.sourceforge.net/docs.html

// Global Variable
float freq = 40.0;
float dt = 1.0/freq;
float k, p, s;
//float K2 = 0.000000002, rho_0 = 0.025;
//float K2 = 0.000000002, rho_0 = 0.05;
float K2 = 0.000000002, rho_0 = 0.065;
int field_type, obs_num;
int num_per_seg = 5;
float max_vel = 0.07, maks = 0.0, mini = 100.0;
float  stiff_vec;    
mat obs[10];
mat field_sum, field_sum_sens, go_to_goal, sensor_jacobian;

mat pass_Jh(float k, float p, float s)
{
    mat Jh_ex;
    Jh_ex << cos(p)*(k*s*sin(k*s)+cos(k*s)-1)/pow(k,2) << -sin(p)*(1-cos(k*s))/k << cos(p)*sin(k*s) << endr
	  << sin(p)*(k*s*sin(k*s)+cos(k*s)-1)/pow(k,2) << cos(p)*(1-cos(k*s))/k << sin(p)*sin(k*s) << endr
	  << (-sin(k*s)+k*s*cos(k*s))/pow(k,2) << 0 << cos(k*s) << endr;
    return Jh_ex;
}
    
mat homogeneous_transform(float kappa_sym, float phi_sym, float s_sym)
{
  mat T;
  T << cos(phi_sym)*cos(kappa_sym*s_sym) << -sin(phi_sym) << cos(phi_sym)*sin(kappa_sym*s_sym) << cos(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym << endr
    << sin(phi_sym)*cos(kappa_sym*s_sym) << cos(phi_sym) << sin(phi_sym)*sin(kappa_sym*s_sym) << sin(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym << endr
    << -sin(kappa_sym*s_sym) << 0 << cos(kappa_sym*s_sym) << sin(kappa_sym*s_sym)/kappa_sym << endr
    << 0 << 0 << 0 << 1 << endr;
  return T;
}  
    
geometry_msgs::Pose homo_to_pose(mat T)
{
    geometry_msgs::Pose pose;
    pose.position.x = T(0,3);
    pose.position.y = T(1,3);
    pose.position.z = T(2,3);
    pose.orientation.w = 0.5*sqrt(1+T(0,0)+T(1,1)+T(2,2));
    pose.orientation.x = (T(2,1)-T(1,2))/(4*pose.orientation.w);
    pose.orientation.y = (T(0,2)-T(2,0))/(4*pose.orientation.w);
    pose.orientation.z = (T(1,0)-T(0,1))/(4*pose.orientation.w);
    
    return pose;
}

mat homo_to_pose_sym(mat T)
{
  mat position;
  position << T(0,3) << endr
	   << T(1,3) << endr
	   << T(2,3) << endr;
  return position;
}

mat zeros_mat()
{
  mat zero_mat;
  zero_mat << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr;
	   
  return zero_mat;
}

mat obstacle_field(mat pose_of_sensor, unsigned char obs_index)
{
  float d = 0.0134; // radius of manipulator
  float obs_range = 0.01;
  float speed;
  mat velocity, stiff_mat;
  mat vector_obs = pose_of_sensor - obs[obs_index];
  mat dummy = vector_obs.t()*vector_obs;
  float rho = sqrt((float)(dummy(0,0)));
  float rho_modify = rho - d - obs_range;
  if(rho_modify < mini) mini = rho_modify;
  stiff_mat << stiff_vec << 0 << 0 << endr
	    << 0 << stiff_vec << 0 << endr
	    << 0 << 0 << stiff_vec << endr;
  mat ident = eye<mat>(3,3);
  if(field_type == 0)
  {
    if(rho_modify<rho_0) velocity = K2*stiff_mat*(vector_obs/rho)*(1/rho_modify - 1/rho_0)/pow(rho_modify,2);
    else velocity = zeros_mat();
  }
  else //if(field_type == 1)
  {
    if(rho_modify<rho_0)
    {
      if(rho_modify<0.005)
      {
	cout << "rho:" << rho_modify << endl;
	rho_modify = 0.005;
      }
      speed = K2*stiff_vec*(1/rho_modify - 1/rho_0)/pow(rho_modify,2);
      if(speed > maks)
      {
	maks = speed;
	cout << "maks: " << maks << endl;
      }
      velocity = speed*ident*(vector_obs/rho);
    }
    else velocity = zeros_mat();
  }
  
  return velocity;
}

float get_dist(mat pose_of_sensor, unsigned char obs_index)
{
  float d = 0.0134; // radius of manipulator
  float obs_range = 0.01;
  mat distance;
  mat vector_obs = pose_of_sensor - obs[obs_index];
  mat dummy = vector_obs.t()*vector_obs;
  float rho = sqrt((float)(dummy(0,0)));
  float rho_modify = rho - d - obs_range;
  
  return rho_modify;
}


void get_obs(const geometry_msgs::PoseArray::ConstPtr& msg)
{
  geometry_msgs::PoseArray env;
  env = *msg;
  
  for(unsigned char i=0; i<obs_num; i++)
  {
    obs[i] << env.poses[i].position.x << endr
	   << env.poses[i].position.y << endr
	   << env.poses[i].position.z << endr;
  }
}

void get_kps(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray kps;
  kps = *msg;
  k = kps.data[0];
  p = kps.data[1];
  s = kps.data[2];
}

void vel_callback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray vel;
  vel = *msg;
  go_to_goal << vel.data[0] << endr
	     << vel.data[1] << endr
	     << vel.data[2] << endr;
}

void get_stiff(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray stiff;
  stiff = *msg;
  stiff_vec = stiff.data[0];
}

void initialize()
{
  field_sum << 0.0 << endr
	    << 0.0 << endr
	    << 0.0 << endr;
  for(unsigned char i=0; i<obs_num; i++)
  {
    obs[i] << 1.0 << endr
	   << 1.0 << endr
	   << 1.0 << endr;
  }
  go_to_goal << 0.0 << endr
	     << 0.0 << endr
	     << 0.0 << endr;
  k = 4.10176630034;
  p = -0.783358622128065;
  s = 0.1186;
}

int
main(int argc, char** argv)
  { 
    std_msgs::Float64MultiArray send_obs, send_obs_sens, send_dist;
    geometry_msgs::PoseArray sensor_pose;
    mat sensor_matrix, velocity_field;
    vector<float> dist_to_obs;
    ros::init(argc,argv, "sensor_pose_node");
    ros::NodeHandle n;
    n.param("fieldtype", field_type, field_type);
    n.param("obs_num", obs_num, obs_num);
    
    initialize();
    
    ros::Rate loop_rate(freq);
    
    ros::Publisher sensor_pub = n.advertise<geometry_msgs::PoseArray>("sensor_pose",10);
    ros::Publisher vel_obs_pub = n.advertise<std_msgs::Float64MultiArray>("motion_plan/obs_field/cspace",10);
    ros::Publisher vel_sensor_pub = n.advertise<std_msgs::Float64MultiArray>("motion_plan/obs_field/tspace",10);
    ros::Publisher dist_obs_pub = n.advertise<std_msgs::Float64MultiArray>("min_distance",10);
    
    ros::Subscriber obs_sub = n.subscribe("obs_pose", 10, get_obs);
    ros::Subscriber config_space_sub = n.subscribe("observer/cspace/estimate", 10, get_kps);
    ros::Subscriber rec_vel = n.subscribe("motion_plan/goal_field", 10, vel_callback);
    ros::Subscriber stiff_sub = n.subscribe("stiffness/obs", 10, get_stiff);
    
//     obs[0].print();
    
    while(ros::ok())
    {
      sensor_pose.poses.clear();
      send_obs_sens.data.clear();
      dist_to_obs.clear();
      send_dist.data.clear();
      field_sum = zeros_mat();
      field_sum_sens = zeros_mat();
      float maxi = 10.0;
      unsigned char sensor_max, obs_max, index_max, index=0;
      for(unsigned char j=0; j<num_per_seg; j++)
      {
	sensor_matrix = homogeneous_transform(k, p, (j+1)*s/(num_per_seg));
	sensor_pose.poses.push_back(homo_to_pose(sensor_matrix));

	sensor_jacobian = pass_Jh(k, p, (j+1)*s/(num_per_seg));
	for(unsigned char h = 0; h<obs_num; h++)
	{
	  velocity_field = obstacle_field(homo_to_pose_sym(sensor_matrix),h);
	  dist_to_obs.push_back(get_dist(homo_to_pose_sym(sensor_matrix),h));
	  if((field_type == 0) || (field_type==1))
	  {
	    field_sum += inv(sensor_jacobian)*velocity_field;
	    field_sum_sens += velocity_field;
	  }
	  else
	  {
	    if(get_dist(homo_to_pose_sym(sensor_matrix),h) < maxi)
	    {
	      maxi = get_dist(homo_to_pose_sym(sensor_matrix),h);
	      sensor_max = j;
	      obs_max = h;
	      index_max = index;
	      field_sum_sens = velocity_field;
	      field_sum = pinv(sensor_jacobian)*velocity_field;
	    }
	    index++;
	  }
	}
	for(unsigned char h = 0; h<3; h++)
	{
	  send_obs_sens.data.push_back(field_sum_sens(h,0));
	}
      }
      
      
      float max = 0.0;
      for(unsigned char i=0; i<(num_per_seg*obs_num); i++)
      {
	if(dist_to_obs[i] > max) max = dist_to_obs[i];
      }
      send_dist.data.push_back(max);
      
      send_obs.data.clear();
      for(unsigned char i=0; i<3; i++)
      {
	send_obs.data.push_back(field_sum(i,0));
      }
      
      vel_obs_pub.publish(send_obs);
      sensor_pub.publish(sensor_pose);
      vel_sensor_pub.publish(send_obs_sens);
      dist_obs_pub.publish(send_dist);
      ros::spinOnce();
      loop_rate.sleep();
    }
    return 0;
  }