#include "ros/ros.h"
#include "geometry_msgs/Pose.h"
#include "std_msgs/Float64MultiArray.h"
#include "geometry_msgs/Pose.h"
#include <iostream>
#include <armadillo>
#include <math.h>
// #include <random>

using namespace std;
using namespace arma;

// Armadillo documentation is available at:
// http://arma.sourceforge.net/docs.html

// It is IMPORTANT to be noted that l variable here stands for change of length 
// Global Variable
double d = 0.0134; // diameter of manipulator
double freq = 40.0;
double dt = 1.0/freq;
// double dt = 0.01;
double Lo = 0.1200;
double y[] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.002185073211087172, 0.013844107151657448, 0.02127896636075233, 0.011625708022642836, -0.0015575909147796168, 0.026584259563230038, 0.009536396107657672, 0.011578932751209454, 0.014420456620548895};
// double y[] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.002, 0.003, 0.003, 0.002, 0.001, 0.001, 0.002, 0.003};
double len[3][3], l_noise[9], rate[16], position_base[3], angle_base[3];
double k_val[3], p_val[3], s_val[3];
double k_noi, p_noi, s_noi;
double K_config = 0.005;
unsigned char flag_error;
int noise_flag;
double  stiff_vec;
double s_min = -0.3*Lo, s_max = 0.3*Lo, s_lim = 0.005;
mat e_vector, path_vel, v_obs, fields, noise_mat, v_config;
mat Jv, Jvb, Jkpsl;
int flag_try = 0;
std_msgs::Float64MultiArray config_field;

double l_to_k(double l1, double l2, double l3)
{
    double k_out = 2*(pow(pow(l1,2)+pow(l2,2)+pow(l3,2)-l1*l2-l1*l3-l2*l3,0.5))/(d*(3*Lo+l1+l2+l3));
    
    return k_out;
}

double l_to_phi(double l_1, double l_2, double l_3)
{
    double phi_out = atan2((sqrt(3)*(l_2+l_3-2*l_1)),(3*(l_2-l_3)));
    
    return phi_out;
}

double l_to_s(double l1, double l2, double l3)
{
    double s_out = (3*Lo+l1+l2+l3)/3;
    
    return s_out;
}

mat pass_Jkpsl(double l[3][3])
{
	double jac_kpsl[9][9];
	
	jac_kpsl[0][0] = 2*(l[0][0] - l[0][1]/2 - l[0][2]/2)/((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824)*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))) - 0.0268*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))/pow((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824),2);
	jac_kpsl[0][1] = 2*(-l[0][0]/2 + l[0][1] - l[0][2]/2)/((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824)*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))) - 0.0268*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))/pow((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824),2);
	jac_kpsl[0][2] = 2*(-l[0][0]/2 - l[0][1]/2 + l[0][2])/((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824)*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))) - 0.0268*sqrt(pow(l[0][0], 2) - l[0][0]*l[0][1] - l[0][0]*l[0][2] + pow(l[0][1], 2) - l[0][1]*l[0][2] + pow(l[0][2], 2))/pow((0.0134*l[0][0] + 0.0134*l[0][1] + 0.0134*l[0][2] + 0.004824),2);
	jac_kpsl[0][3] = 0.0;
	jac_kpsl[0][4] = 0.0;
	jac_kpsl[0][5] = 0.0;
	jac_kpsl[0][6] = 0.0;
	jac_kpsl[0][7] = 0.0;
	jac_kpsl[0][8] = 0.0;
	jac_kpsl[1][0] = -2*sqrt(3)/((1 + 3*pow((-2*l[0][0] + l[0][1] + l[0][2]),2)/pow((3*l[0][1] - 3*l[0][2]),2))*(3*l[0][1] - 3*l[0][2]));
	jac_kpsl[1][1] = (sqrt(3)/(3*l[0][1] - 3*l[0][2]) - 3*sqrt(3)*(-2*l[0][0] + l[0][1] + l[0][2])/pow((3*l[0][1] - 3*l[0][2]),2))/(1 + 3*pow((-2*l[0][0] + l[0][1] + l[0][2]),2)/pow((3*l[0][1] - 3*l[0][2]),2));
	jac_kpsl[1][2] = (sqrt(3)/(3*l[0][1] - 3*l[0][2]) + 3*sqrt(3)*(-2*l[0][0] + l[0][1] + l[0][2])/pow((3*l[0][1] - 3*l[0][2]),2))/(1 + 3*pow((-2*l[0][0] + l[0][1] + l[0][2]),2)/pow((3*l[0][1] - 3*l[0][2]),2));
	jac_kpsl[1][3] = 0.0;
	jac_kpsl[1][4] = 0.0;
	jac_kpsl[1][5] = 0.0;
	jac_kpsl[1][6] = 0.0;
	jac_kpsl[1][7] = 0.0;
	jac_kpsl[1][8] = 0.0;
	jac_kpsl[2][0] = 1.0/3.0;
	jac_kpsl[2][1] = 1.0/3.0;
	jac_kpsl[2][2] = 1.0/3.0;
	jac_kpsl[2][3] = 0.0;
	jac_kpsl[2][4] = 0.0;
	jac_kpsl[2][5] = 0.0;
	jac_kpsl[2][6] = 0.0;
	jac_kpsl[2][7] = 0.0;
	jac_kpsl[2][8] = 0.0;
	jac_kpsl[3][0] = 0.0;
	jac_kpsl[3][1] = 0.0;
	jac_kpsl[3][2] = 0.0;
	jac_kpsl[3][3] = 2*(l[1][0] - l[1][1]/2 - l[1][2]/2)/((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824)*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))) - 0.0268*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))/pow((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824),2);
	jac_kpsl[3][4] = 2*(-l[1][0]/2 + l[1][1] - l[1][2]/2)/((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824)*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))) - 0.0268*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))/pow((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824),2);
	jac_kpsl[3][5] = 2*(-l[1][0]/2 - l[1][1]/2 + l[1][2])/((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824)*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))) - 0.0268*sqrt(pow(l[1][0], 2) - l[1][0]*l[1][1] - l[1][0]*l[1][2] + pow(l[1][1], 2) - l[1][1]*l[1][2] + pow(l[1][2], 2))/pow((0.0134*l[1][0] + 0.0134*l[1][1] + 0.0134*l[1][2] + 0.004824),2);
	jac_kpsl[3][6] = 0.0;
	jac_kpsl[3][7] = 0.0;
	jac_kpsl[3][8] = 0.0;
	jac_kpsl[4][0] = 0.0;
	jac_kpsl[4][1] = 0.0;
	jac_kpsl[4][2] = 0.0;
	jac_kpsl[4][3] = -2*sqrt(3)/((1 + 3*pow((-2*l[1][0] + l[1][1] + l[1][2]),2)/pow((3*l[1][1] - 3*l[1][2]),2))*(3*l[1][1] - 3*l[1][2]));
	jac_kpsl[4][4] = (sqrt(3)/(3*l[1][1] - 3*l[1][2]) - 3*sqrt(3)*(-2*l[1][0] + l[1][1] + l[1][2])/pow((3*l[1][1] - 3*l[1][2]),2))/(1 + 3*pow((-2*l[1][0] + l[1][1] + l[1][2]),2)/pow((3*l[1][1] - 3*l[1][2]),2));
	jac_kpsl[4][5] = (sqrt(3)/(3*l[1][1] - 3*l[1][2]) + 3*sqrt(3)*(-2*l[1][0] + l[1][1] + l[1][2])/pow((3*l[1][1] - 3*l[1][2]),2))/(1 + 3*pow((-2*l[1][0] + l[1][1] + l[1][2]),2)/pow((3*l[1][1] - 3*l[1][2]),2));
	jac_kpsl[4][6] = 0.0;
	jac_kpsl[4][7] = 0.0;
	jac_kpsl[4][8] = 0.0;
	jac_kpsl[5][0] = 0.0;
	jac_kpsl[5][1] = 0.0;
	jac_kpsl[5][2] = 0.0;
	jac_kpsl[5][3] = 1.0/3.0;
	jac_kpsl[5][4] = 1.0/3.0;
	jac_kpsl[5][5] = 1.0/3.0;
	jac_kpsl[5][6] = 0.0;
	jac_kpsl[5][7] = 0.0;
	jac_kpsl[5][8] = 0.0;
	jac_kpsl[6][0] = 0.0;
	jac_kpsl[6][1] = 0.0;
	jac_kpsl[6][2] = 0.0;
	jac_kpsl[6][4] = 0.0;
	jac_kpsl[6][3] = 0.0;
	jac_kpsl[6][5] = 0.0;
	jac_kpsl[6][6] = 2*(l[2][0] - l[2][1]/2 - l[2][2]/2)/((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824)*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))) - 0.0268*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))/pow((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824),2);
	jac_kpsl[6][7] = 2*(-l[2][0]/2 + l[2][1] - l[2][2]/2)/((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824)*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))) - 0.0268*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))/pow((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824),2);
	jac_kpsl[6][8] = 2*(-l[2][0]/2 - l[2][1]/2 + l[2][2])/((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824)*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))) - 0.0268*sqrt(pow(l[2][0], 2) - l[2][0]*l[2][1] - l[2][0]*l[2][2] + pow(l[2][1], 2) - l[2][1]*l[2][2] + pow(l[2][2], 2))/pow((0.0134*l[2][0] + 0.0134*l[2][1] + 0.0134*l[2][2] + 0.004824),2);
	jac_kpsl[7][0] = 0.0;
	jac_kpsl[7][1] = 0.0;
	jac_kpsl[7][2] = 0.0;
	jac_kpsl[7][3] = 0.0;
	jac_kpsl[7][4] = 0.0;
	jac_kpsl[7][5] = 0.0;
	jac_kpsl[7][6] = -2*sqrt(3)/((1 + 3*pow((-2*l[2][0] + l[2][1] + l[2][2]),2)/pow((3*l[2][1] - 3*l[2][2]),2))*(3*l[2][1] - 3*l[2][2]));
	jac_kpsl[7][7] = (sqrt(3)/(3*l[2][1] - 3*l[2][2]) - 3*sqrt(3)*(-2*l[2][0] + l[2][1] + l[2][2])/pow((3*l[2][1] - 3*l[2][2]),2))/(1 + 3*pow((-2*l[2][0] + l[2][1] + l[2][2]),2)/pow((3*l[2][1] - 3*l[2][2]),2));
	jac_kpsl[7][8] = (sqrt(3)/(3*l[2][1] - 3*l[2][2]) + 3*sqrt(3)*(-2*l[2][0] + l[2][1] + l[2][2])/pow((3*l[2][1] - 3*l[2][2]),2))/(1 + 3*pow((-2*l[2][0] + l[2][1] + l[2][2]),2)/pow((3*l[2][1] - 3*l[2][2]),2));
	jac_kpsl[8][0] = 0.0;
	jac_kpsl[8][1] = 0.0;
	jac_kpsl[8][2] = 0.0;
	jac_kpsl[8][3] = 0.0;
	jac_kpsl[8][4] = 0.0;
	jac_kpsl[8][5] = 0.0;
	jac_kpsl[8][6] = 1.0/3.0;
	jac_kpsl[8][7] = 1.0/3.0;
	jac_kpsl[8][8] = 1.0/3.0;
	
	mat Jg_ex;
    Jg_ex << jac_kpsl[0][0] << jac_kpsl[0][1] << jac_kpsl[0][2] << jac_kpsl[0][3] << jac_kpsl[0][4] << jac_kpsl[0][5] << jac_kpsl[0][6] << jac_kpsl[0][7] << jac_kpsl[0][8] << endr
	  << jac_kpsl[1][0] << jac_kpsl[1][1] << jac_kpsl[1][2] << jac_kpsl[1][3] << jac_kpsl[1][4] << jac_kpsl[1][5] << jac_kpsl[1][6] << jac_kpsl[1][7] << jac_kpsl[1][8] << endr
	  << jac_kpsl[2][0] << jac_kpsl[2][1] << jac_kpsl[2][2] << jac_kpsl[2][3] << jac_kpsl[2][4] << jac_kpsl[2][5] << jac_kpsl[2][6] << jac_kpsl[2][7] << jac_kpsl[2][8] << endr
	  << jac_kpsl[3][0] << jac_kpsl[3][1] << jac_kpsl[3][2] << jac_kpsl[3][3] << jac_kpsl[3][4] << jac_kpsl[3][5] << jac_kpsl[3][6] << jac_kpsl[3][7] << jac_kpsl[3][8] << endr
	  << jac_kpsl[4][0] << jac_kpsl[4][1] << jac_kpsl[4][2] << jac_kpsl[4][3] << jac_kpsl[4][4] << jac_kpsl[4][5] << jac_kpsl[4][6] << jac_kpsl[4][7] << jac_kpsl[4][8] << endr
	  << jac_kpsl[5][0] << jac_kpsl[5][1] << jac_kpsl[5][2] << jac_kpsl[5][3] << jac_kpsl[5][4] << jac_kpsl[5][5] << jac_kpsl[5][6] << jac_kpsl[5][7] << jac_kpsl[5][8] << endr
	  << jac_kpsl[6][0] << jac_kpsl[6][1] << jac_kpsl[6][2] << jac_kpsl[6][3] << jac_kpsl[6][4] << jac_kpsl[6][5] << jac_kpsl[6][6] << jac_kpsl[6][7] << jac_kpsl[6][8] << endr
	  << jac_kpsl[7][0] << jac_kpsl[7][1] << jac_kpsl[7][2] << jac_kpsl[7][3] << jac_kpsl[7][4] << jac_kpsl[7][5] << jac_kpsl[7][6] << jac_kpsl[7][7] << jac_kpsl[7][8] << endr
	  << jac_kpsl[8][0] << jac_kpsl[8][1] << jac_kpsl[8][2] << jac_kpsl[8][3] << jac_kpsl[8][4] << jac_kpsl[8][5] << jac_kpsl[8][6] << jac_kpsl[8][7] << jac_kpsl[8][8] << endr;
    
    return Jg_ex;
}

mat pass_Jvb(double base_ang[3], double k[3], double p[3], double s[3])
{
	mat Jvb_ex;
	double jac_b[3][6];
	jac_b[0][0] = 1.0;
    jac_b[0][1] = 0.0;
    jac_b[0][2] = 0.0;
    jac_b[0][3] = (((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) + sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*(((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) + sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + (((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) + sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) + sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (-cos(k[1]*s[1]) + 1)*((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/k[1] + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/k[1] + (-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/k[0] + (-sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) + sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])/k[0] - (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])/k[0];
    jac_b[0][4] = ((sin(base_ang[1])*sin(p[0])*cos(base_ang[0]) + sin(base_ang[2])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(base_ang[0])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*sin(p[0])*cos(base_ang[0]) + sin(base_ang[2])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]) + cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(base_ang[0])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((sin(base_ang[1])*sin(p[0])*cos(base_ang[0]) + sin(base_ang[2])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]) + cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(base_ang[0])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (sin(base_ang[1])*sin(p[0])*cos(base_ang[0]) + sin(base_ang[2])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[1])*cos(base_ang[0])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])/k[1] + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]) + cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] - (-cos(k[0]*s[0]) + 1)*sin(base_ang[1])*cos(base_ang[0])*cos(p[0])/k[0] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[2])*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])/k[0] + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])/k[0];
    jac_b[0][5] = ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[1])*cos(p[0])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[1])*sin(k[1]*s[1])*cos(p[0]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])*cos(p[0])/k[1] + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/k[0] + (sin(base_ang[0])*cos(base_ang[2]) - sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])/k[0];
    jac_b[1][0] = 0.0;
    jac_b[1][1] = 1.0;
    jac_b[1][2] = 0.0;
    jac_b[1][3] = (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*cos(p[2])/k[2] + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (-cos(k[1]*s[1]) + 1)*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/k[1] + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/k[1] + (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])/k[0] + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/k[0] + (-cos(k[0]*s[0]) + 1)*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])/k[0];
    jac_b[1][4] = ((sin(base_ang[0])*sin(base_ang[1])*sin(p[0]) + sin(base_ang[0])*sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[0])*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((sin(base_ang[0])*sin(base_ang[1])*sin(p[0]) + sin(base_ang[0])*sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[0])*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[0])*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((sin(base_ang[0])*sin(base_ang[1])*sin(p[0]) + sin(base_ang[0])*sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[0])*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[0])*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (sin(base_ang[0])*sin(base_ang[1])*sin(p[0]) + sin(base_ang[0])*sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[0])*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])/k[1] + (-sin(base_ang[0])*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(base_ang[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] - (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*sin(base_ang[1])*cos(p[0])/k[0] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])/k[0] + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2])/k[0];
    jac_b[1][5] = (-(-(-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]))*sin(p[1]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((-(-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]))*sin(k[1]*s[1]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[1])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((-(-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]))*cos(k[1]*s[1]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[1])*sin(k[1]*s[1])*cos(p[0]))*sin(k[2]*s[2])/k[2] + (-(-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + ((-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]))*sin(k[1]*s[1])/k[1] + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])*cos(p[0])/k[1] + (-sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) - cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])/k[0] + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/k[0];
    jac_b[2][0] = 0.0;
    jac_b[2][1] = 0.0;
    jac_b[2][2] = 1.0;
    jac_b[2][3] = 0.0;
    jac_b[2][4] = ((-sin(base_ang[1])*sin(base_ang[2])*cos(p[0]) + sin(p[0])*cos(base_ang[1]))*cos(p[1]) - (-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[2]) - cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((-sin(base_ang[1])*sin(base_ang[2])*cos(p[0]) + sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0]) - sin(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[2]) - cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((-sin(base_ang[1])*sin(base_ang[2])*cos(p[0]) + sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0]) - sin(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[2]) - cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (-sin(base_ang[1])*sin(base_ang[2])*cos(p[0]) + sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[2]) - cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/k[1] + (-sin(base_ang[1])*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0]) - sin(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/k[1] - (-cos(k[0]*s[0]) + 1)*sin(base_ang[1])*sin(base_ang[2])*sin(p[0])/k[0] - (-cos(k[0]*s[0]) + 1)*cos(base_ang[1])*cos(p[0])/k[0] - sin(base_ang[1])*sin(k[0]*s[0])*cos(base_ang[2])/k[0];
    jac_b[2][5] = (-(sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(p[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(p[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(p[0])*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(p[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[2])*cos(base_ang[1])*cos(k[0]*s[0]) + sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1]) + sin(p[1])*cos(base_ang[1])*cos(base_ang[2])*cos(p[0])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(p[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-sin(base_ang[2])*cos(base_ang[1])*cos(k[0]*s[0]) + sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(k[1]*s[1]) + sin(p[1])*sin(k[1]*s[1])*cos(base_ang[1])*cos(base_ang[2])*cos(p[0]))*sin(k[2]*s[2])/k[2] + (sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1]) + sin(p[0])*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (-sin(base_ang[2])*cos(base_ang[1])*cos(k[0]*s[0]) + sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])/k[1] + (-cos(k[1]*s[1]) + 1)*sin(p[1])*cos(base_ang[1])*cos(base_ang[2])*cos(p[0])/k[1] + (-cos(k[0]*s[0]) + 1)*sin(p[0])*cos(base_ang[1])*cos(base_ang[2])/k[0] - sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])/k[0];
    
    Jvb_ex << jac_b[0][0] << jac_b[0][1] << jac_b[0][2] << jac_b[0][3] << jac_b[0][4] << jac_b[0][5] << endr
	  << jac_b[1][0] << jac_b[1][1] << jac_b[1][2] << jac_b[1][3] << jac_b[1][4] << jac_b[1][5] << endr
	  << jac_b[2][0] << jac_b[2][1] << jac_b[2][2] << jac_b[2][3] << jac_b[2][4] << jac_b[2][5] << endr;
	  
    return Jvb_ex;
}

mat pass_Jv(double base_ang[3], double k[3], double p[3], double s[3])
{
    mat Jv_ex;
    double jac_v[3][9];
    jac_v[0][0] = (-(-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + s[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + s[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (-s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + s[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] + s[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0])/k[0] + s[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0])/k[0] + s[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])/k[0] - (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])/pow(k[0],2) - (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/pow(k[0],2) - (-cos(k[0]*s[0]) + 1)*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])/pow(k[0],2);
    jac_v[0][1] = ((-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])*cos(p[0]) - sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(k[1]*s[1]) + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])*cos(p[0]) - sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(k[1]*s[1]) + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (-(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0]) - cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])*cos(p[0]) - sin(p[0])*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(k[1]*s[1])/k[1] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0])*cos(k[0]*s[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*(-cos(k[0]*s[0]) + 1)*cos(p[0])/k[0] - (-cos(k[0]*s[0]) + 1)*sin(p[0])*cos(base_ang[0])*cos(base_ang[1])/k[0];
    jac_v[0][2] = (sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]) + (-(-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + k[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + k[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) - k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (-k[0]*(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + k[0]*(-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + k[0]*cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1];
    jac_v[0][3] = (-cos(k[2]*s[2]) + 1)*(-s[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - s[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(p[2])/k[2] + (s[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + s[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + s[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1])/k[1] + s[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1])/k[1] + s[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1])/k[1] - ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/pow(k[1],2) - (-cos(k[1]*s[1]) + 1)*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/pow(k[1],2) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/pow(k[1],2);
    jac_v[0][4] = (-((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(k[1]*s[1])*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1])*cos(k[1]*s[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] - (-cos(k[1]*s[1]) + 1)*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])/k[1];
    jac_v[0][5] =  ((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + (-cos(k[2]*s[2]) + 1)*(-k[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - k[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(p[2])/k[2] + (k[1]*((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + k[1]*(-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2];
    jac_v[0][6] = s[2]*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2])/k[2] + s[2]*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(k[2]*s[2])/k[2] + s[2]*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])/k[2] - (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/pow(k[2],2) - (-cos(k[2]*s[2]) + 1)*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*cos(p[2])/pow(k[2],2) - (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/pow(k[2],2);
    jac_v[0][7] = (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(p[2])/k[2];
    jac_v[0][8] = (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*cos(p[1]) - (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]))*cos(k[2]*s[2]) + (((-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*cos(p[0]) - sin(p[0])*cos(base_ang[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) + (-(sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*cos(k[0]*s[0]) + cos(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[2]) + sin(base_ang[1])*cos(base_ang[0])*cos(base_ang[2]))*cos(k[0]*s[0]) + (-sin(base_ang[0])*cos(base_ang[2]) + sin(base_ang[1])*sin(base_ang[2])*cos(base_ang[0]))*sin(p[0])*sin(k[0]*s[0]) + sin(k[0]*s[0])*cos(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]);
    jac_v[1][0] = ((-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]) + (s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + s[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]) - (s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + s[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + s[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1] + s[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0])/k[0] + s[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0])/k[0] + s[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0])/k[0] - (sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[0]*s[0]) + 1)*sin(p[0])/pow(k[0],2) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0])/pow(k[0],2) - (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])/pow(k[0],2);
    jac_v[1][1] = ((-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])*cos(p[0]) - sin(base_ang[0])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])*cos(p[0]) - sin(base_ang[0])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (-(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0]) - sin(base_ang[0])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(k[0]*s[0])*cos(p[0]) - sin(base_ang[0])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]))*sin(k[1]*s[1])/k[1] + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0])*cos(k[0]*s[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*(-cos(k[0]*s[0]) + 1)*cos(p[0])/k[0] - (-cos(k[0]*s[0]) + 1)*sin(base_ang[0])*sin(p[0])*cos(base_ang[1])/k[0];
    jac_v[1][2] = (sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]) + ((-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]) + (k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + k[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]) - (k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + k[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(-k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) - k[0]*sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(p[1])/k[1] + (k[0]*(sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - k[0]*(sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + k[0]*sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])/k[1];
    jac_v[1][3] = (-cos(k[2]*s[2]) + 1)*(-s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1])/k[1] + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1])/k[1] + s[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1])/k[1] - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/pow(k[1],2) - (-cos(k[1]*s[1]) + 1)*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])/pow(k[1],2) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/pow(k[1],2);
    jac_v[1][4] = (-((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(k[1]*s[1])*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] - (-cos(k[1]*s[1]) + 1)*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1])/k[1];
    jac_v[1][5] = ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-cos(k[2]*s[2]) + 1)*(-k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + k[1]*((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2];
    jac_v[1][6] = s[2]*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2])/k[2] + s[2]*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2])/k[2] + s[2]*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])/k[2] - (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/pow(k[2],2) - (-cos(k[2]*s[2]) + 1)*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/pow(k[2],2) - (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/pow(k[2],2);
    jac_v[1][7] = (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(p[2])/k[2];
    jac_v[1][8] = (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2]) + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2]) + (((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*cos(p[0]) - sin(base_ang[0])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*sin(k[0]*s[0]) + (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*cos(k[0]*s[0]) + sin(base_ang[0])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + ((sin(base_ang[0])*sin(base_ang[1])*sin(base_ang[2]) + cos(base_ang[0])*cos(base_ang[2]))*sin(p[0])*cos(k[0]*s[0]) - (sin(base_ang[0])*sin(base_ang[1])*cos(base_ang[2]) - sin(base_ang[2])*cos(base_ang[0]))*sin(k[0]*s[0]) + sin(base_ang[0])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]);
    jac_v[2][0] = ((s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-s[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + s[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - (-s[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + s[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])/k[1] + (-s[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + s[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - s[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])/k[1] - s[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0])/k[0] + s[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1])/k[0] + s[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0])/k[0] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[1])*cos(p[0])/pow(k[0],2) - (-cos(k[0]*s[0]) + 1)*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])/pow(k[0],2) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2])/pow(k[0],2);
    jac_v[2][1] = ((sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*cos(p[1]) - (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*cos(k[1]*s[1]) - (sin(base_ang[1])*sin(p[0])*sin(k[0]*s[0]) + sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1]) + (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/k[2] + ((sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*sin(p[1])*sin(k[1]*s[1]) + (sin(base_ang[1])*sin(p[0])*sin(k[0]*s[0]) + sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*cos(k[1]*s[1]) + (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/k[2] + (sin(base_ang[1])*cos(p[0]) - sin(base_ang[2])*sin(p[0])*cos(base_ang[1]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/k[1] + (sin(base_ang[1])*sin(p[0])*sin(k[0]*s[0]) + sin(base_ang[2])*sin(k[0]*s[0])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])/k[1] + (sin(base_ang[1])*sin(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0])*cos(k[0]*s[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[1])*sin(p[0])/k[0] + (-cos(k[0]*s[0]) + 1)*sin(base_ang[2])*cos(base_ang[1])*cos(p[0])/k[0];
    jac_v[2][2] = -sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]) + ((k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])*cos(p[1]) + (-k[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + k[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])*cos(k[1]*s[1]) - (-k[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + k[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*(k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(p[1])*sin(p[2])/k[2] + (-cos(k[1]*s[1]) + 1)*(k[0]*sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) - k[0]*sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) - k[0]*cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(p[1])/k[1] + (-k[0]*sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + k[0]*sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - k[0]*sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])/k[1];
    jac_v[2][3] = (-cos(k[2]*s[2]) + 1)*(-s[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) - s[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) - s[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (s[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - s[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + s[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + s[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1])/k[1] + s[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1])/k[1] + s[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1])/k[1] - (sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*sin(p[1])/pow(k[1],2) - (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])/pow(k[1],2) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1])/pow(k[1],2);
    jac_v[2][4] = (-(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/k[2] + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(k[1]*s[1])*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1])*sin(k[1]*s[1]))*sin(k[2]*s[2])/k[2] + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1])*cos(k[1]*s[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] + (sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*(-cos(k[1]*s[1]) + 1)*cos(p[1])/k[1] - (-cos(k[1]*s[1]) + 1)*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1])/k[1];
    jac_v[2][5] = (sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]) + (-cos(k[2]*s[2]) + 1)*(-k[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) - k[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) - k[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(p[2])/k[2] + (k[1]*(sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - k[1]*(-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + k[1]*(-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])/k[2];
    jac_v[2][6] = s[2]*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2])/k[2] + s[2]*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2])/k[2] + s[2]*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2])/k[2] - ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*sin(p[2])/pow(k[2],2) - (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*cos(p[2])/pow(k[2],2) - ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*sin(k[2]*s[2])/pow(k[2],2);
    jac_v[2][7] = ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*(-cos(k[2]*s[2]) + 1)*cos(p[2])/k[2] - (-cos(k[2]*s[2]) + 1)*((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(p[2])/k[2];
    jac_v[2][8] = ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*cos(p[1]) - (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(p[1]))*sin(p[2])*sin(k[2]*s[2]) + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*sin(k[1]*s[1]) + (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*cos(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*sin(k[1]*s[1])*cos(p[1]))*cos(k[2]*s[2]) + ((sin(base_ang[1])*sin(p[0]) + sin(base_ang[2])*cos(base_ang[1])*cos(p[0]))*sin(p[1])*cos(k[1]*s[1]) - (-sin(base_ang[1])*sin(k[0]*s[0])*cos(p[0]) + sin(base_ang[2])*sin(p[0])*sin(k[0]*s[0])*cos(base_ang[1]) + cos(base_ang[1])*cos(base_ang[2])*cos(k[0]*s[0]))*sin(k[1]*s[1]) + (-sin(base_ang[1])*cos(p[0])*cos(k[0]*s[0]) + sin(base_ang[2])*sin(p[0])*cos(base_ang[1])*cos(k[0]*s[0]) - sin(k[0]*s[0])*cos(base_ang[1])*cos(base_ang[2]))*cos(p[1])*cos(k[1]*s[1]))*sin(k[2]*s[2])*cos(p[2]);
    
    
    Jv_ex << jac_v[0][0] << jac_v[0][1] << jac_v[0][2] << jac_v[0][3] << jac_v[0][4] << jac_v[0][5] << jac_v[0][6] << jac_v[0][7] << jac_v[0][8] << endr
	  << jac_v[1][0] << jac_v[1][1] << jac_v[1][2] << jac_v[1][3] << jac_v[1][4] << jac_v[1][5] << jac_v[1][6] << jac_v[1][7] << jac_v[1][8] << endr
	  << jac_v[2][0] << jac_v[2][1] << jac_v[2][2] << jac_v[2][3] << jac_v[2][4] << jac_v[2][5] << jac_v[2][6] << jac_v[2][7] << jac_v[2][8] << endr;
    return Jv_ex;
}

void c_to_q()
{ 
  for(unsigned char i=0; i<3; i++)
  {
    k_val[i] = l_to_k(len[i][0],len[i][1],len[i][2]);
    p_val[i] = l_to_phi(len[i][0],len[i][1],len[i][2]);
    s_val[i] = l_to_s(len[i][0],len[i][1],len[i][2]);
  }
  
//   k_noi = l_to_k(l_noise[0],l_noise[1],l_noise[2]);
//   p_noi = l_to_phi(l_noise[0],l_noise[1],l_noise[2]);
//   s_noi = l_to_s(l_noise[0],l_noise[1],l_noise[2]);
}

mat constraint_potential(double l[3][3])
{
    double vel[9];
    config_field.data.clear();
    double a_i = 0.5*(Lo + s_min + Lo + s_max);
    unsigned char ind = 0;
    for(unsigned char i=0; i<3; i++)
    {
      for(unsigned char j=0; j<3; j++)
      {
      double weight = 5.0*(1.0-exp(-0.5*norm(fields)));
      
      vel[ind] = -0.05*weight*1.0/3.0*(Lo+l[i][j]-a_i)/pow((a_i-s_max),2);
      
      config_field.data.push_back(vel[ind]);
      ind++;
      }
    }
    mat cf_mat;
    cf_mat << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << vel[0] << endr
	   << vel[1] << endr
	   << vel[2] << endr
	   << vel[3] << endr
	   << vel[4] << endr
	   << vel[5] << endr
	   << vel[6] << endr
	   << vel[7] << endr
	   << vel[8] << endr;
    return cf_mat;
}

void integral_length()
{

  // Inverse Jacobian
//   double diameter = 0.015;
//   double scale = 128.0*9.54929659643/(diameter/2.0);
//   double rpm_max = 9090.0;
//   double tendon_max = rpm_max/scale;
  unsigned char ind;
  mat Ju, rate_mat, stiff_mat;
  mat jumper;
  jumper = Jv*Jkpsl;
  Ju = join_rows(Jvb, jumper);
//   Jvb.print();
   
//   stiff_mat << stiff_vec << 0 << 0 << endr
// 	    << 0 << stiff_vec << 0 << endr
// 	    << 0 << 0 << stiff_vec << endr;
// 	    
//   fields = -K1*stiff_mat*e_vector;

//   fields << 0.001 << endr
// 	 << 0.001 << endr
// 	 << 0.000 << endr;
//   cout << "A" << endl;
  rate_mat = pinv(Ju)*(fields) + v_obs + v_config;
//   cout << "B" << endl;
  for(unsigned char i=0; i<15; i++)
  {
    rate[i] = rate_mat(i,0);
//     if(rate[i] > tendon_max)
//     {
//       rate[i] = tendon_max;
//       cout << rate[i] << endl;
//     }
//     else if(rate[i] < -tendon_max)
//     {
//       rate[i] = -tendon_max;
//       cout << rate[i] << endl;
//     }
      
  }
  
//   ind = 0;
//   for(unsigned char i=0; i<3; i++)
//   {
//     for(unsigned char j=0; j<3; j++)
//     {
//       if(len[i][j]<s_min)
//       {
// 	if(rate[ind+6]<0)
// 	{
// // 	  len[i][j] = s_min;
// 	  rate[ind+6]=0;
// 	}
//       }
//       else if(len[i][j]>s_max)
//       {
// 	if(rate[ind+6]>0)
// 	{
// // 	  len[i][j] = s_max;
// 	  rate[ind+6]=0;
// 	}
//       }
//       ind++;
//     }
// //     l_noise[i] = l_noise[i] + rate[i]*dt;// + noise_mat(i,0);
//   }

  //Integral Length
  ind = 0;
  for(unsigned char i=0; i<3; i++)
  {
    for(unsigned char j=0; j<3; j++)
    {
      len[i][j] = len[i][j] + rate[ind+6]*dt; 
      ind++;
    }
//     l_noise[i] = l_noise[i] + rate[i]*dt;// + noise_mat(i,0);
  }
  
  
  for(unsigned char i=0; i<3; i++)
  {
    position_base[i] = position_base[i] + rate[i]*dt; 
    angle_base[i] = angle_base[i] + rate[i+3]*dt; 
  }

//   if((l[0]<0.0) && (l[1]<0.0) && (l[2]<0.0)) flag_error = 1;
  c_to_q();
  
   
}

// void get_error(const geometry_msgs::Pose::ConstPtr& msg)
// {
//   geometry_msgs::Pose er;
//   er = *msg;
//   e_vector << er.position.x << endr
// 	   << er.position.y << endr
// 	   << er.position.z << endr;
// }
// 
// void get_ref(const std_msgs::Float64MultiArray::ConstPtr& msg)
// {
//   std_msgs::Float64MultiArray pathv;
//   pathv = *msg;
//   path_vel << pathv.data[0] << endr
// 	   << pathv.data[1] << endr
// 	   << pathv.data[2] << endr;
// }

void get_field(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray pathv;
  pathv = *msg;
  fields << pathv.data[0] << endr
	   << pathv.data[1] << endr
	   << pathv.data[2] << endr;
//   fields.print();
}


void obs_callback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray vob;
  vob = *msg;
  v_obs << vob.data[0] << endr
	<< vob.data[1] << endr
	<< vob.data[2] << endr
	<< vob.data[3] << endr
	<< vob.data[4] << endr
	<< vob.data[5] << endr
	<< vob.data[6] << endr
	<< vob.data[7] << endr
	<< vob.data[8] << endr
	<< vob.data[9] << endr
	<< vob.data[10] << endr
	<< vob.data[11] << endr
	<< vob.data[12] << endr
	<< vob.data[13] << endr
	<< vob.data[14] << endr;
}
 
// void get_noise(const std_msgs::Float64MultiArray::ConstPtr& msg)
// {
//   std_msgs::Float64MultiArray noise;
//   noise = *msg;
//   noise_mat << noise.data[0] << endr
// 	    << noise.data[0] << endr
// 	    << noise.data[0] << endr;
// }
// 
// void get_stiff(const std_msgs::Float64MultiArray::ConstPtr& msg)
// {
//   std_msgs::Float64MultiArray stiff;
//   stiff = *msg;
//   stiff_vec = stiff.data[0];
// }
// 

void initialize()
{
//   e_vector << 0.0 << endr
// 	   << 0.0 << endr
// 	   << 0.0 << endr;
  fields << 0.0 << endr
	 << 0.0 << endr
	 << 0.0 << endr;
  v_obs << 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr;
  v_config << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr;
  noise_mat << 0.0 << endr
	    << 0.0 << endr
	    << 0.0 << endr;
  unsigned char ind = 0;
  for(unsigned char i=0; i<3; i++)
  {
    for(unsigned char j=0; j<3; j++)
    {
      len[i][j] = y[ind+6];
      ind++;
    }
  }
  
  for(unsigned char i=0; i<3; i++)
  {
    position_base[i] = y[i];
    angle_base[i] = y[i+3];
  }
  
  c_to_q();
  flag_error = 0;
}

int
main(int argc, char** argv)
  { 
    std_msgs::Float64MultiArray vel_send, field_send, kps, length, kps_noi, length_noisy;
    geometry_msgs::Pose base_pose;
    ros::init(argc,argv, "inv_jacobi_3");
    ros::NodeHandle n;
    ros::Time begin = ros::Time::now();
    ros::Time current;
    initialize();
//     n.param("noise", noise_flag, noise_flag);
    
    ros::Rate loop_rate(freq);
    
//     ros::Publisher vel_pub = n.advertise<std_msgs::Float64MultiArray>("motor_speed",10);
//     ros::Publisher pub = n.advertise<std_msgs::Float64MultiArray>("observer/cspace/true",10);
//     ros::Publisher c_noisy = n.advertise<std_msgs::Float64MultiArray>("observer/cspace/noisy",10);
//     ros::Publisher length_pub = n.advertise<std_msgs::Float64MultiArray>("observer/length/true",10);
//     ros::Publisher length_n_pub = n.advertise<std_msgs::Float64MultiArray>("observer/length/noisy",10);
//     ros::Publisher base_pub = n.advertise<std_msgs::Float64MultiArray>("observer/base/true",10);
//     ros::Publisher base_n_pub = n.advertise<std_msgs::Float64MultiArray>("observer/base/noisy",10);
//     ros::Publisher field_pub = n.advertise<std_msgs::Float64MultiArray>("motion_plan/goal_field",10);
    ros::Publisher pub = n.advertise<std_msgs::Float64MultiArray>("configuration_space",10);
    ros::Publisher length_pub = n.advertise<std_msgs::Float64MultiArray>("length",10);
    ros::Publisher base_pub = n.advertise<geometry_msgs::Pose>("base_pose",10);
    ros::Publisher trial_pub = n.advertise<std_msgs::Float64MultiArray>("configuration_field",10);
    ros::Publisher dot_pub = n.advertise<std_msgs::Float64MultiArray>("length_rate",10);
    
//     ros::Subscriber error_sub = n.subscribe("error", 10, get_error);
    ros::Subscriber field_sub = n.subscribe("field", 10, get_field);
    ros::Subscriber rec_vel_obs = n.subscribe("obs_field_configuration_space", 10, obs_callback);
//     ros::Subscriber field_sub = n.subscribe("error", 10, get_field);
//     ros::Subscriber ref_sub = n.subscribe("ref_speed", 10, get_ref);
//     ros::Subscriber rec_vel_obs = n.subscribe("motion_plan/obs_field/cspace", 10, obs_callback);
//     ros::Subscriber procnoi_sub = n.subscribe("process_noise", 10, get_noise);
//     ros::Subscriber stiff_sub = n.subscribe("stiffness/goal", 10, get_stiff);
    

    while(ros::ok() && (flag_error == 0))
    {
//       vel_send.data.clear();
      kps.data.clear();
//       kps_noi.data.clear();
      length.data.clear();
//       length_noisy.data.clear();
//       field_send.data.clear();
      current = ros::Time::now();
      if((current.sec - begin.sec) > 1)
      {
	Jv = pass_Jv(angle_base, k_val, p_val, s_val);
	Jvb = pass_Jvb(angle_base, k_val, p_val, s_val);
	Jkpsl = pass_Jkpsl(len);
  //       Jkpsl.print();
	v_config = constraint_potential(len);
	integral_length();
      }

      for(unsigned char i=0; i<3; i++)
      {
	for(unsigned char j=0; j<3; j++)
	{
	  length.data.push_back(len[i][j]);  
	}
      }	
      for(unsigned char i=0; i<3; i++)
      {
	kps.data.push_back(position_base[i]);
      }
      for(unsigned char i=0; i<3; i++)
      {
	kps.data.push_back(angle_base[i]);
      }
      for(unsigned char i=0; i<3; i++)
      {
	kps.data.push_back(k_val[i]);
	kps.data.push_back(p_val[i]);
	kps.data.push_back(s_val[i]);
      }
      
      base_pose.position.x = position_base[0];
      base_pose.position.y = position_base[1];
      base_pose.position.z = position_base[2];
      base_pose.orientation.w = 0.5*sqrt(1+cos(angle_base[0])*cos(angle_base[1])+sin(angle_base[0])*sin(angle_base[1])*sin(angle_base[2])+cos(angle_base[0])*cos(angle_base[2])+cos(angle_base[1])*cos(angle_base[2]));
      base_pose.orientation.x = (cos(angle_base[1])*sin(angle_base[2])-(sin(angle_base[0])*sin(angle_base[1])*cos(angle_base[2])-cos(angle_base[0])*sin(angle_base[2])))/(4*base_pose.orientation.w);
      base_pose.orientation.y = (cos(angle_base[0])*sin(angle_base[1])*cos(angle_base[2])+sin(angle_base[0])*sin(angle_base[2])-(-sin(angle_base[1])))/(4*base_pose.orientation.w);
      base_pose.orientation.z = (sin(angle_base[0])*cos(angle_base[1])-(cos(angle_base[0])*sin(angle_base[1])*sin(angle_base[2])-sin(angle_base[0])*cos(angle_base[2])))/(4*base_pose.orientation.w);
      pub.publish(kps);
      length_pub.publish(length);
      trial_pub.publish(config_field);
//       c_noisy.publish(kps_noi);
//       length_n_pub.publish(length_noisy);
      base_pub.publish(base_pose);
      ros::spinOnce();
      loop_rate.sleep();
    }
    return 0;
  }
