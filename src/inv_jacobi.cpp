#include "ros/ros.h"
#include "geometry_msgs/Pose.h"
#include "std_msgs/Float64MultiArray.h"
#include <iostream>
#include <armadillo>
#include <math.h>
// #include <random>

using namespace std;
using namespace arma;

// Armadillo documentation is available at:
// http://arma.sourceforge.net/docs.html

// Global Variable
float d = 0.0134; // diameter of manipulator
float freq = 40.0;
float dt = 1.0/freq;
float l[] = {0.1232, 0.1203, 0.1123}, l_noise[] = {0.1232, 0.1203, 0.1123}, rate[3];
float k_val, p_val, s_val;
float k_noi, p_noi, s_noi;
float K1 = 2.0;
unsigned char flag_error;
int noise_flag;
float  stiff_vec;
mat e_vector, path_vel, v_obs, fields, noise_mat;
mat Jg, Jh;
ros::Time begin, current;

float l_to_k(float l1, float l2, float l3)
{
    float k_out = 2*(pow(pow(l1,2)+pow(l2,2)+pow(l3,2)-l1*l2-l1*l3-l2*l3,0.5))/(d*(l1+l2+l3));
    
    return k_out;
}

float l_to_phi(float l_1, float l_2, float l_3)
{
    float phi_out = atan2((sqrt(3)*(l_2+l_3-2*l_1)),(3*(l_2-l_3)));
    
    return phi_out;
}

float l_to_s(float l1, float l2, float l3)
{
    float s_out = (l1+l2+l3)/3;
    
    return s_out;
}

mat pass_Jg(float l1, float l2, float l3)
{
    float l_sum = l1+l2+l3;
    float l_sqrt = sqrt(pow(l1,2)+pow(l2,2)+pow(l3,2)-l1*l2-l1*l3-l2*l3);
    mat Jg_ex;
    Jg_ex << 3*(l1*l2+l1*l3-pow(l2,2)-pow(l3,2))/(d*pow(l_sum,2)*l_sqrt) << -3*(pow(l1,2)-l1*l2-l2*l3+pow(l3,2))/(d*pow(l_sum,2)*l_sqrt) << -3*(pow(l1,2)-l1*l3-l2*l3+pow(l2,2))/(d*pow(l_sum,2)*l_sqrt) << endr
	  << sqrt(3)*(l3-l2)/(2*pow(l_sqrt,2)) << sqrt(3)*(l1-l3)/(2*pow(l_sqrt,2)) << sqrt(3)*(l2-l1)/(2*pow(l_sqrt,2)) << endr
	  << 1.0/3.0 << 1.0/3.0 << 1.0/3.0 << endr;
    
    return Jg_ex;
}

mat pass_Jh(float k, float p, float s)
{
    mat Jh_ex;
    Jh_ex << cos(p)*(k*s*sin(k*s)+cos(k*s)-1)/pow(k,2) << -sin(p)*(1-cos(k*s))/k << cos(p)*sin(k*s) << endr
	  << sin(p)*(k*s*sin(k*s)+cos(k*s)-1)/pow(k,2) << cos(p)*(1-cos(k*s))/k << sin(p)*sin(k*s) << endr
	  << (-sin(k*s)+k*s*cos(k*s))/pow(k,2) << 0 << cos(k*s) << endr;
    return Jh_ex;
}

void c_to_q()
{
  k_val = l_to_k(l[0],l[1],l[2]);
  p_val = l_to_phi(l[0],l[1],l[2]);
  s_val = l_to_s(l[0],l[1],l[2]);
  k_noi = l_to_k(l_noise[0],l_noise[1],l_noise[2]);
  p_noi = l_to_phi(l_noise[0],l_noise[1],l_noise[2]);
  s_noi = l_to_s(l_noise[0],l_noise[1],l_noise[2]);
}
void integral_length()
{
  
  // Inverse Jacobian
  float diameter = 0.015;
  float scale = 128.0*9.54929659643/(diameter/2.0);
  float rpm_max = 9090.0;
  float tendon_max = rpm_max/scale;
  mat Ju, rate_mat, stiff_mat;
  
  Ju = Jh*Jg;
  stiff_mat << stiff_vec << 0 << 0 << endr
	    << 0 << stiff_vec << 0 << endr
	    << 0 << 0 << stiff_vec << endr;
	    
  fields = -K1*stiff_mat*e_vector;

//   fields << 0.001 << endr
// 	 << 0.001 << endr
// 	 << 0.000 << endr;
  current = ros::Time::now();
  double deltaT = (double)(current.sec) + (double)(current.nsec)*pow(10,-9) - ((double)(begin.sec) + (double)(begin.nsec)*pow(10,-9));
//   cout << deltaT << endl;
  double period = 10.0;
  double omega = 2*M_PI/period;
  double radius = 0.02;
//   fields << omega*radius*cos(omega*deltaT) << endr
// 	 << omega*radius*sin(omega*deltaT) << endr
// 	 << 0.000 << endr;
  rate_mat = inv(Ju)*(fields) + v_obs;
  for(unsigned char i=0; i<3; i++)
  {
    rate[i] = rate_mat(i,0);
    if(rate[i] > tendon_max)
    {
      rate[i] = tendon_max;
      cout << rate[i] << endl;
    }
    else if(rate[i] < -tendon_max)
    {
      rate[i] = -tendon_max;
      cout << rate[i] << endl;
    }
  }
    
  //Integral Length
  for(unsigned char i=0; i<3; i++)
  {
    l[i] = l[i] + rate[i]*dt;
    l_noise[i] = l_noise[i] + rate[i]*dt;// + noise_mat(i,0);
  }
  if((l[0]<0.0) && (l[1]<0.0) && (l[2]<0.0)) flag_error = 1;
  c_to_q();
}
    
    
    
void get_error(const geometry_msgs::Pose::ConstPtr& msg)
{
  geometry_msgs::Pose er;
  er = *msg;
  e_vector << er.position.x << endr
	   << er.position.y << endr
	   << er.position.z << endr;
}

void get_ref(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray pathv;
  pathv = *msg;
  path_vel << pathv.data[0] << endr
	   << pathv.data[1] << endr
	   << pathv.data[2] << endr;
}

void obs_callback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray vob;
  vob = *msg;
  v_obs << vob.data[0] << endr
	<< vob.data[1] << endr
	<< vob.data[2] << endr;
}

void get_noise(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray noise;
  noise = *msg;
  noise_mat << noise.data[0] << endr
	    << noise.data[0] << endr
	    << noise.data[0] << endr;
}

void get_stiff(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  std_msgs::Float64MultiArray stiff;
  stiff = *msg;
  stiff_vec = stiff.data[0];
}

void initialize()
{
  e_vector << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr;
  v_obs << 0.0 << endr
	<< 0.0 << endr
	<< 0.0 << endr;
  path_vel << 0.0 << endr
	   << 0.0 << endr
	   << 0.0 << endr;
  noise_mat << 0.0 << endr
	    << 0.0 << endr
	    << 0.0 << endr;
  
  c_to_q();
  flag_error = 0;
}

int
main(int argc, char** argv)
  { 
    std_msgs::Float64MultiArray vel_send, field_send, kps, length, kps_noi, length_noisy;
    ros::init(argc,argv, "inv_jacobi");
    ros::NodeHandle n;
    
    initialize();
    n.param("noise", noise_flag, noise_flag);
    
    ros::Rate loop_rate(freq);
    
    ros::Publisher vel_pub = n.advertise<std_msgs::Float64MultiArray>("motor_speed",10);
    ros::Publisher pub = n.advertise<std_msgs::Float64MultiArray>("observer/cspace/true",10);
    ros::Publisher c_noisy = n.advertise<std_msgs::Float64MultiArray>("observer/cspace/noisy",10);
    ros::Publisher length_pub = n.advertise<std_msgs::Float64MultiArray>("observer/length/true",10);
    ros::Publisher length_n_pub = n.advertise<std_msgs::Float64MultiArray>("observer/length/noisy",10);
    ros::Publisher field_pub = n.advertise<std_msgs::Float64MultiArray>("motion_plan/goal_field",10);
    
    ros::Subscriber error_sub = n.subscribe("error", 10, get_error);
    ros::Subscriber ref_sub = n.subscribe("ref_speed", 10, get_ref);
    ros::Subscriber rec_vel_obs = n.subscribe("motion_plan/obs_field/cspace", 10, obs_callback);
    ros::Subscriber procnoi_sub = n.subscribe("process_noise", 10, get_noise);
    ros::Subscriber stiff_sub = n.subscribe("stiffness/goal", 10, get_stiff);
    
    begin = ros::Time::now();
    
    while(ros::ok() && (flag_error == 0))
    {
      vel_send.data.clear();
      kps.data.clear();
      kps_noi.data.clear();
      length.data.clear();
      length_noisy.data.clear();
      field_send.data.clear();
      
      Jg = pass_Jg(l[0],l[1],l[2]);
      Jh = pass_Jh(k_val, p_val, s_val);
//       Jh.print("Jh:");
      integral_length();
      for(unsigned char i=0; i<3; i++)
      {
	vel_send.data.push_back(rate[i]);
	length.data.push_back(l[i]);
	length_noisy.data.push_back(l_noise[i]);
	field_send.data.push_back(fields(i,0));
      }	
      kps.data.push_back(k_val);
      kps.data.push_back(p_val);
      kps.data.push_back(s_val);
      kps_noi.data.push_back(k_noi);
      kps_noi.data.push_back(p_noi);
      kps_noi.data.push_back(s_noi);
      
      vel_pub.publish(vel_send);
      field_pub.publish(field_send);
      pub.publish(kps);
      length_pub.publish(length);
      c_noisy.publish(kps_noi);
      length_n_pub.publish(length_noisy);
      
      ros::spinOnce();
      loop_rate.sleep();
    }
    return 0;
  }