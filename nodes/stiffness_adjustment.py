#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_motion_planning')
import rospy
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Pose
import tf
import math
from sympy import *
    
class stiffness_adjustment(object):
  def __init__(self):
    rospy.init_node('stiffness_adjustment')
    self.stiff_g_vec = Float64MultiArray()
    self.stiff_o_vec = Float64MultiArray()
    self.vel_max = Float64MultiArray()
    self.bound = 0.01
    self.num_per_seg = 3
    self.distance = 1.0
    error_sub = rospy.Subscriber('observer_error', Pose, self.get_error)
    obs_sub = rospy.Subscriber('motion_plan/obs_field/tspace', Float64MultiArray, self.get_obs_field)
    goal_sub = rospy.Subscriber('motion_plan/goal_field', Float64MultiArray, self.get_go_field)
    dist_sub = rospy.Subscriber('min_distance', Float64MultiArray, self.get_dist)
    goal_pub = rospy.Publisher('stiffness/goal', Float64MultiArray, queue_size = 10)
    obstacle_pub = rospy.Publisher('stiffness/obs', Float64MultiArray, queue_size = 10)
    field_pub = rospy.Publisher('obs_field_max', Float64MultiArray, queue_size = 10)
    
    self.e_vector = [1.0, 1.0, 1.0]
    self.stiff_goal = 0.0
    self.stiff_obs = 0.0
    self.goal_field = zeros(3,1)
    self.obs_field = []
    self.state_mag = 0
    self.state_dir = 0
    self.state_observer = 0
    for i in range(0,self.num_per_seg):
      self.obs_field.append(zeros(3,1))
      
    rate = rospy.Rate(40.0)
    
    while (not rospy.is_shutdown()):
	self.stiffness()
	self.transform_to_sent()
	goal_pub.publish(self.stiff_g_vec)
	obstacle_pub.publish(self.stiff_o_vec)
	field_pub.publish(self.vel_max)
        rate.sleep()

  def get_error(self,msg):
    self.e_vector = [msg.position.x, msg.position.y, msg.position.z]
    
  def get_dist(self,msg):
    self.distance = msg.data[0]
    
  def get_obs_field(self,msg):
    self.obs_field = []
    for i in range(0,self.num_per_seg):
      self.obs_field.append(Matrix([[msg.data[i*self.num_per_seg+0]],[msg.data[i*self.num_per_seg+1]],[msg.data[i*self.num_per_seg+2]]]))
    
  def get_go_field(self,msg):
    self.goal_field = Matrix([[msg.data[0]],[msg.data[1]],[msg.data[2]]])
    
  def max_field(self):
    self.max_val = 0.0
    self.max_index = 0
    for i in range(0, self.num_per_seg):
      if(self.obs_field[i].norm()>self.max_val):
	self.max_val = self.obs_field[i].norm()
	self.max_index = i
    
  def stiffness(self):
    self.max_field()
    
    #stiffness_adjustment due to dynamic of observer
    error_mag = math.sqrt(self.e_vector[0]**2+self.e_vector[1]**2+self.e_vector[2]**2)
    if((error_mag > self.bound) and (self.state_observer == 0)):
      self.stiff_goal = 0.0
      self.stiff_obs = 0.0
    else:
      self.stiff_goal = 1.0
      self.stiff_obs = 1.0
      self.state_observer = 1
      
    #stiffness_adjustment due to avoid fighting between attractive and repulsive
    #scale = 0.000
    #self.field_limit = 0.00005
    #if(self.max_val > self.field_limit):
      #self.state_mag = 1
    #else:
      #self.state_mag = 0
    
    #cos_alpha = self.obs_field[self.max_index].T*self.goal_field
    #if(cos_alpha[0,0] < 0):
      #self.state_dir = 1
    #else:
      #self.state_dir = 0
    
    ##if((self.state_mag == 1) and (self.state_dir == 1)):
    
    #if(self.distance > 0.025):
      #weight = 1.0
    #else:
      #weight = math.exp(14*self.distance)-1
      
    #self.stiff_goal = self.stiff_goal*weight
    #self.stiff_goal = self.stiff_goal*(1.0 - math.exp(-10*self.distance))
    
    
    #if (self.state_stiff == 1):
      #self.stiff_goal = self.stiff_goal*scale
      
    #for i in range(0, self.num_per_seg):
      #self.state_quit = 0
      #if(self.obs_field[i].norm() == 0):
	#self.state_quit = self.state_quit + 1
      #if(self.state_quit == 0):
	#self.state_stiff = 0
    
  def transform_to_sent(self):
    self.stiff_g_vec.data = []
    self.stiff_o_vec.data = []
    self.vel_max.data = []
    self.stiff_g_vec.data.append(self.stiff_goal)
    self.stiff_o_vec.data.append(self.stiff_obs)
    self.vel_max.data.append(self.max_val)
      
if __name__ == '__main__':
    try:
        stiffness_adjustment()
    except rospy.ROSInterruptException: pass