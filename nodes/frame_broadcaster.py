#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_motion_planning')
import rospy
from geometry_msgs.msg import Pose
import tf
import random
    
class path_broadcaster(object):
  def __init__(self):
    rospy.init_node('frame_broadcaster')
    
    self.active_frame = rospy.get_param('~frame')
    self.reference_frame = rospy.get_param('~ref')
    topic_name = rospy.get_param('~subscribe_from')
    
    tip_sub = rospy.Subscriber(topic_name, Pose, self.handle_pose)
        
    rospy.spin()

  def handle_pose(self, msg):
    path_frame = tf.TransformBroadcaster()
    path_frame.sendTransform((msg.position.x,msg.position.y,msg.position.z),
                     (0.0, 0.0, 0.0, 1.0),
                     rospy.Time.now(),
                     self.active_frame,
                     self.reference_frame)
        
if __name__ == '__main__':
    try:
        path_broadcaster()
    except rospy.ROSInterruptException: pass
