#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_motion_planning')
import math
import rospy
import numpy as np
from std_msgs.msg import Float64MultiArray

class noise_publisher(object):
  def __init__(self):
    rospy.init_node('noise_publisher')
    self.procnoi = Float64MultiArray()
    self.measnoi = Float64MultiArray()
    procnoi_pub = rospy.Publisher('process_noise', Float64MultiArray, queue_size = 10)
    measnoi_pub = rospy.Publisher('measurement_noise', Float64MultiArray, queue_size = 10)
    
    rate = rospy.Rate(40.0)
    
    while (not rospy.is_shutdown()):
	self.noise_generator()
	self.transform_to_sent()
	procnoi_pub.publish(self.procnoi)
	measnoi_pub.publish(self.measnoi)
        rate.sleep()
        
        #rospy.spin()  
  
  def noise_generator(self):
    # Noise generator
    self.mu_proc, self.mu_meas = 0.0, 0.0
    self.sigma_proc, self.sigma_meas = 0.001, 0.001
    self.size_proc, self.size_meas = 1, 3
    self.proc_noi = np.random.normal(self.mu_proc, self.sigma_proc, self.size_proc)
    self.meas_noi = np.random.normal(self.mu_meas, self.sigma_meas, self.size_meas)
    #self.noise = scalar_noise*[1, 1, 1]
    
  def transform_to_sent(self):
    self.procnoi.data = []
    self.measnoi.data = []
    for i in range(0,self.size_proc):
      self.procnoi.data.append(self.proc_noi[i])
    for i in range(0,self.size_meas):
      self.measnoi.data.append(self.meas_noi[i])
    
if __name__ == '__main__':
    try:
        noise_publisher()
    except rospy.ROSInterruptException: pass
