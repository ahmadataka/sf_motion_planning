#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_motion_planning')
import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from std_msgs.msg import Float64MultiArray
from sf_path_planning.msg import sf_msg
from sympy import *


class record_data(object):
  def __init__(self):
    rospy.init_node('record_data')
    self.obs_num = rospy.get_param('/obs_num')
    self.base = rospy.get_param('/base')
    self.kalman_filter = rospy.get_param('/kalman_filter')
    self.send = sf_msg()
    self.k = [0.0, 0.0, 0.0]
    self.p = [0.0, 0.0, 0.0]
    self.s = [0.0, 0.0, 0.0]
    self.k_e = [0.0, 0.0, 0.0]
    self.p_e = [0.0, 0.0, 0.0]
    self.s_e = [0.0, 0.0, 0.0]
    self.l = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    self.l_e = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    self.position_base = [0.0, 0.0, 0.0]
    self.angle_base = [0.0, 0.0, 0.0]
    
    send_pub = rospy.Publisher('/record_data', sf_msg, queue_size = 10)
    rec_goal = rospy.Subscriber('/goal_pose', Pose, self.goal_callback)
    rec_tip = rospy.Subscriber('/tip_pose', PoseArray, self.tip_callback)
    rec_tip_est = rospy.Subscriber('/tip_pose_est', PoseArray, self.tip_callback_est)
    rec_obs = rospy.Subscriber('/obs_pose', PoseArray, self.obspose_callback)
    c_sub = rospy.Subscriber('configuration_space', Float64MultiArray, self.get_kps)
    c_est_sub = rospy.Subscriber('configuration_space_est', Float64MultiArray, self.get_kps_est)
    l_sub = rospy.Subscriber('length', Float64MultiArray, self.get_l)
    l_est_sub = rospy.Subscriber('length_est', Float64MultiArray, self.get_l_est)
    self.send = sf_msg()
    self.obs_pose = Pose()
    self.tip_pose = Pose()

    frequency = 40.0
    r = rospy.Rate(frequency)
    
    while not rospy.is_shutdown():
	self.transform_to_sent()
	send_pub.publish(self.send)
        r.sleep()
        
  def get_kps(self,kps):
    if(self.base == 1):
      self.position_base = [kps.data[0], kps.data[1], kps.data[2]]
      self.angle_base = [kps.data[3], kps.data[4], kps.data[5]]
      self.k = [kps.data[6], kps.data[9], kps.data[12]]
      self.p = [kps.data[7], kps.data[10], kps.data[13]]
      self.s = [kps.data[8], kps.data[11], kps.data[14]]
    else:
      self.k = [kps.data[0], kps.data[3], kps.data[6]]
      self.p = [kps.data[1], kps.data[4], kps.data[7]]
      self.s = [kps.data[2], kps.data[5], kps.data[8]]

  def get_kps_est(self,kps):
    if(self.kalman_filter == 1):
      self.k_e = [kps.data[0], kps.data[3], kps.data[6]]
      self.p_e = [kps.data[1], kps.data[4], kps.data[7]]
      self.s_e = [kps.data[2], kps.data[5], kps.data[8]]

  def get_l(self, msg):
    for i in range(0,9):
      self.l[i] = msg.data[i]

  def get_l_est(self, msg):
    if(self.kalman_filter == 1):
      for i in range(0,9):
	self.l_e[i] = msg.data[i]

  def tip_callback(self, msg):
    self.send.tip.poses = []
    for i in range(0, 3):
      self.tip_pose = Pose()
      self.tip_pose.position.x = msg.poses[i].position.x
      self.tip_pose.position.y = msg.poses[i].position.y
      self.tip_pose.position.z = msg.poses[i].position.z
      self.tip_pose.orientation.w = msg.poses[i].orientation.w
      self.tip_pose.orientation.x = msg.poses[i].orientation.x
      self.tip_pose.orientation.y = msg.poses[i].orientation.y
      self.tip_pose.orientation.z = msg.poses[i].orientation.z
      self.send.tip.poses.append(self.tip_pose)
    
  def tip_callback_est(self, msg):
    if(self.kalman_filter == 1):
      self.send.tip_est.poses = []
      for i in range(0, 3):
	self.tip_pose = Pose()
	self.tip_pose.position.x = msg.poses[i].position.x
	self.tip_pose.position.y = msg.poses[i].position.y
	self.tip_pose.position.z = msg.poses[i].position.z
	self.tip_pose.orientation.w = msg.poses[i].orientation.w
	self.tip_pose.orientation.x = msg.poses[i].orientation.x
	self.tip_pose.orientation.y = msg.poses[i].orientation.y
	self.tip_pose.orientation.z = msg.poses[i].orientation.z
	self.send.tip_est.poses.append(self.tip_pose)
      
  def goal_callback(self, msg):
      self.send.goal.position.x = msg.position.x
      self.send.goal.position.y = msg.position.y
      self.send.goal.position.z = msg.position.z
      self.send.goal.orientation.w = msg.orientation.w
      self.send.goal.orientation.x = msg.orientation.x
      self.send.goal.orientation.y = msg.orientation.y
      self.send.goal.orientation.z = msg.orientation.z

  def obspose_callback(self, msg):
    self.send.obstacles.poses = []
    for i in range(0, self.obs_num):
      self.obs_pose = Pose()
      self.obs_pose.position.x = msg.poses[i].position.x
      self.obs_pose.position.y = msg.poses[i].position.y
      self.obs_pose.position.z = msg.poses[i].position.z
      self.obs_pose.orientation.w = msg.poses[i].orientation.w
      self.obs_pose.orientation.x = msg.poses[i].orientation.x
      self.obs_pose.orientation.y = msg.poses[i].orientation.y
      self.obs_pose.orientation.z = msg.poses[i].orientation.z
      self.send.obstacles.poses.append(self.obs_pose)
      #print(i)


  def transform_to_sent(self):
    self.send.c_space.data = []
    self.send.l_space.data = []
    self.send.state_est.data = []
    self.send.c_space.data.append(self.k[0])
    self.send.c_space.data.append(self.p[0])
    self.send.c_space.data.append(self.s[0])
    self.send.c_space.data.append(self.k[1])
    self.send.c_space.data.append(self.p[1])
    self.send.c_space.data.append(self.s[1])
    self.send.c_space.data.append(self.k[2])
    self.send.c_space.data.append(self.p[2])
    self.send.c_space.data.append(self.s[2])
    self.send.l_space.data.append(self.l[0])
    self.send.l_space.data.append(self.l[1])
    self.send.l_space.data.append(self.l[2])
    self.send.l_space.data.append(self.l[3])
    self.send.l_space.data.append(self.l[4])
    self.send.l_space.data.append(self.l[5])
    self.send.l_space.data.append(self.l[6])
    self.send.l_space.data.append(self.l[7])
    self.send.l_space.data.append(self.l[8])
    if(self.kalman_filter == 1):
      self.send.state_est.data.append(self.l_e[0])
      self.send.state_est.data.append(self.l_e[1])
      self.send.state_est.data.append(self.l_e[2])
      self.send.state_est.data.append(self.l_e[3])
      self.send.state_est.data.append(self.l_e[4])
      self.send.state_est.data.append(self.l_e[5])
      self.send.state_est.data.append(self.l_e[6])
      self.send.state_est.data.append(self.l_e[7])
      self.send.state_est.data.append(self.l_e[8])
    self.send.base.position.x = self.position_base[0]
    self.send.base.position.y = self.position_base[1]
    self.send.base.position.z = self.position_base[2]
    self.send.base.orientation.w = 0.5*sqrt(1+cos(self.angle_base[0])*cos(self.angle_base[1])+sin(self.angle_base[0])*sin(self.angle_base[1])*sin(self.angle_base[2])+cos(self.angle_base[0])*cos(self.angle_base[2])+cos(self.angle_base[1])*cos(self.angle_base[2]))
    self.send.base.orientation.x = (cos(self.angle_base[1])*sin(self.angle_base[2])-(sin(self.angle_base[0])*sin(self.angle_base[1])*cos(self.angle_base[2])-cos(self.angle_base[0])*sin(self.angle_base[2])))/(4*self.send.base.orientation.w)
    self.send.base.orientation.y = (cos(self.angle_base[0])*sin(self.angle_base[1])*cos(self.angle_base[2])+sin(self.angle_base[0])*sin(self.angle_base[2])-(-sin(self.angle_base[1])))/(4*self.send.base.orientation.w)
    self.send.base.orientation.z = (sin(self.angle_base[0])*cos(self.angle_base[1])-(cos(self.angle_base[0])*sin(self.angle_base[1])*sin(self.angle_base[2])-sin(self.angle_base[0])*cos(self.angle_base[2])))/(4*self.send.base.orientation.w)
    
    self.send.now = rospy.get_rostime()
            
if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
