#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_motion_planning')
import rospy
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Pose
import tf
import math
from sympy import *
    
class stiffness_adjustment(object):
  def __init__(self):
    rospy.init_node('stiffness_adjustment')
    self.stiff_g_vec = Float64MultiArray()
    self.stiff_o_vec = Float64MultiArray()
    self.bound = 0.02
    self.num_per_seg = 3
    
    error_sub1 = rospy.Subscriber('observer_error_up', Pose, self.get_up)
    error_sub2 = rospy.Subscriber('observer_error_mid', Pose, self.get_mid)
    error_sub3 = rospy.Subscriber('observer_error_bot', Pose, self.get_bot)
    obs_sub = rospy.Subscriber('obs_field_task_space', Float64MultiArray, self.get_obs_field)
    goal_sub = rospy.Subscriber('field', Float64MultiArray, self.get_go_field)
    goal_pub = rospy.Publisher('stiffness/goal', Float64MultiArray, queue_size = 10)
    obstacle_pub = rospy.Publisher('stiffness/obs', Float64MultiArray, queue_size = 10)
    
    self.e_up = Matrix([[1.0], [1.0], [1.0]])
    self.e_mid = Matrix([[1.0], [1.0], [1.0]])
    self.e_bot = Matrix([[1.0], [1.0], [1.0]])
    self.stiff_goal = 0.0
    self.stiff_obs = 0.0
    self.goal_field = zeros(3,1)
    self.obs_field = []
    for i in range(0,3*self.num_per_seg):
      self.obs_field.append(zeros(3,1))
      
    rate = rospy.Rate(40.0)
    
    while (not rospy.is_shutdown()):
	self.stiffness()
	self.transform_to_sent()
	goal_pub.publish(self.stiff_g_vec)
	obstacle_pub.publish(self.stiff_o_vec)
        rate.sleep()

  def get_up(self,msg):
    self.e_up = Matrix([[msg.position.x], [msg.position.y], [msg.position.z]])
  
  def get_mid(self,msg):
    self.e_mid = Matrix([[msg.position.x], [msg.position.y], [msg.position.z]])
  
  def get_bot(self,msg):
    self.e_bot = Matrix([[msg.position.x], [msg.position.y], [msg.position.z]])
  
  def get_obs_field(self,msg):
    self.obs_field = []
    ind = 0
    for j in range(0, 3):
      for i in range(0,self.num_per_seg):
	self.obs_field.append(Matrix([[msg.data[ind*self.num_per_seg+0]],[msg.data[ind*self.num_per_seg+1]],[msg.data[ind*self.num_per_seg+2]]]))
	ind = ind+1
    
  def get_go_field(self,msg):
    self.goal_field = Matrix([[msg.data[0]],[msg.data[1]],[msg.data[2]]])
    
  def stiffness(self):
    #stiffness_adjustment due to dynamic of observer
    error_mag_up = self.e_up.norm()
    error_mag_mid = self.e_mid.norm()
    error_mag_bot = self.e_bot.norm()
    error_mag = error_mag_up + error_mag_mid + error_mag_bot
    if((error_mag) > 3*self.bound):
      self.stiff_goal = 0.0
      self.stiff_obs = 0.0
    else:
      self.stiff_goal = 1.0
      self.stiff_obs = 1.0
      
    #stiffness_adjustment due to avoid fighting between attractive and repulsive
    #scale = 0.001
    #self.field_limit = 0.00005
    #for i in range(0, self.num_per_seg):
      #if(self.obs_field[i].norm() > self.field_limit):
	#self.stiff_goal = self.stiff_goal*scale
	#break
    
  def transform_to_sent(self):
    self.stiff_g_vec.data = []
    self.stiff_o_vec.data = []
    self.stiff_g_vec.data.append(self.stiff_goal)
    self.stiff_o_vec.data.append(self.stiff_obs)
      
if __name__ == '__main__':
    try:
        stiffness_adjustment()
    except rospy.ROSInterruptException: pass